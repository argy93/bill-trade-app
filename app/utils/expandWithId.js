import uuid from 'uuid';

const expandWithId = (array, additionalParameters = {}) => array.map((value) => {
  return {
    id: uuid(),
    ...value,
    ...additionalParameters,
  };
});

export default expandWithId;
