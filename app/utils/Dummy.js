import dummyProfile from './dummy/dummyProfile';

export class Dummy {
  static getProfile(forceFail = false) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (forceFail) {
          reject(new Error('Dummy error'));
        } else {
          resolve(dummyProfile);
        }
      }, 300);
    });
  }
}
