const formatDate = (notFormatedDate, short = false) => {
  const date = new Date(notFormatedDate);
  const year = date.getFullYear();
  let month = date.getMonth() + 1;
  let dt = date.getDate();
  let hours = date.getHours();
  let minutes = date.getMinutes();

  if (dt < 10) {
    dt = `0${dt}`;
  }
  if (month < 10) {
    month = `0${month}`;
  }

  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  if (short) {
    return `${month}.${dt} ${hours}:${minutes}`;
  }
  return `${year}-${month}-${dt} ${hours}:${minutes}`;
};

export default formatDate;
