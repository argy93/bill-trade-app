import random from 'random';
import generatorName from './generators/name';
import generatorStats from './generators/stats';
import generatorTagsMain from './generators/tagsMain';
import generatorPortfolio from './generators/portfolio';
import generatorFinance from './generators/finance';
import generatorTransactions from './generators/transactions';

export function generate(count = random.int(2, 15)) {
  const result = [];

  for (let i = 0; i < count; i += 1) {
    result.push({
      id: i,
      photo: 'https://via.placeholder.com/200.jpg',
      name: generatorName(),
      tags: {
        tagsMain: generatorTagsMain(),
        tagsSecond: ['Accredited investor'],
      },
      stats: generatorStats(),
      portfolio: generatorPortfolio(),
      finance: generatorFinance(),
      about: 'Теосо́фия — теоретическая часть оккультизма и оккультное движение; в широком смысле слова — мистическое богопознание, созерцание',
      transactions: generatorTransactions(),
      percentTransactions: generatorTransactions(),
      strides: [
        {
          name: 'Profit factor',
          value: 90,
        },
        {
          name: 'Investors deposits',
          value: 99,
        },
        {
          name: 'Profit/Loss ratio',
          value: 44,
        },
        {
          name: 'Maximum declared loss',
          value: 50,
        },
        {
          name: 'Daily maximum loss',
          value: 50,
        },
        {
          name: 'Winrate',
          value: 90,
        },
        {
          name: 'Percent of average slip',
          value: 100,
        },
        {
          name: 'Platform trading time',
          value: 10,
        },
      ],
    });
  }

  if (count === 1) return result[0];

  return result;
}

export default generate(1);
