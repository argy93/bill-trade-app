import random from 'random';

const platforms = [
  'Binance',
  'Bitmax',
  'Bitfinex',
  'Bibox',
  'Bitmex',
  'Huobi',
  'OKEx',
  'Gate',
  'Poloniex',
];

const statsCurrencies = [
  'Eth',
  'Erp',
  'Btc',
];

export default function () {
  const result = [];

  for (let i = 0; i < platforms.length; i += 1) {
    const stats = [];
    const statsRand = random.int(0, 2);

    for (let j = 0; j <= statsRand; j += 1) {
      const typeRand = random.int(0, 2);

      if (!stats.find((item) => item.name === statsCurrencies[typeRand])) {
        stats.push({
          name: statsCurrencies[typeRand],
          value: parseFloat(random.float(1, 30)
            .toFixed(3)),
        });
      } else {
        j -= 1;
      }
    }
    result.push({
      name: platforms[i],
      stats,
    });
  }

  return result;
}
