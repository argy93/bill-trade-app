import random from 'random';

const tags = [
  'Trader', 'Analyst', 'Investor',
];

export default function () {
  const count = random.int(1, tags.length);
  const result = [];

  for (let i = 0; i < count; i += 1) {
    const rand = random.int(0, tags.length - 1);

    if (!result.find((item) => item === tags[rand])) {
      result.push(tags[rand]);
    } else {
      i -= 1;
    }
  }

  return result;
}
