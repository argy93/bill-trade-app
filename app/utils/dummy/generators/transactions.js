import random from 'random';
import { addDays, getTime } from 'date-fns';

const currencies = [
  'Eth',
  'Usd',
  'Btc',
];

export default function () {
  const pointCount = random.int(5, 75);
  const transactions = [...currencies.map((name) => ({
    name,
    points: [],
  }))];
  let timestamp = new Date(2014, 6, 10);

  for (let i = 0; i < pointCount; i += 1) {
    for (let j = 0; j < transactions.length; j += 1) {
      transactions[j].points.push({
        timestamp: getTime(timestamp),
        value: parseFloat(random.float(-10, 45)
          .toFixed(3)),
      });
    }
    timestamp = addDays(timestamp, 2);
  }

  return transactions;
}
