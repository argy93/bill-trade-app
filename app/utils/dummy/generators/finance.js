import random from 'random';

export default function () {
  const balance = random.int(100, 50345123);
  const profit = parseInt(
    (balance * random.float(1.1, 1.78)).toFixed(),
    10
  );

  return {
    balance: {
      usd: balance,
      btc: balance / 10000,
    },
    profit: {
      usd: profit,
      btc: profit / 10000,
    },
  };
}
