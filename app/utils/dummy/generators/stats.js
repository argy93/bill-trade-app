import random from 'random';

export default function () {
  const followers = random.int(0, 500);
  const s = followers * random.float(1.1, 1.6);
  const following = parseInt(s.toFixed(), 10);
  const winRate = parseFloat(random.float(0.5, 100)
    .toFixed(1));
  const rank = parseFloat(random.float(1, 10)
    .toFixed(1));
  const maxLoss = parseFloat(random.float(2, 1000)
    .toFixed(2));
  const averageTime = random.int(2, 300);
  const trades = random.int(0, 400);

  return {
    following,
    followers,
    winRate,
    rank,
    maxLoss,
    averageTime,
    trades,
    tradingType: 'Day Trading',
    dailyProfit: 115,
    investorsProfit: 32000
  };
}
