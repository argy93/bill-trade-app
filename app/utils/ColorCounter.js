import Color from 'color';

export default class ColorCounter {
  constructor(iterateFrom = 0) {
    this.defaultIterator = iterateFrom;
    this.colorIterator = iterateFrom;
    this.colors = [
      '#9289E9',
      '#FFC700',
      '#20AD65',
      '#A7C2F1',
      '#FF9B81',
      '#FFB8CB',
      '#FFCBE4',
      '#F3C9ED',
      '#CEA5E8',
      '#B496E8',
    ];

    this.additionalColor = Color(this.colors[this.colors.length - 1]);
  }

  getAdditionalColor() {
    this.additionalColor = this.additionalColor.darken(0.05);

    return this.additionalColor.hex();
  }

  resetIterator(resetTo) {
    this.additionalColor = Color(this.colors[this.colors.length - 1]);
    this.colorIterator = resetTo || this.defaultIterator;
  }

  getNextColor() {
    if (this.colorIterator >= this.colors.length - 1) {
      return this.getAdditionalColor();
    }
    this.colorIterator += 1;

    return this.colors[this.colorIterator];
  }
}
