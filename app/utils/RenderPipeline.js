export default class RenderPipeline {
  constructor() {
    this.items = [];
    this.timeout = null;
  }

  add(func, zIndex = 0, debug) {
    this.items.push({
      func,
      zIndex,
      debug,
    });
  }

  reset() {
    clearTimeout(this.timeout);
    this.items = [];
  }

  render(delay = 0) {
    const sorted = this.items.sort((itemA, itemB) => {
      if (itemA.zIndex < itemB.zIndex) {
        return -1;
      }

      if (itemA.zIndex > itemB.zIndex) {
        return 1;
      }

      return 0;
    });

    this.timeout = setTimeout(() => {
      sorted.forEach((item) => item.func());
    }, delay);
  }
}
