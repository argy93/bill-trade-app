/**
 * Function splits the expression by 3 characters, separating them with a comma and adds the fractional part at the end, if it exists
 *
 * @param {number} num - value which need to be formatted
 * @returns {string} 333444555.67 -> 333,444,555.67
 */
const separateNum = (num) => {
  let negativeValue = false;

  if (Math.sign(num) === -1) {
    num *= -1;
    negativeValue = true;
  }

  const numArr = num.toString().split('.');
  const floatPart = numArr[1] ? `.${numArr[1]}` : '';
  const minus = negativeValue ? '-' : '';

  return minus + numArr[0].split(/(?=(?:\d{3})+(?!\d))/).join(',') + floatPart;
};


/**
 * Function converts the expression to a string, converting every 3 characters from the end to the character "k"
 *
 * @param {number} num - value which need to be formatted
 * @param {string} currency - currency the value is converted in
 * @returns {string} 1222333 -> 1kk
 */
const formattingNumbersByThousands = (num, currency = '$') => {
  const str = num.toFixed(0).toString();

  if (str.length > 3) {
    const strArray = str.split(/(?=(?:\d{3})+(?!\d))/);

    return currency + strArray.map((item, i) => (item.length === 3 && i !== 0 ? 'k' : item)).join('');
  }

  return currency + str;
};


/**
 * Function to convert expression to abbreviation
 *
 * @param {string} str - Expression which convert to abbreviation
 * @returns {string} - If the expression can be converted, the abbreviation is returned, otherwise the expression itself
 */
const strAbbreviation = (str) => {
  if (str.match(/ /) !== null) {
    str = str.split(' ').map((item) => item.charAt(0).toUpperCase()).join('');
  }

  return str;
};

export {
  separateNum,
  formattingNumbersByThousands,
  strAbbreviation
};
