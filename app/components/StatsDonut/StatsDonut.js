import React from 'react';
import PropTypes from 'prop-types';
import SVG from 'svg.js';

import './style';

class StatsDonut extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.rankSvg = React.createRef();
    this.radius = 165;

    this.windowWidth = window.innerWidth;
  }

  componentDidMount() {
    const green = 2;
    const red = 4;

    const length = Math.PI * (this.radius - 10);
    const offsetGreen = length - (length / 100 * (green * 10));
    const offsetRed = length - (length / 100 * (red * 10));

    console.log(length, offsetGreen, offsetRed);

    if (this.windowWidth < 1100) {
      this.radius = 130;
    }
    if ((this.windowWidth < 960 && this.windowWidth >= 576)
    || this.windowWidth < 400) {
      this.radius = 200;
    }

    if (this.rankSvg) {
      this.draw = SVG(this.rankSvg.current)
        .size(this.radius, this.radius)
        .viewbox({
          x: 0,
          y: -2,
          width: this.radius,
          height: this.radius + 2,
        });

      this.greyCircle = this.draw.circle(this.radius - 10);
      this.blueCircle = this.draw.circle(this.radius - 10);
      this.greenCircle = this.draw.circle(this.radius - 10);

      this.greyCircle.attr({
        cx: this.radius / 2,
        cy: this.radius / 2,
        fill: 'transparent',
        stroke: '#F2F2F2',
        'stroke-width': 10,
      });

      this.blueCircle.attr({
        cx: this.radius / 2,
        cy: this.radius / 2,
        fill: 'transparent',
        stroke: '#2F80ED',
        'stroke-width': 10,
        'stroke-dasharray': length,
        'stroke-dashoffset': offsetGreen,
      });

      this.greenCircle.attr({
        cx: this.radius / 2,
        cy: this.radius / 2,
        fill: 'transparent',
        stroke: '#20AD65',
        'stroke-width': 10,
        'stroke-dasharray': length,
        'stroke-dashoffset': offsetRed,
      });
    }
  }

  render() {
    return (
      <div className="stats-donut">
        <svg
          style={{
            transform: 'rotate(-90deg)',
          }}
          ref={this.rankSvg}
        />
      </div>
    );
  }
}

StatsDonut.propTypes = {

};

export default StatsDonut;
