import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import FullScreenIcon from '../Icons/FullScreenIcon';
import NormalScreenIcon from '../Icons/NormalScreenIcon';
import TimeFilter from '../TimeFilter';
import ChartWithControls from '../ChartWithControls';
import { propsChartData } from '../Chart';

class Activity extends PureComponent {
  render() {
    const {
      animateToFull,
      animateToNormal,
      changePeriod,
      period,
      full,
      transactions,
      percentTransactions,
      chartHeight,
      resizeIcon
    } = this.props;

    console.log(this.props);

    return (
      <div className={`c-activity ${(full ? ' is-full' : '')}`}>
        <div className="c-activity__head">
          <div className="c-activity__head-title">Your activity</div>
          {/* eslint-disable-next-line no-nested-ternary */}
          {window.innerWidth >= 960 && resizeIcon ? (
            full ? (
              <button type="button" className="c-activity__head-expand" onClick={animateToNormal}>
                <NormalScreenIcon />
              </button>
            ) : (
              <button type="button" className="c-activity__head-expand" onClick={animateToFull}>
                <FullScreenIcon />
              </button>
            )
          ) : null}
        </div>
        <div className="c-activity__stats">
          <div className="c-activity__stats-title">TimePeriod:</div>
          <TimeFilter
            type={'normal'}
            from={period.from}
            to={period.to}
            filterUpdate={changePeriod}
          />
        </div>
        <ChartWithControls
          transactions={transactions}
          percentTransactions={percentTransactions}
          height={chartHeight}
          chartTitle={false}
        />
      </div>
    );
  }
}

Activity.propTypes = {
  animateToFull: PropTypes.func,
  animateToNormal: PropTypes.func,
  changePeriod: PropTypes.func,
  transactions: propsChartData,
  percentTransactions: propsChartData,
  period: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  full: PropTypes.bool,
  chartHeight: PropTypes.number,
  resizeIcon: PropTypes.bool
};

export default Activity;
