import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import { separateNum } from '../../utils/helpers';

// eslint-disable-next-line react/prefer-stateless-function
class WalletInfo extends React.Component {
  render() {
    const {
      title,
      usd,
      btc,
      type
    } = this.props;

    return (
      <div className={`c-wallet-info is-right-separator is-${type}`}>
        <span className="c-wallet-info__title">{title}</span>
        <span className="c-wallet-info__usd">$ {separateNum(usd)}</span>
        <span className="c-wallet-info__crypto">{separateNum(btc)} BTC</span>
      </div>
    );
  }
}

WalletInfo.propTypes = {
  title: PropTypes.string,
  usd: PropTypes.number,
  btc: PropTypes.number,
  type: PropTypes.oneOf(['blue', 'green', 'short'])
};

export default WalletInfo;
