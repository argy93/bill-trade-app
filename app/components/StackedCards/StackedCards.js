/* eslint-disable react/no-array-index-key */
/**
 *
 * StackedCards
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';

const StackedCards = ({
  items,
}) => {
  const renderedItems = items.map((item, index) => (
    <div
      className={index === 0 ? 'item first' : 'item'}
      key={index}
      style={index === 0 ? { background: item.background, borderColor: item.background } : {}}
    >
      <div className="title">{ item.title }</div>
      <div className="value">{item.value}</div>
      <div
        className="description"
        style={{ color: item.color ? item.color : '#BDBDBD' }}
      >
        {item.description}
      </div>
    </div>
  ));
  return (
    <div className="stacked-cards-container">
      {renderedItems}
    </div>
  );
};

StackedCards.propTypes = {
  items: PropTypes.array,
};

export default StackedCards;
