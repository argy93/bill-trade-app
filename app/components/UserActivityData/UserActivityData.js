import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

// eslint-disable-next-line react/prefer-stateless-function
class UserActivityData extends React.Component {
  render() {
    const { following, followers, winRate } = this.props;

    return (
      <div className="c-user-activity-data">
        <div className="c-user-activity-data__item">
          <span className="label">Following</span>
          <span className="count">{following}</span>
        </div>
        <div className="c-user-activity-data__item">
          <span className="label">Followers</span>
          <span className="count">{followers}</span>
        </div>
        <div className="c-user-activity-data__item">
          <span className="label">Win rate</span>
          <span className="count">{winRate}%</span>
        </div>
      </div>
    );
  }
}

UserActivityData.propTypes = {
  following: PropTypes.number,
  followers: PropTypes.number,
  winRate: PropTypes.number
};

export default UserActivityData;
