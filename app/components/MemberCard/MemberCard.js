import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import { Link } from 'react-router-dom';
import DownArrowIcon from '../Icons/DownArrowIcon';
import SubscribersIcon from '../Icons/SubscribersIcon';
import {
  separateNum,
  formattingNumbersByThousands
} from '../../utils/helpers';
import ColorCounter from '../../utils/ColorCounter';
import ChartWithControls from '../ChartWithControls';
import ProgressCircle from '../ProgressCircle';

class MemberCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.chartHeight = 125;
  }

  render() {
    const {
      id,
      name,
      photo,
      stats: {
        rank,
        winRate,
        trades,
        averageTime,
        maxLoss,
        tradingType,
        dailyProfit,
        investorsProfit,
      },
      finance: {
        balance: {
          usd: balanceUsd,
        },
        profit: {
          usd: profitUsd,
        },
      },
      tags: {
        tagsMain,
        tagsSecond,
      },
      percentTransactions,
      transactions
    } = this.props;

    this.colorCounter = new ColorCounter();

    return (
      <div className="c-members-card">
        <div className="c-members-card__content">
          <div className="c-members-card__left">
            <div className="c-members-card__top">
              <div className="c-members-card__column">
                <div className="c-members-card__photo-wrap">
                  <div className="c-members-card__photo">
                    <img src={photo} alt="" />
                  </div>
                  <div className="c-members-card__rank">{rank}</div>
                </div>
                <div className="c-members-card__name">{name}</div>
              </div>
              <div className="c-members-card__column">
                <ProgressCircle
                  containerClass={'c-progress-circle'}
                  size={78}
                  percent={winRate}
                  borderWidth={6}
                  borderColor={'#20AD65'}
                  progressBarContent={() => (
                    <Fragment>
                      <div className="c-progress-circle__percent">{winRate}%</div>
                      <div className="c-progress-circle__sum">
                        {formattingNumbersByThousands(balanceUsd)}
                      </div>
                    </Fragment>
                  )}
                />
                <div className="c-members-card__features">
                  <div className="c-members-card__features-tags">
                    {tagsMain.map((tag) => (<span key={tag} title={tag}>{tag[0]}</span>))}
                    {tagsSecond.map((tag) => (<span className="is-secondary" key={tag} title={tag}>{tag[0]}</span>))}
                  </div>
                  <div className="c-members-card__features-subscribers">
                    <SubscribersIcon />
                    241
                  </div>
                </div>
              </div>
            </div>
            <div className="c-members-card__bottom">
              <div className="c-members-card__income">
                <div className="c-members-card__income-heading">All time income</div>
                <b>${separateNum(profitUsd)}</b>
                <span> | </span>
                <p>{winRate}%</p>
              </div>
            </div>
          </div>
          <div className="c-members-card__main">
            <div className="c-members-card__top">
              <ChartWithControls
                height={this.chartHeight}
                percentTransactions={percentTransactions}
                transactions={transactions}
                chartTitle
              />
            </div>
            <div className="c-members-card__bottom">
              <div className="c-members-card__info-item">
                <p>Investors profit</p>
                <b>${investorsProfit}</b>
              </div>
              <div className="c-members-card__info-item">
                <p>Daily profit</p>
                <b>~ ${dailyProfit}</b>
              </div>
              <div className="c-members-card__info-item">
                <p>Trades</p>
                <b>{trades}</b>
              </div>
              <div className="c-members-card__info-item">
                <p>Max loss</p>
                <b>${maxLoss}</b>
              </div>
              <div className="c-members-card__info-item">
                <p>Winrate</p>
                <b>{winRate}%</b>
              </div>
              <div className="c-members-card__info-item">
                <p>Average time</p>
                <b>{averageTime}H</b>
              </div>
              <div className="c-members-card__info-item">
                <p>Trading type</p>
                <b>{tradingType}</b>
              </div>
            </div>
          </div>
        </div>
        <Link to={`/top-members/${id}`} className="c-members-card__subscribe">
          <DownArrowIcon
            width={'30px'}
            height={'30px'}
          />
        </Link>
      </div>
    );
  }
}

MemberCard.propTypes = {};

export default MemberCard;
