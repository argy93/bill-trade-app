/**
 *
 * Title
 *
 * This component is the skeleton of standart title
 * parameter: @size string: H1, H2, H3, H4, H5, H6
 * parameter: @weight required string: bold
 * parameter: @children required string
 * example <Title size="H4">Any text put here</Title>
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';

const Title = ({ size, weight = 'normal', children }) => (
  <div className={`c-title ${size} ${weight}`}>
    { children }
  </div>
);

Title.propTypes = {
  size: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  weight: PropTypes.string,
};

export default Title;
