import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import SVG from 'svg.js';
import './style.scss';
import RenderPipeline from '../../utils/RenderPipeline';

class ChartPolygon extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.domComponent = createRef();
    this.domSvg = createRef();
    this.state = {
      history: [],
    };
  }

  componentDidMount() {
    this.drawChart();
  }

  componentDidUpdate() {

  }

  static setCorrectOrder(items) {
    return [...items.slice(2), ...items.slice(0, 2)];
  }

  cleanSvg() {
    SVG(this.domSvg.current)
      .clear();
  }

  drawChart() {
    if (!this.domSvg || !this.domComponent) return;

    this.cleanSvg();

    const { items, min, max } = this.props;
    const length = max - min;
    const size = this.domComponent.current.clientWidth;
    const center = size / 2;
    const renderPipeline = new RenderPipeline();
    const history = [];
    const sortedItems = ChartPolygon.setCorrectOrder(items);
    this.chart = SVG(this.domSvg.current);
    this.chart.size(size, size);
    this.chart.viewbox({
      x: -5,
      y: -5,
      width: size + 5,
      height: size + 5,
    });

    let i;
    let c = 1;
    const coveringPoints = [];

    for (i = 0; i < 10; i += 1) {
      const radius = ((size / 2) / 10) * c;
      const points = [];

      for (let j = 0; j < sortedItems.length; j += 1) {
        const angle = ((360 / (sortedItems.length) * j)) * Math.PI / 180;
        const pointX = radius * Math.cos(angle) + center;
        const pointY = radius * Math.sin(angle) + center;

        points.push([pointX, pointY]);

        if (i === 9) {
          const outRadius = ((size / 2) / 10) * (c + 3);

          history.push({
            angle: 360 / (sortedItems.length) * j,
            position: {
              left: outRadius * Math.cos(angle) + center,
              top: outRadius * Math.sin(angle) + center,
            },
          });

          renderPipeline.add(() => {
            // draw lines
            const line = this.chart.line([[center, center], [pointX, pointY]]);

            line.attr({
              fill: 'transparent',
              stroke: '#E9F0FC',
              'stroke-width': 1,
            });
          }, 1);

          const pointRadius = (center / 100) * (length / 100 * sortedItems[j].value);
          const cx = pointRadius * Math.cos(angle) + center;
          const cy = pointRadius * Math.sin(angle) + center;

          coveringPoints.push([cx, cy]);

          renderPipeline.add(() => {
            // draw points
            const point = this.chart.circle(9);

            point.attr({
              cx,
              cy,
              stroke: '#E9F0FC',
              'stroke-width': '1',
              fill: '#1D62C9',
            });
          }, 3);
        }
      }

      renderPipeline.add(() => {
        const polygon = this.chart.polygon(points);

        polygon.attr({
          fill: 'transparent',
          stroke: '#E9F0FC',
          'stroke-width': 1,
        });
      }, 0);

      c += 1;
    }

    renderPipeline.add(() => {
      const polygon = this.chart.polygon(coveringPoints);

      polygon.attr({
        fill: '#E9F0FC',
        stroke: 'none',
        opacity: 0.5,
      });
    }, 2);

    renderPipeline.render();

    this.setState({
      history,
    });
  }

  render() {
    const { history } = this.state;
    const { items: unsorted } = this.props;

    const items = ChartPolygon.setCorrectOrder(unsorted);

    return (
      <div className="c-chart-polygon">
        <div
          className="c-chart-polygon__svg-wrap"
          ref={this.domComponent}
        >
          <svg
            ref={this.domSvg}
            width={'100%'}
          />
          <div className="c-chart-polygon__history">
            {history.length && items.map((item, count) => (
              <div
                key={item.name}
                className={`c-chart-polygon__item${history[count].angle > 225 && history[count].angle < 315 ? ' is-top' : ''}${history[count].angle > 45 && history[count].angle < 135 ? ' is-bottom' : ''}`}
                data-angle={history[count].angle.toString()}
                style={{
                  position: 'absolute',
                  ...history[count].position || {
                    top: 0,
                    left: 0,
                  },
                }}
              >
                <div className="c-chart-polygon__item-inner">{item.name}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

ChartPolygon.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.number,
    }),
  ),
  min: PropTypes.number,
  max: PropTypes.number,
};

export default ChartPolygon;
