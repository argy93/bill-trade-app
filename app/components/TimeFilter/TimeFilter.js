import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import {
  format,
  differenceInDays,
} from 'date-fns';
import Select from 'react-dropdown-select';
import Calendar from 'react-calendar';
import InputMask from 'react-input-mask';

const day = 1000 * 3600 * 24;
const week = day * 7;
const month = day * 30;

// eslint-disable-next-line react/prefer-stateless-function
class TimeFilter extends React.Component {
  constructor(props) {
    super(props);

    const now = Date.now();

    this.options = [
      {
        id: 1,
        label: 'All period',
        value: 'All period',
        from: 0,
        to: now,
        validate: (from) => from === 0,
      },
      {
        id: 2,
        label: '1 day',
        value: '1 day',
        from: now - day,
        to: now,
        validate: (from) => from === now - day,
      },
      {
        id: 3,
        label: '1 week',
        value: '1 week',
        from: now - week,
        to: now,
        validate: (from) => from === now - week,
      },
      {
        id: 4,
        label: '2 weeks',
        value: '2 weeks',
        from: now - week * 2,
        to: now,
        validate: (from) => from === now - week * 2,
      },
      {
        id: 5,
        label: '1 month',
        value: '1 month',
        from: now - month,
        to: now,
        validate: (from) => from === now - month,
      },
      {
        id: 6,
        label: '3 month',
        value: '3 month',
        from: now - month * 3,
        to: now,
        validate: (from) => from === now - month * 3,
      },
      {
        id: 7,
        label: '6 month',
        value: '6 month',
        from: now - month * 6,
        to: now,
        validate: (from) => from === now - month * 6,
      },
      {
        id: 8,
        label: '1 year',
        value: '1 year',
        from: now - month * 12,
        to: now,
        validate: (from) => from === now - month * 12,
      },
    ];

    this.state = {
      activeCalendar: false,
      inputFrom: format(Date.now(), 'dd.MM.yyyy'),
      inputTo: format(Date.now(), 'dd.MM.yyyy'),
    };
  }

  onCalendarChange = () => {
    const { activeCalendar } = this.state;

    this.setState({
      activeCalendar: !activeCalendar,
    });
  };

  updateTimeInputs(from, to) {
    this.setState({
      inputFrom: format(from, 'dd.MM.yyyy'),
      inputTo: format(to, 'dd.MM.yyyy'),
    });
  }

  customDropdownRenderer(select) {
    const { options } = this;
    const {
      type, filterUpdate, from, to,
    } = this.props;
    const { inputFrom, inputTo, activeCalendar } = this.state;

    console.log(select);

    return (
      <div className="c-select-period">
        <div className="c-select-period__list">
          {options.map((item) => (
            <button
              type="button"
              className={`c-select-period__list-item${item.validate(from, to) ? ' is-active' : ''}`}
              key={item.value}
              onClick={() => {
                select.methods.addItem({
                  label: item.label,
                  value: item.value,
                });
                filterUpdate(item.from, item.to);
              }}
            >{item.value}
            </button>
          ))}
          {type === 'full' ? (
            <button type="button" className="c-select-period__list-item" onClick={this.onCalendarChange}>
              Custom period...
            </button>
          ) : ''}
        </div>
        {activeCalendar ? (
          <div className="c-calendar">
            <div className="c-calendar__container">
              <Calendar
                nextLabel={''}
                prevLabel={''}
                showNeighboringMonth={false}
                locale={'en-EN'}
                formatShortWeekday={(locale, date) => format(date, 'eeeee')}
                returnValue={'range'}
                selectRange
              />
            </div>
            <div className="c-calendar__navigation">
              <div className="c-calendar__navigation-inputs">
                <InputMask
                  name="dateFrom"
                  placeholder={'30.12.2019'}
                  value={inputFrom}
                  onChange={(e) => this.updateTimeInputs(e.target.value, inputTo)}
                  mask={'99.99.9999'}
                  maskChar={'_'}
                  alwaysShowMask={false}
                  formatChars={{ 9: '[0-9]' }}
                />
                <span className="separator" />
                <InputMask
                  name="date-to"
                  placeholder={'30.12.2019'}
                  value={inputTo}
                  onChange={(e) => this.updateTimeInputs(inputFrom, e.target.value)}
                  mask={'99.99.9999'}
                  maskChar={'_'}
                  alwaysShowMask={false}
                  formatChars={{ 9: '[0-9]' }}
                />
              </div>
              <button
                type="button"
                className="btn-primary"
              >Apply
              </button>
            </div>
          </div>
        ) : ''}
      </div>
    );
  }

  shortTimeFilterRender() {
    const { to = Date.now(), from, filterUpdate } = this.props;
    const now = Date.now();

    return (
      <div className="c-time-filter">
        <button
          type="button"
          className={differenceInDays(from, to) === 1 ? 'is-active' : null}
          onClick={() => {
            filterUpdate(now, now - day);
          }}
        >Day
        </button>
        <button
          type="button"
          className={differenceInDays(from, to) === 7 ? 'is-active' : null}
          onClick={() => {
            filterUpdate(now, now - week);
          }}
        >Week
        </button>
        <button
          type="button"
          className={differenceInDays(from, to) === 30 ? 'is-active' : null}
          onClick={() => {
            filterUpdate(now, now - month);
          }}
        >Month
        </button>
      </div>
    );
  }

  mainTimeFilterRender() {
    return (
      <Select
        dropdownRenderer={(select) => this.customDropdownRenderer(select)}
        className={'c-time-filter__period'}
        options={this.options}
        values={[this.options[0]]}
      />
    );
  }

  render() {
    const {
      type,
    } = this.props;

    return (
      type === 'short'
        ? this.shortTimeFilterRender()
        : this.mainTimeFilterRender()
    );
  }
}


TimeFilter.propTypes = {
  type: PropTypes.oneOf(['short', 'normal', 'full']),
  to: PropTypes.number,
  from: PropTypes.number,
  filterUpdate: PropTypes.func,
};

export default TimeFilter;
