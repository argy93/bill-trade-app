/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Switch = ({ isOn, handleToggle, onColor, name }) => (
  <>
    <input
      checked={isOn}
      onChange={handleToggle}
      name={name}
      className="react-switch-checkbox"
      id={name}
      type="checkbox"
    />
    <label
      style={{ background: isOn && onColor }}
      className="react-switch-label"
      htmlFor={name}
    >
      <span className={'react-switch-button'} />
    </label>
  </>
);

Switch.propTypes = {
  isOn: PropTypes.bool,
  handleToggle: PropTypes.func,
  onColor: PropTypes.string,
  name: PropTypes.string
};

export default Switch;
