import React from 'react';
import PropTypes from 'prop-types';
import SVG from 'svg.js';
import './style.scss';

// eslint-disable-next-line react/prefer-stateless-function
class ProgressCircle extends React.Component {
  constructor(props) {
    super(props);

    this.progressBar = React.createRef();
  }

  componentDidMount() {
    this.drewProgressCircle();
  }

  componentDidUpdate() {
    this.drewProgressCircle();
  }

  drewProgressCircle() {
    const {
      size,
      percent,
      borderWidth,
      borderColor
    } = this.props;

    const length = Math.PI * (size - borderWidth);
    const offset = length - (length / 100 * percent);

    if (this.progressBar) {
      this.draw = SVG(this.progressBar.current).size(size, size);

      this.backgroundCircle = this.draw.circle(size - borderWidth);
      this.progressCircle = this.draw.circle(size - borderWidth);

      this.backgroundCircle.attr({
        cx: size / 2,
        cy: size / 2,
        fill: 'transparent',
        stroke: '#F2F2F2',
        'stroke-width': borderWidth,
      });

      this.progressCircle.attr({
        cx: size / 2,
        cy: size / 2,
        fill: 'transparent',
        stroke: borderColor,
        'stroke-width': borderWidth,
        'stroke-dasharray': length,
        'stroke-dashoffset': offset,
      });
    }
  }

  render() {
    const {
      containerClass,
      progressBarContent
    } = this.props;

    return (
      <div className={containerClass}>
        <div className={`${containerClass}__round-chart`}>
          <svg ref={this.progressBar} />
        </div>
        {progressBarContent()}
      </div>
    );
  }
}

ProgressCircle.propTypes = {
  containerClass: PropTypes.string,
  size: PropTypes.number,
  percent: PropTypes.number,
  borderWidth: PropTypes.number,
  borderColor: PropTypes.string,
  progressBarContent: PropTypes.func
};

export default ProgressCircle;
