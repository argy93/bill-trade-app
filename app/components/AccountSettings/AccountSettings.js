import React from 'react';
import {
  toast
} from 'react-toastify';
import Input from '../Input';
import './style';


// eslint-disable-line react/prefer-stateless-function
export default class AccountSettings extends React.PureComponent {

  handleUpdate = () => {
    console.log('handling save');
  }

  render() {
    return (
      <div className="account-settings">
        <div className="head">
          <div className="title">
            <div className="text">Personal Data</div>
          </div>
        </div>
        <form className="flex-container">
          <div className="column">
            <Input
              index={1}
              labelStyles={{ color: '#1F2123', fontWeight: 600 }}
              disabled
              name="login"
              label="Username"
            >
            </Input>
            <Input
              index={1}
              labelStyles={{ color: '#1F2123', fontWeight: 600 }}
              disabled
              name="firstname"
              label="First name"
            >
            </Input>
          </div>
          <div className="column">
            <Input
              index={1}
              labelStyles={{ color: '#1F2123', fontWeight: 600 }}
              disabled
              name="email"
              label="E-mail"
            >
            </Input>
            <Input
              index={1}
              labelStyles={{ color: '#1F2123', fontWeight: 600 }}
              disabled
              name="lastname"
              label="Last name"
            >
            </Input>
          </div>
        </form>
        <div className="divider"></div>
        <div className="footer-container-default">
          <button onClick={this.handleUpdate} type="button" className="add-new">Update personal data</button>
        </div>
      </div>
    );
  }
}
