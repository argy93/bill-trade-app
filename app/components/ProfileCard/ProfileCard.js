import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import UserSubstrateIcon from '../Icons/UserSubstrateIcon';
import EditIcon from '../Icons/EditIcon';
import UserActivityData from '../UserActivityData';

class ProfileCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      photo,
      name,
      currency,
      tagsMain,
      tagsSecond,
      following,
      followers,
      winRate,
      userType
    } = this.props;

    return (
      <div className="c-profile-card">
        <div className="c-profile-card__info">
          <div className="c-profile-card__img">
            {<img src={photo} alt="" /> || (
              <UserSubstrateIcon
                width={'55px'}
                height={'55px'}
              />
            )}
          </div>
          <div className="c-profile-card__content">
            <div className="c-profile-card__content-title">
              <span className="name">{name}</span>
              {userType === 'member' ? (
                <span className="currency">{currency}</span>
              ) : null}
            </div>
            {userType === 'member' ? (
              <a href="#" className="c-profile-card__subscribe-edit">
                <EditIcon />
                <span>Edit subscription</span>
              </a>
            ) : null}
            <div className="c-profile-card__content-tags">
              <div className="is-main">
                {tagsMain.map((item) => <span key={item}>{item}</span>)}
              </div>
              <div className="is-second">
                {tagsSecond.map((item) => <span key={item}>{item}</span>)}
              </div>
            </div>
          </div>
        </div>
        {userType === 'own' ? (
          <UserActivityData
            following={following}
            followers={followers}
            winRate={winRate}
          />
        ) : null}
        {userType === 'member' ? (
          <div className="c-profile-card__unsubscribe">
            <button type="button">Unsubscribe</button>
          </div>
        ) : null}
      </div>
    );
  }
}

ProfileCard.propTypes = {
  photo: PropTypes.string,
  name: PropTypes.string,
  currency: PropTypes.string,
  tagsMain: PropTypes.arrayOf(PropTypes.string),
  tagsSecond: PropTypes.arrayOf(PropTypes.string),
  following: PropTypes.number,
  followers: PropTypes.number,
  winRate: PropTypes.number,
  userType: PropTypes.oneOf(['own', 'member'])
};

export default ProfileCard;
