import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
  AccordionItemState,
} from 'react-accessible-accordion';
import DoubleDownArrowsIcon from '../Icons/DoubleDownArrowsIcon';

// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';
import ChartPolygon from '../ChartPolygon';
import CardShort from '../CardShort';
import StatsList from '../StatsList';
import ProgressCircle from '../ProgressCircle';

class Stats extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.windowWidth = window.innerWidth;
  }

  svgSize() {
    let radius = 0;

    if (this.windowWidth < 1100 && this.windowWidth >= 960) {
      radius = 130;
    } else if (this.windowWidth < 400
      || (this.windowWidth < 960 && this.windowWidth >= 576)) {
      radius = 200;
    } else {
      radius = 165;
    }

    return radius;
  }

  render() {
    const {
      rank,
      maxLoss,
      averageTime,
      trades,
      tradingType,
      strides,
    } = this.props;

    return (
      <div className="c-stats">
        <div className="c-stats__title">Your stats</div>
        <div className="c-stats__row">
          <ProgressCircle
            containerClass={'c-stats-rank'}
            size={this.svgSize()}
            percent={rank * 10}
            borderWidth={10}
            borderColor={'#A7C2F1'}
            progressBarContent={() => (
              <Fragment>
                <div className="c-stats-rank__title">BT rank</div>
                <div className="c-stats-rank__number">{rank}</div>
              </Fragment>
            )}
          />
          <div className="c-stats__info">
            <div className="c-stats__info-row">
              <div className="c-stats__item">
                <div className="c-stats__item-title">
                  Max loss
                </div>
                <div className="c-stats__item-value">
                  $ {maxLoss}
                </div>
              </div>
              <div className="c-stats__item">
                <div className="c-stats__item-title">
                  Avg. time
                </div>
                <div className="c-stats__item-value">
                  {averageTime}h
                </div>
              </div>
            </div>
            <div className="c-stats__info-row">
              <div className="c-stats__item">
                <div className="c-stats__item-title">
                  Trades
                </div>
                <div className="c-stats__item-value">
                  {trades}
                </div>
              </div>
              <div className="c-stats__item">
                <div className="c-stats__item-title">
                  Trading type
                </div>
                <div className="c-stats__item-value">
                  {tradingType}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Accordion
          allowMultipleExpanded
          allowZeroExpanded
        >
          <AccordionItem>
            <AccordionItemPanel>
              <AccordionItemState>
                {(state) => (
                  <Fragment>
                    {state.expanded ? (
                      <div className="c-stats__chart-wrap">
                        <ChartPolygon
                          items={strides}
                          max={100}
                          min={0}
                        />
                      </div>
                    ) : ''}
                    <CardShort
                      main="User deposit x BT rank = Max. subscription amount, USD"
                      additional="92 300 x 9.9 = 920 000"
                    />
                    <StatsList items={strides} />
                  </Fragment>
                )}
              </AccordionItemState>
            </AccordionItemPanel>

            <AccordionItemHeading>
              <AccordionItemButton>
                <span>Expand for details</span>
                <DoubleDownArrowsIcon />
              </AccordionItemButton>
            </AccordionItemHeading>
          </AccordionItem>
        </Accordion>
      </div>
    );
  }
}

Stats.propTypes = {
  rank: PropTypes.number,
  maxLoss: PropTypes.number,
  averageTime: PropTypes.number,
  trades: PropTypes.number,
  tradingType: PropTypes.string,
  strides: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  })),
};

export default Stats;
