import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import isEqual from 'react-fast-compare';
import ReactTooltip from 'react-tooltip';
import {
  Accordion,
  AccordionItem, AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemState,
} from 'react-accessible-accordion';
import ColorCounter from '../../utils/ColorCounter';
import ChartRound from '../ChartRound';
import PortfolioHistory from '../PortfolioHistory';
import DoubleDownArrowsIcon from '../Icons/DoubleDownArrowsIcon';

class Portfolio extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidUpdate(prevProps) {
    const {
      items,
    } = this.props;

    if (!isEqual(items, prevProps.items)) {
      ReactTooltip.rebuild();
    }
  }

  getValuesArray() {
    const { items } = this.props;
    const result = [];

    items.filter((item) => item.selected)
      .forEach((item) => item.stats.forEach((itemStats) => {
        result.push({
          value: itemStats.value,
        });
      }));

    return result;
  }

  getOtherValue() {
    const {
      items,
    } = this.props;

    let summary = 0;

    items
      .filter((item) => !item.selected)
      .forEach((item) => {
        item.stats.forEach((statsItem) => {
          summary += statsItem.value;
        });
      });

    return summary;
  }

  render() {
    const {
      items,
      toggleSelectPlatform,
      membersPage
    } = this.props;
    const colorCounter = new ColorCounter(4);

    return (
      <div className="c-portfolio">
        <div className="c-portfolio__row">
          <div className="c-portfolio__title c-content-card-title">My portfolio</div>
          <div className="c-portfolio__2col">
            <div className="c-portfolio__chart">
              <ChartRound
                items={[
                  ...this.getValuesArray(),
                ]}
                max={this.getOtherValue()}
              />
            </div>
            <div className="c-portfolio__stats">
              {
                items
                  .filter((item) => item.selected)
                  .map((selectedPlatform) => selectedPlatform.stats.map((item) => (
                    <div
                      className="c-portfolio__stats-item"
                      data-tip={selectedPlatform.platformName}
                      key={`${selectedPlatform.platformName}_${item.currencyName}`}
                    >
                      <div className="c-portfolio__stats-currency">
                        <span
                          style={{
                            display: 'block',
                            width: 10,
                            height: 10,
                            background: colorCounter.getNextColor(),
                          }}
                        />
                        {item.currencyName}
                      </div>
                      <div className="c-portfolio__stats-value">
                        {item.value}
                      </div>
                    </div>
                  )))
              }
              <div className="c-portfolio__stats-item is-other">
                <div className="c-portfolio__stats-currency">
                  <span
                    style={{
                      display: 'block',
                      width: 10,
                      height: 10,
                      background: '#E0E0E0',
                    }}
                  />
                  Other
                </div>
                <div className="c-portfolio__stats-value">
                  {this.getOtherValue()
                    .toFixed(3)
                    .replace(/[.,]000$/, '')}
                </div>
              </div>
            </div>
          </div>
          <div className="c-portfolio__title c-content-card-title">Linked exchange platforms</div>
          <div className="c-portfolio__platforms">
            {
              items.map((item) => (
                <button
                  type="button"
                  key={item.platformName}
                  onClick={() => {
                    toggleSelectPlatform(item.platformName);
                  }}
                  className={`c-portfolio__platform${item.selected ? ' is-selected' : ''}`}
                >
                  <span>
                    <svg width="10" height="7" viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M1.37988 2.72016L4.23988 5.58016L8.65988 1.16016" stroke="white" strokeWidth="2" />
                    </svg>
                  </span> {item.platformName}
                </button>
              ))
            }
          </div>
        </div>
        {membersPage ? <PortfolioHistory />
          : (
            <Fragment>
              <Accordion
                allowMultipleExpanded
                allowZeroExpanded
              >
                <AccordionItem>
                  <AccordionItemPanel>
                    <AccordionItemState>
                      {(state) => (
                        <PortfolioHistory />
                      )}
                    </AccordionItemState>
                  </AccordionItemPanel>

                  <AccordionItemHeading>
                    <AccordionItemButton>
                      <span>Live trading history</span>
                      <DoubleDownArrowsIcon />
                    </AccordionItemButton>
                  </AccordionItemHeading>
                </AccordionItem>
              </Accordion>
              <ReactTooltip />
            </Fragment>
          ) }
      </div>
    );
  }
}

Portfolio.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      platformName: PropTypes.string,
      selected: PropTypes.bool,
      stats: PropTypes.arrayOf(PropTypes.shape({
        currencyName: PropTypes.string,
        value: PropTypes.number,
      })),
    }),
  ),
  toggleSelectPlatform: PropTypes.func,
  membersPage: PropTypes.bool
};

export default Portfolio;
