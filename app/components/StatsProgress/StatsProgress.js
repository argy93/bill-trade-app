import React from 'react';
import PropTypes from 'prop-types';

import './style';

const StatsProgress = ({ children, progress }) => (
  <div className="stats-progress">
    <div className="column">{children}</div>
    <div className="column percent">{progress}%</div>
    <div className="progress" style={{ width: `${progress}%` }}></div>
  </div>
);

StatsProgress.propTypes = {
  children: PropTypes.any,
  progress: PropTypes.number,
};

export default StatsProgress;
