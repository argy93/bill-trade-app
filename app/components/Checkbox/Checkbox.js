/**
 *
 * Input
 *
 * This component is the skeleton of standart input
 * example <Input name="Password" type="password"></Input>
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';

const Checkbox = ({
  name,
  defaultChecked = false,
  label,
  index,
  // eslint-disable-next-line no-console
  onChange = console.log,
}) => (
  <label htmlFor={name} className="container">
    <input
      tabIndex={index}
      id={name}
      name={name}
      type="checkbox"
      defaultChecked={defaultChecked}
      onChange={() => {
        onChange(defaultChecked);
      }}
    />
    <span className="checkmark"></span>
    <span className="label">{label}</span>
  </label>
);

Checkbox.propTypes = {
  label: PropTypes.string,
  defaultChecked: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  index: PropTypes.number,
};

export default Checkbox;
