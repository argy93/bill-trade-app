/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-array-index-key */
/**
 *
 * APIkeys tab content component
 *
 */

import React from 'react';
import { timer } from 'rxjs';
import Select from 'react-select';
import formatDate from 'utils/formatDate';
import {
  toast
} from 'react-toastify';
import SettingsService from '../../containers/SettingsPage/service';
import ButtonSmall from '../ButtonSmall';
import Input from '../Input';
import Done from '../Icons/DoneIcon';
import PlusIcon from '../Icons/PlusIcon';
import './style';


// eslint-disable-line react/prefer-stateless-function
export default class APIkeys extends React.PureComponent {
  state = {
    dataList$: SettingsService.APIkeys$.subscribe(({ data }) => {
      this.setState({
        dataList: data,
      });
    }),
    dataList: [],
    headersList: [
      'Stock',
      'API key',
      'API secret key',
      'Date Added',
      'Actions',
      'Comments'
    ],
    isNewOpened: false,
    isSuccess: false,
    stock: '',
    api_key: '',
    api_secret: '',
    comment: '',
  };

  componentDidMount() {
    SettingsService.listAPIkeys();
  }

  componentWillUnmount() {
    const { dataList$ } = this.state;
    dataList$.unsubscribe();
  }

  renderTableHead = (data) => (
    <div className="table-head">
      {
        data.map((value, key) => (
          <div
            className="head-cell"
            key={key}
          >
            { value }
          </div>
        ))
      }
    </div>
  )

  renderTableRows = (data) => (
    <div className="table-rows">
      {
        data.map((value) => {
          const date = formatDate(value.created_at);
          return (<div className="row" key={value.id}>
            <div className="item">{ value.stock }</div>
            <div className="item">{ value.api_key }</div>
            <div className="item">{ value.api_secret }</div>
            <div className="item">
              {date}
            </div>
            <div className="item">
              <ButtonSmall type="contained" onClick={() => SettingsService.deleteAPIkey(value.id)}>Delete</ButtonSmall>
            </div>
            <div className="item">{ value.comments || 'empty' }</div>
          </div>
          );
        })
      }
    </div>
  )

  handleNew = () => {
    const {
      isNewOpened
    } = this.state;
    this.setState({
      isNewOpened: !isNewOpened
    });
  }

  handleNewCreate = () => {
    const {
      isSuccess,
      stock,
      api_key,
      api_secret,
    } = this.state;

    SettingsService.createAPIkey(api_key, api_secret, stock)
      .then(() => {
        SettingsService.listAPIkeys();
        this.setState({
          isSuccess: !isSuccess
        }, this.handleCreateFinish);
      })
      .catch(() => toast('Some error occured, please try again.'));
  }

  handleCreateFinish = () => {
    const source = timer(2000);
    const subscriber = source.subscribe(() => {
      this.setState({
        isSuccess: false,
        isNewOpened: false,
      });
      subscriber.unsubscribe();
    });
  }

  handleInputChange = (value, name) => {
    this.setState({
      [name]: value,
    });
  }

  handleStockChange = ({ value }) => {
    this.setState({
      stock: value
    });
  }

  render() {
    const {
      dataList,
      headersList,
      isNewOpened,
      isSuccess
    } = this.state;

    const options = [{
      value: 'BITMEX',
      label: 'BITMEX'
    },
    {
      value: 'BINANCE',
      label: 'BINANCE'
    }];

    const tableHeader = this.renderTableHead(headersList);
    const tableBody = this.renderTableRows(dataList);
    const newAPIkey = (
      <div className="new-api-key-container">
        <div className="new-api-key-title">Adding new key</div>
        <div className="new-api-form">
          <div className="column">
            <div className="stock-select">
              <div className="label">Stock</div>
              <Select id="stock" name="stock" aria-label="stock" aria-labelledby="stock" options={options} onChange={this.handleStockChange} />
            </div>
            <Input labelStyles={{ color: '#1F2123', fontWeight: 600 }} onChange={this.handleInputChange} type="text" name="api_key" label="API Key" />
            <Input labelStyles={{ color: '#1F2123', fontWeight: 600 }} onChange={this.handleInputChange} type="text" name="api_secret" label="Secret" />
          </div>
          <div className="column">
            <Input labelStyles={{ color: '#1F2123', fontWeight: 600 }} onChange={this.handleInputChange} type="textarea" name="comment" label="Comment" />
            <button onClick={this.handleNewCreate} type="button" className="add-new">Add new key</button>
          </div>
        </div>
      </div>
    );
    const successMessage = (
      <div className="success-message">
        <span className="icon">
          <Done />
        </span>
        <span className="text">
          New API key <b>fgcdhgvJGHCVHGcjqshjv763rhjndGHGJHB</b> successfully added!
        </span>
      </div>
    );
    return (
      <div className="api-key-container">
        <div className="head">
          <div className="title">
            <span className="text">API keys</span>
            { !isNewOpened
              ? <ButtonSmall
                type="contained"
                color="#20AD65"
                icon={<PlusIcon width="17px" height="17px" />}
                onClick={this.handleNew}
              >
                Add new key
              </ButtonSmall>
              : ''}
          </div>
          {isNewOpened && !isSuccess ? newAPIkey : ''}
          {isNewOpened && isSuccess ? successMessage : ''}
        </div>

        <div className="content">
          <div className="table">
            {tableHeader}
            {tableBody}
          </div>
        </div>
      </div>
    );
  }
}

APIkeys.propTypes = {

};
