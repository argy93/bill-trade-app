import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import { separateNum } from '../../utils/helpers';
import UserInfo from '../UserInfo';
import TimeFilter from '../TimeFilter';

// eslint-disable-next-line react/prefer-stateless-function
class TopTradesList extends React.Component {
  render() {
    const {
      trades,
      lossPeriod,
      tradesPeriod,
      filterType,
      changeLossPeriod,
      changeTradesPeriod,
      changeFilterType,
    } = this.props;

    const mainFrom = filterType === 'trades' ? tradesPeriod.from : lossPeriod.from;
    const mainTo = filterType === 'trades' ? tradesPeriod.to : lossPeriod.to;
    const mainPeriodChange = filterType === 'trades' ? changeTradesPeriod : changeLossPeriod;

    return (
      <div className="c-top-trade-list">
        <div className="c-top-trade-list__header">
          <span
            onClick={() => changeFilterType('trades')}
            className={filterType === 'trades' ? 'is-active' : ''}
          >Top trades
          </span>
          <span
            onClick={() => changeFilterType('loss')}
            className={filterType === 'loss' ? 'is-active' : ''}
          >Top loss
          </span>
        </div>
        <div className="c-top-trade-list__body">
          <div className="c-top-trade-list__nav">
            <span className="c-top-trade-list__nav-label">Period:</span>
            <TimeFilter
              type={'short'}
              from={mainFrom}
              to={mainTo}
              filterUpdate={mainPeriodChange}
            />
          </div>
          {
            trades.map((item) => (
              <div className="c-top-trade-list__body-item" key={`trader${item.id}`}>
                <div className="user-info">
                  <UserInfo
                    width={'19px'}
                    height={'19px'}
                  />
                  <div className="user-info__content">
                    <span className="user-info__content-title">
                      {item.name}
                    </span>
                    <span className="user-info__content-followers">
                      {item.followers} Followers
                    </span>
                  </div>
                </div>
                <div className="user-wallet">
                  <span
                    className="user-wallet__price"
                    style={filterType === 'loss' ? { color: 'red' } : null}
                  >
                    {separateNum(item.price)}
                  </span>
                  <span className="user-wallet__crypto">
                    {item.crypto}
                  </span>
                </div>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

TopTradesList.propTypes = {
  trades: PropTypes.arrayOf(
    PropTypes.shape({
      photo: PropTypes.string,
      name: PropTypes.string,
      timestamp: PropTypes.number,
      crypto: PropTypes.string,
      price: PropTypes.number,
      followers: PropTypes.number,
    }),
  ),
  filterType: PropTypes.oneOf(['trades', 'loss']),
  tradesPeriod: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  lossPeriod: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  changeFilterType: PropTypes.func,
  changeTradesPeriod: PropTypes.func,
  changeLossPeriod: PropTypes.func,
};

export default TopTradesList;
