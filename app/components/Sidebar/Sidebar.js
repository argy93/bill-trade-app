import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import Logo from './images/logo.svg';
import MobileLogo from './images/mobile-logo.svg';
import Navigation from '../Navigation';

// eslint-disable-next-line react/prefer-stateless-function
class Sidebar extends React.Component {
  render() {
    const { expand } = this.props;

    const windowWidth = window.innerWidth;
    const logo = (windowWidth >= 1025 || windowWidth < 768) ? Logo : MobileLogo;

    return (
      <div className={`c-sidebar ${(expand ? ' is-active' : '')}`}>
        <div className="c-sidebar__logo">
          <img src={logo} alt="" />
        </div>
        <Navigation />
      </div>
    );
  }
}

Sidebar.propTypes = {
  expand: PropTypes.bool
};

export default Sidebar;
