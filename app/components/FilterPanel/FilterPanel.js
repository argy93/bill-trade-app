import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-dropdown-select';
import TimeFilter from '../TimeFilter';
import './style.scss';
import SearchIcon from '../Icons/SearchIcon';
import FilterIcon from '../Icons/FilterIcon';
import Switch from '../Switch';
import Checkbox from '../Checkbox';

class FilterPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      advanced: false,
      search: '',
    };

    this.searchInputChange = this.searchInputChange.bind(this);
  }

  searchInputChange(event) {
    this.setState({
      search: event.target.value,
    });
  }

  toggleAdvanced() {
    const { advanced } = this.state;

    this.setState({
      advanced: !advanced,
    });
  }

  render() {
    const {
      sortUpdate, searchUpdate, timePeriodUpdate, currencyUpdate, membersTypeUpdate,
      sort, from, to, currency, membersType, advanced: advancedFilter, advancedToggle,
    } = this.props;

    const {
      search: searchState,
      advanced,
    } = this.state;

    const allMembersCustomClass = sort === 'all' ? ' is-active' : '';
    const subscribedCustomClass = sort === 'subscribed' ? ' is-active' : '';
    const usdCustomClass = currency === 'usd' ? ' is-active' : '';
    const btcCustomClass = currency === 'btc' ? ' is-active' : '';
    const ethCustomClass = currency === 'eth' ? ' is-active' : '';

    return (
      <div className="c-filter-panel">
        <div className="c-filter-panel__main">
          <div className="c-filter-panel__sort">
            <div className="c-filter-panel__heading">Sort members list:</div>
            <button className={allMembersCustomClass} type="button" onClick={() => sortUpdate('all')}>All members
            </button>
            <span> | </span>
            <button
              className={subscribedCustomClass}
              type="button"
              onClick={() => sortUpdate('subscribed')}
            >Subscribed
            </button>
          </div>
          <div className="c-filter-panel__search">
            <input
              value={searchState}
              onChange={this.searchInputChange}
              type="text"
              placeholder="Search for members..."
            />
            <button type="button" onClick={() => searchUpdate(searchState)}>
              <SearchIcon />
            </button>
          </div>
          <div className="c-filter-panel__time">
            <div className="c-filter-panel__heading">Time period:</div>
            <TimeFilter
              type={'full'}
              from={from}
              to={to}
              filterUpdate={timePeriodUpdate}
            />
          </div>
          <div className="c-filter-panel__currency">
            <div className="c-filter-panel__heading">Members currency:</div>
            <div>
              <button className={usdCustomClass} type="button" onClick={() => currencyUpdate('usd')}>USD</button>
              <button className={btcCustomClass} type="button" onClick={() => currencyUpdate('btc')}>BTC</button>
              <button className={ethCustomClass} type="button" onClick={() => currencyUpdate('eth')}>ETH</button>
            </div>
          </div>
          <div className="c-filter-panel__members-type">
            <div className="c-filter-panel__heading">Members type:</div>
            <Select
              className={'c-time-filter__period'}
              options={[
                {
                  label: 'All types',
                  value: 'all',
                },
                {
                  label: 'Traders',
                  value: 'trader',
                },
                {
                  label: 'Analysts',
                  value: 'analyst',
                },
                {
                  label: 'Investors',
                  value: 'investors',
                },
              ]}
              values={[{
                label: membersType,
                value: membersType,
              }]}
              onChange={(e) => membersTypeUpdate(e[0].value)}
            />
          </div>
          <div className="c-filter-panel__advanced">
            <button type="button" onClick={() => this.toggleAdvanced()}><FilterIcon /></button>
          </div>
        </div>
        {advanced ? (
          <div className="c-filter-panel__drop">
            <div className="c-filter-panel__blue-title">
              <FilterIcon width={'15px'} height={'16px'} /> Advanced filters
            </div>
            <div className="c-filter-panel__row">
              <div className="c-filter-panel__col">
                <div className="c-filter-panel__heading">Members’ card display data</div>
                <div className="c-filter-panel__field-list">
                  <div className="c-filter-panel__field-list-column">
                    <div>
                      <Switch
                        isOn={advancedFilter.investorsProfit}
                        handleToggle={() => advancedToggle('investorsProfit')}
                      />
                      <span className="react-switch-text">Investors profit</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.dailyProfit}
                        handleToggle={() => advancedToggle('dailyProfit')}
                      />
                      <span className="react-switch-text">Daily profit</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.trades}
                        handleToggle={() => advancedToggle('trades')}
                      />
                      <span className="react-switch-text">Trades</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.maxLoss}
                        handleToggle={() => advancedToggle('maxLoss')}
                      />
                      <span className="react-switch-text">Max loss</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.maxLoss}
                        handleToggle={() => advancedToggle('winrate')}
                      />
                      <span className="react-switch-text">Winrate</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.averageTime}
                        handleToggle={() => advancedToggle('averageTime')}
                      />
                      <span className="react-switch-text">Average time</span>
                    </div>
                  </div>
                  <div className="c-filter-panel__field-list-column">
                    <div>
                      <Switch
                        isOn={advancedFilter.maximumDeclaredLoss}
                        handleToggle={() => advancedToggle('maximumDeclaredLoss')}
                      />
                      <span className="react-switch-text">Maximum declared loss</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.averageProfit}
                        handleToggle={() => advancedToggle('averageProfit')}
                      />
                      <span className="react-switch-text">Average profit / trade</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.averageTrades}
                        handleToggle={() => advancedToggle('averageTrades')}
                      />
                      <span className="react-switch-text">Average trades per day</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.averageOrderSize}
                        handleToggle={() => advancedToggle('averageOrderSize')}
                      />
                      <span className="react-switch-text">Average order size</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.profitLossRatio}
                        handleToggle={() => advancedToggle('profitLossRatio')}
                      />
                      <span className="react-switch-text">Profit/loss ratio</span>
                    </div>
                    <div>
                      <Switch
                        isOn={advancedFilter.minimumFollowingDeposit}
                        handleToggle={() => advancedToggle('minimumFollowingDeposit')}
                      />
                      <span className="react-switch-text">Minimum following deposit</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="c-filter-panel__col">
                <div className="c-filter-panel__heading">Type of trading system</div>
                <div className="c-filter-panel__field-list">
                  <div className="c-filter-panel__field-list-column">
                    <div>
                      <Checkbox
                        name={'indicators'}
                        defaultChecked={advancedFilter.indicators}
                        onChange={() => advancedToggle('indicators')}
                      />
                      <span className="trading-system__text">Indicators</span>
                    </div>
                    <div>
                      <Checkbox
                        name={'technicalAnalysis'}
                        defaultChecked={advancedFilter.technicalAnalysis}
                        onChange={() => advancedToggle('technicalAnalysis')}
                      />
                      <span className="trading-system__text">Technical analysis</span>
                    </div>
                    <div>
                      <Checkbox
                        name={'artificialIntelligence'}
                        defaultChecked={advancedFilter.artificialIntelligence}
                        onChange={() => advancedToggle('artificialIntelligence')}
                      />
                      <span className="trading-system__text">Artificial intelligence</span>
                    </div>
                    <div>
                      <Checkbox
                        name={'artificialIntelligence'}
                        defaultChecked={advancedFilter.artificialIntelligence}
                        onChange={() => advancedToggle('artificialIntelligence')}
                      />
                      <span className="trading-system__text">Artificial intelligence</span>
                    </div>
                    <div>
                      <Checkbox
                        name={'algorithmicTrading'}
                        defaultChecked={advancedFilter.algorithmicTrading}
                        onChange={() => advancedToggle('algorithmicTrading')}
                      />
                      <span className="trading-system__text">Algorithmic Trading</span>
                    </div>
                    <div>
                      <Checkbox
                        name={'fundamentalAnalysis'}
                        defaultChecked={advancedFilter.fundamentalAnalysis}
                        onChange={() => advancedToggle('fundamentalAnalysis')}
                      />
                      <span className="trading-system__text">Fundamental Analysis</span>
                    </div>
                    <div>
                      <Checkbox
                        name={'arbitrage'}
                        defaultChecked={advancedFilter.arbitrage}
                        onChange={() => advancedToggle('arbitrage')}
                      />
                      <span className="trading-system__text">Arbitrage</span>
                    </div>
                  </div>
                  <div className="c-filter-panel__field-list-column">
                    <div>
                      <Checkbox
                        name={'failureOfRmPolity'}
                        defaultChecked={advancedFilter.failureOfRmPolity}
                        onChange={() => advancedToggle('failureOfRmPolity')}
                      />
                      <span className="trading-system__text">Failure of RM polity</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

FilterPanel.propTypes = {
  sortUpdate: PropTypes.func,
  searchUpdate: PropTypes.func,
  timePeriodUpdate: PropTypes.func,
  currencyUpdate: PropTypes.func,
  membersTypeUpdate: PropTypes.func,
  sort: PropTypes.string,
  from: PropTypes.number,
  to: PropTypes.number,
  currency: PropTypes.string,
  membersType: PropTypes.string,
  advanced: PropTypes.object,
};

export default FilterPanel;
