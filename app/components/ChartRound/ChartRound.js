import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import SVG from 'svg.js';
import './style.scss';
import isEqual from 'react-fast-compare';
import RenderPipeline from '../../utils/RenderPipeline';
import ColorCounter from '../../utils/ColorCounter';

class ChartRound extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.domComponent = createRef();
    this.domSvg = createRef();
    this.colorCounter = new ColorCounter(4);
  }

  componentDidMount() {
    this.drawChart();
  }

  componentDidUpdate(prevProps) {
    const { items } = this.props;

    if (!isEqual(prevProps.items, items)) {
      this.drawChart();
    }
  }

  cleanSvg() {
    SVG(this.domSvg.current)
      .clear();
  }

  drawChart() {
    if (!this.domSvg || !this.domComponent) return;

    const { items, max } = this.props;
    const renderPipeline = new RenderPipeline();
    const size = this.domComponent.current.clientWidth;
    const length = Math.PI * (size - 30);
    const circleSize = size - 30;
    const strokeWidth = 30;

    let valuesSummary = max;
    for (let i = 0; i < items.length; i += 1) {
      valuesSummary += items[i].value;
    }

    const valuesSummaryPercent = valuesSummary / 100;

    let offsetSummary = 0;

    this.colorCounter.resetIterator();
    this.cleanSvg();

    this.chart = SVG(this.domSvg.current);
    this.chart.size(size, size);
    this.chart.viewbox({
      x: -5,
      y: -5,
      width: size + 5,
      height: size + 5,
    });

    const group = this.chart.group();
    const mask = this.chart.mask();

    mask.add(this.chart.circle(circleSize + strokeWidth)
      .attr({
        cx: size / 2,
        cy: size / 2,
        fill: 'white',
      }),
    );

    mask.add(this.chart.circle(circleSize - strokeWidth)
      .attr({
        cx: size / 2,
        cy: size / 2,
        fill: 'black',
      }),
    );

    group.circle(circleSize)
      .attr({
        cx: size / 2,
        cy: size / 2,
        fill: 'transparent',
        stroke: '#E0E0E0',
        'stroke-width': strokeWidth,
      });

    group.maskWith(mask);

    for (let i = items.length - 1; i >= 0; i -= 1) {
      const item = items[i];
      offsetSummary += (length / 100 * (item.value / valuesSummaryPercent));
      const offset = length - offsetSummary;
      const color = item.color || this.colorCounter.getNextColor();

      renderPipeline.add(() => {
        const circle = group.circle(circleSize);

        circle.attr({
          cx: size / 2,
          cy: size / 2,
          fill: 'transparent',
          stroke: color,
          'stroke-width': strokeWidth,
          'stroke-dasharray': length,
          'stroke-dashoffset': offset,
        });
      }, i, color);
    }

    renderPipeline.render();
  }

  render() {
    return (
      <div className="c-chart-round" ref={this.domComponent}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          style={{
            transform: 'rotate(-90deg)',
          }}
          ref={this.domSvg}
        />
      </div>
    );
  }
}

ChartRound.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      color: PropTypes.string,
      value: PropTypes.number,
    }),
  ),
  max: PropTypes.number,
};

export default ChartRound;
