import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class CardShort extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      main,
      additional,
    } = this.props;

    return (
      <div className="c-card-short">
        <div className="c-card-short__main">{main}</div>
        <div className="c-card-short__additional">{additional}</div>
      </div>
    );
  }
}

CardShort.propTypes = {
  main: PropTypes.string,
  additional: PropTypes.string,
};

export default CardShort;
