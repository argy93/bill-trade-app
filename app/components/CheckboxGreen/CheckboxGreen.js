/**
 *
 * Input
 *
 * This component is the skeleton of standart input
 * example <Input name="Password" type="password"></Input>
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';

const CheckboxGreen = ({
  name,
  defaultChecked = false,
  label,
  index,
  disabled = false,
  // eslint-disable-next-line no-console
  onChange = console.log,
}) => (
  <label htmlFor={name} className="green-checkbox-container">
    <input
      tabIndex={index}
      id={name}
      name={name}
      type="checkbox"
      disabled={disabled}
      defaultChecked={defaultChecked}
      onChange={() => {
        onChange(defaultChecked);
      }}
    />
    <span className="checkmark"></span>
    <span className="label">{label}</span>
  </label>
);

CheckboxGreen.propTypes = {
  label: PropTypes.string,
  defaultChecked: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  index: PropTypes.number,
  disabled: PropTypes.bool,
};

export default CheckboxGreen;
