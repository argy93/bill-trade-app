/**
 *
 * ButtonSmall
 *
 * This component is the skeleton of standart button
 * types: contained, outlined, text
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';

const ButtonSmall = ({
  type = 'contained',
  children,
  index,
  icon,
  color,
  // eslint-disable-next-line no-console
  onClick = console.log,
}) => (
  <button
    tabIndex={index}
    type="button"
    className={`button-small ${type}`}
    onClick={onClick}
    style={color ? { background: color } : {}}
  >
    {icon ? <span className="icon">{icon}</span> : ''}
    {children}
  </button>
);

ButtonSmall.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func,
  index: PropTypes.number,
  icon: PropTypes.any,
  color: PropTypes.string,
};

export default ButtonSmall;
