/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-array-index-key */
/**
 *
 * ProfitLose tab content component
 *
 */

// main imports
import React from 'react';
import ReactApexChart from 'react-apexcharts';
import ReactPaginate from 'react-paginate';

// components imports
import StackedCards from '../StackedCards';
import Dropdown from '../Dropdown';
import Checkbox from '../Checkbox';
import SubscriptionStats from '../SubscriptionStats';

// images imports
import visa from './Visa.svg';
import mastercard from './Mastercard.svg';
import btc from './btc.svg';
import tether from './tether-seeklogo.com.svg';

// style imports
import './style';


// eslint-disable-line react/prefer-stateless-function
export default class ProfitLose extends React.PureComponent {
  state = {
    location: [
      {
        id: 0,
        title: 'All period',
        selected: true,
        key: 'allperiod'
      },
      {
        id: 1,
        title: 'Week',
        selected: false,
        key: 'week'
      },
      {
        id: 2,
        title: 'Month',
        selected: false,
        key: 'month'
      },
    ],
    stackedCardsData: [
      {
        title: 'Balance:',
        value: '5,050 BTC',
        description: '50,345,123 USD',
        background: '#1D62C9',
        color: '#A7C2F1'
      },
      {
        title: 'Followers:',
        value: '142',
        description: '+7',
        color: '#20AD65'
      },
      {
        title: 'Profit:',
        value: '$ 2,122',
        description: '+14%',
        color: '#20AD65'
      },
      {
        title: 'Awaiting for payment',
        value: '$ 122',
        description: '',
      },
    ],
    options: {
      legend: {
        show: false,
      },
      chart: {
        stacked: false,
        toolbar: {
          show: true,
        },
        background: 'transparent',
        sparkline: {
          enabled: false,
        },
        brush: {
          enabled: false,
        },
      },
      grid: {
        show: false,
      },
      stroke: {
        width: [0, 0, 0],
        curve: 'smooth'
      },
      plotOptions: {
        bar: {
          columnWidth: '100%',
        }
      },
      fill: {
        colors: ['#E1F9ED', '#1A58C2'],
        opacity: [0.9, 0.7],
        gradient: {
          inverseColors: true,
          shade: 'dark',
          type: 'vertical',
          opacityFrom: 0.85,
          opacityTo: 1,
          stops: [0, 100, 100, 100]
        },
        pattern: {
          style: 'verticalLines',
          width: 6,
          height: 6,
          strokeWidth: 2,
        },
      },
      labels: [
        '01/01/2003',
        '02/01/2003',
        '03/01/2003',
        '04/01/2003',
        '05/01/2003',
        '06/01/2003',
        '07/01/2003',
        '08/01/2003',
        '09/01/2003',
        '10/01/2003',
        '11/01/2003',
        '01/01/2004',
        '02/01/2004',
        '03/01/2004',
        '04/01/2004',
        '05/01/2004',
        '06/01/2004',
        '07/01/2004',
        '08/01/2004',
        '09/01/2004',
        '10/01/2004',
        '11/01/2004',
      ],
      markers: {
        size: 0
      },
      xaxis: {
        type: 'datetime',
        labels: {
          show: false
        },
        lines: {
          show: false,
        },
      },
      yaxis: {
        title: {
          show: false,
        },
        labels: {
          show: false
        },
        lines: {
          show: false,
        },
        min: 0
      },
      tooltip: {
        shared: true,
        intersect: false,
        y: {
          formatter(y) {
            if (typeof y !== 'undefined') {
              return `${y.toFixed(0)} points`;
            }
            return y;
          }
        }
      }
    },
    series: [{
      name: 'Parameter 1',
      type: 'column',
      data: [
        23,
        11,
        22,
        27,
        13,
        22,
        37,
        21,
        44,
        22,
        30,
        44,
        55,
        41,
        67,
        22,
        43,
        21,
        41,
        56,
        27,
        43,
      ]
    }, {
      name: 'Parameter 2',
      type: 'area',
      data: [
        44,
        55,
        41,
        67,
        22,
        43,
        21,
        41,
        56,
        27,
        43,
        23,
        11,
        22,
        27,
        13,
        22,
        37,
        21,
        44,
        22,
        30,
      ]
    }],
    headersList: [
      'Date & time',
      'Status',
      'Amount',
      'Operation currency',
    ],
    tableData: [
      {
        id: 0,
        date: '22-12-2020 12:31',
        status: 'Refil',
        amount: '420 USD',
        currency: 'BTC'
      },
      {
        id: 1,
        date: '22-12-2020 12:31',
        status: 'Refil',
        amount: '420 USD',
        currency: 'BTC'
      },
      {
        id: 2,
        date: '22-12-2020 12:31',
        status: 'Refil',
        amount: '420 USD',
        currency: 'BTC'
      },
      {
        id: 3,
        date: '22-12-2020 12:31',
        status: 'Refil',
        amount: '420 USD',
        currency: 'BTC'
      },
      {
        id: 4,
        date: '22-12-2020 12:31',
        status: 'Refil',
        amount: '420 USD',
        currency: 'BTC'
      },
    ],
  }

  formatDate = (notFormatedDate) => {
    const date = new Date(notFormatedDate);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();
    let hours = date.getHours();
    let minutes = date.getMinutes();

    if (dt < 10) {
      dt = `0${dt}`;
    }
    if (month < 10) {
      month = `0${month}`;
    }

    if (hours < 10) {
      hours = `0${hours}`;
    }
    if (minutes < 10) {
      minutes = `0${minutes}`;
    }
    return `${year}-${month}-${dt} ${hours}:${minutes}`;
  }


  renderTableHead = (data) => (
    <div className="table-head">
      {
        data.map((value, key) => (
          <div
            className="head-cell"
            key={key}
          >
            { value }
          </div>
        ))
      }
    </div>
  )

  renderTableRows = (data) => (
    <div className="table-rows">
      {
        data.map((value) => (
          <div className="row" key={value.id}>
            <div className="item">{ value.date }</div>
            <div className="item">{ value.status }</div>
            <div className="item">{ value.amount }</div>
            <div className="item">{ value.currency }</div>
          </div>
        ))
      }
    </div>
  )

  render() {
    const {
      options,
      series,
      stackedCardsData,
      location,
      headersList,
      tableData,
    } = this.state;

    const tableHead = this.renderTableHead(headersList);
    const tableBody = this.renderTableRows(tableData);

    const signalsBody = [{
      name: 'Last incoming transaction',
      value: '2,122,01 USD',
      amount: '0.2123 BTC',
    },
    {
      name: 'Received this month',
      value: '8,932,39 USD',
      amount: '0.8921 BTC',
    },
    {
      name: 'Total',
      value: '13,276,46 USD',
      amount: '1.3125 BTC',
    }];

    const referallsBody = [{
      name: 'Last incoming transaction',
      value: '2,122,01 USD',
      amount: '0.2123 BTC',
    },
    {
      name: 'Received this month',
      value: '8,932,39 USD',
      amount: '0.8921 BTC',
    },
    {
      name: 'Total',
      value: '13,276,46 USD',
      amount: '1.3125 BTC',
    }];

    const subscriptionBody = [{
      name: 'Last payment',
      value: '2,122,01 USD',
      amount: '0.2123 BTC',
    },
    {
      name: 'Received this month',
      value: '8,932,39 USD',
      amount: '0.8921 BTC',
    },
    {
      name: 'Total',
      value: '13,276,46 USD',
      amount: '1.3125 BTC',
    }];

    return (
      <div className="l-profit-lose">
        <StackedCards items={stackedCardsData} />
        <div className="chart-container">
          <div className="title">Time period:</div>
          <Dropdown
            title="Select period"
            list={location}
          />
          <ReactApexChart options={options} series={series} type="line" height="200" />
          <div className="chart-footer">
            <div className="column">
              <div className="legend-item">
                <span>Parameter 1</span>
              </div>
              <div className="legend-item">
                <span>Parameter 2</span>
              </div>
            </div>
            <div className="column">
              <div className="checkbox-container">
                <Checkbox name="signals" label="Signals" />
              </div>
              <div className="checkbox-container">
                <Checkbox name="referrals" label="Referrals" />
              </div>
              <div className="checkbox-container">
                <Checkbox name="subscriptions" label="Subscriptions" />
              </div>
            </div>
          </div>
        </div>
        <div className="divider"></div>
        <div className="info-container">
          <div className="column">
            <SubscriptionStats
              title="Revenue from sending signals"
              body={signalsBody}
            />
          </div>
          <div className="column">
            <SubscriptionStats
              title="Subscription payments"
              body={subscriptionBody}
            />
          </div>
        </div>
        <div className="info-container">
          <div className="column">
            <SubscriptionStats
              title="Revenue from referral program"
              body={referallsBody}
            />
          </div>
          <div className="column">
            <div className="make-payment">
              <div className="title">Payment</div>
              <div className="divider"></div>
              <div className="description">You can use these payment services:</div>
              <div className="services">
                <div className="service"><img src={visa} alt="visa" /></div>
                <div className="service"><img src={mastercard} alt="mastercard" /></div>
                <div className="service"><img src={btc} alt="btc" /></div>
                <div className="service"><img src={tether} alt="tether" /></div>
              </div>
              <div className="button">Make payment</div>
            </div>
          </div>
        </div>
        <div className="divider"></div>
        <div className="history-container">
          <div className="title">History</div>
          <div className="table">
            {tableHead}
            {tableBody}
          </div>
          <ReactPaginate
            previousLabel={'<'}
            nextLabel={'>'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={20}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'active'}
          />
        </div>
      </div>
    );
  }
}

ProfitLose.propTypes = {

};
