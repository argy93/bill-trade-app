import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import UserSubstrateIcon from '../Icons/UserSubstrateIcon';

// eslint-disable-next-line react/prefer-stateless-function
class UserInfo extends React.Component {
  render() {
    const { photo, width, height } = this.props;

    return (
      <div className="c-user-icon">
        {photo
          ? <img src={photo} alt="" />
          : (
            <UserSubstrateIcon
              width={width}
              height={height}
            />
          )
        }
      </div>
    );
  }
}

UserInfo.propTypes = {
  photo: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};
export default UserInfo;
