import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import Select from 'react-dropdown-select';

class LangSelect extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.options = [
      { id: 1, label: 'RU', value: 'ru' },
      { id: 2, label: 'EN', value: 'en' },
      { id: 3, label: 'UA', value: 'ua' }
    ];
  }

  setValues(values) {
    // console.log(values);
  }

  customDropdownRenderer() {
    return (
      <ul className="c-lang-select__list">
        {this.options.map((item) => (
          <li
            key={item.value}
            className="c-lang-select__list-item"
          >
            {item.label}
          </li>
        ))}
      </ul>
    );
  }

  render() {
    const { position } = this.props;

    return (
      <Select
        dropdownRenderer={this.customDropdownRenderer}
        className={'c-lang-select'}
        options={this.options}
        values={[this.options[0]]}
        dropdownPosition={position}
        onChange={(values) => this.setValues(values)}
      />
    );
  }
}

LangSelect.propTypes = {
  position: PropTypes.string,
};

export default LangSelect;
