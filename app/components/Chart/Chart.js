import React from 'react';
import PropTypes from 'prop-types';
import SVG from 'svg.js';
import { format } from 'date-fns';
import './style.scss';
import ColorCounter from '../../utils/ColorCounter';
import RenderPipeline from '../../utils/RenderPipeline';

class Chart extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static defaultBubbleFormatter(value) {
    return value;
  }

  constructor(props) {
    super(props);
    this.domComponent = React.createRef();
    this.domSvg = React.createRef();
    this.propotions = 3.117;
    this.colorCounter = new ColorCounter();
    this.renderPipeline = new RenderPipeline();
    this.polylines = [];
    this.polygones = [];
    this.state = {
      bubble: {
        show: false,
      },
    };

    this.setBoundValues();
    this.updateComponentWidth = this.updateComponentWidth.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateComponentWidth);
    this.drawSvg();
  }

  componentDidUpdate(prevProps) {
    const { items, highlight } = this.props;
    const isNewData = JSON.stringify(prevProps.items) !== JSON.stringify(items);

    if (isNewData) {
      this.setBoundValues();

      this.draw.animate(200)
        .attr({
          opacity: 0,
        })
        .after(() => {
          this.drawSvg();
          this.draw.animate(200)
            .attr({
              opacity: 1,
            });
        });
    }

    if (prevProps.highlight !== highlight) {
      this.highlightPolyline();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateComponentWidth);
  }

  setBoundValues() {
    const { items } = this.props;

    this.timeMin = Math.min(...items[0].points.map((point) => point.timestamp));
    this.timeMax = Math.max(...items[0].points.map((point) => point.timestamp));
    this.maxValue = Math.max(...items[0].points.map((point) => point.value));
    this.minValue = Math.max(...items[0].points.map((point) => point.value));

    for (let i = 1; i < items.length; i += 1) {
      const item = items[i];

      this.timeMin = Math.min(this.timeMin, ...item.points.map((point) => point.timestamp));
      this.timeMax = Math.max(this.timeMax, ...item.points.map((point) => point.timestamp));
      this.maxValue = Math.max(this.maxValue, ...item.points.map((point) => point.value));
      this.minValue = Math.min(this.minValue, ...item.points.map((point) => point.value));
    }
  }

  getPolygonPointArray(points) {
    const pointArray = [];

    const xd = this.svgWidth / (this.timeMax - this.timeMin);

    for (let i = 0; i < points.length; i += 1) {
      const point = points[i];
      const x = xd * (point.timestamp - this.timeMin);
      const y = this.svgHeight - (this.svgHeight / ((this.maxValue - this.minValue)) * (point.value - this.minValue));

      pointArray.push([x, y]);
    }

    return [
      [0, this.svgHeight],
      ...pointArray,
      [this.svgWidth, this.svgHeight],
    ];
  }

  getBubblePosition(closestPoints) {
    return {
      x: (this.svgWidth / (this.timeMax - this.timeMin)) * (closestPoints[0].bestPoint.timestamp - this.timeMin),
      y: this.svgHeight - (this.svgHeight / this.maxValue * Math.max(...closestPoints.map((point) => point.bestPoint.value))),
    };
  }

  getClosestPointsByX(x) {
    const { items } = this.props;
    const closest = [];
    const cursorPercent = (100 / this.domSvg.current.clientWidth) * x;

    for (let i = 0; i < items.length; i += 1) {
      let bestPointPercent = null;
      let bestPoint = null;
      for (let j = 0; j < items[i].points.length; j += 1) {
        const pointPercent = (100 / (this.timeMax - this.timeMin)) * (items[i].points[j].timestamp - this.timeMin);

        if (bestPointPercent === null) {
          bestPoint = items[i].points[j];
          bestPointPercent = pointPercent;
        } else if (Math.abs(bestPointPercent - cursorPercent) > Math.abs(pointPercent - cursorPercent)) {
          bestPoint = items[i].points[j];
          bestPointPercent = pointPercent;
        }
      }

      if (!closest[i]) {
        closest[i] = {
          ...items[i],
          bestPoint,
        };
      }
    }

    return closest;
  }

  updateComponentWidth() {
    this.drawSvg();
  }

  highlightPolyline() {
    const { highlight } = this.props;

    if (this.polylines.length) {
      for (let i = 0; i < this.polylines.length; i += 1) {
        this.polylines[i].animate(300)
          .attr({
            opacity: highlight === i ? 1 : 0,
          });
      }
    }
  }

  cleanSvg() {
    this.polylines = [];
    this.polygones = [];

    SVG(this.domSvg.current)
      .clear();
  }

  drawSvg() {
    const { items, highlight, height } = this.props;
    this.renderPipeline.reset();
    this.cleanSvg();
    this.colorCounter.resetIterator();

    this.draw = SVG(this.domSvg.current)
      .size('100%', 'auto');

    this.svgWidth = this.domComponent.current.clientWidth;
    this.svgHeight = !height ? Math.ceil(this.svgWidth / this.propotions) : height;
    this.draw = SVG(this.domSvg.current)
      .size(this.svgWidth, this.svgHeight)
      .viewbox({
        x: 0,
        y: 0,
        width: this.svgWidth,
        height: this.svgHeight,
      });

    for (let i = 0; i < items.length; i += 1) {
      const item = items[i];
      const color = item.color || this.colorCounter.getNextColor();

      this.renderPipeline.add(() => {
        this.polygones.push(this.draw.polygon(this.getPolygonPointArray(item.points))
          .attr({
            'stroke-width': 0,
            fill: color,
            opacity: i === items.length ? 1 : 0.5,
          }),
        );
        this.polylines.push(
          this.draw.polyline(this.getPolygonPointArray(item.points))
            .attr({
              'stroke-width': 3,
              stroke: color,
              fill: 'transparent',
              opacity: highlight === i ? 1 : 0,
            }));
      });
    }

    this.renderPipeline.add(() => {
      this.draw = SVG(this.domSvg.current)
        .size(this.svgWidth, this.svgHeight)
        .viewbox({
          x: 0,
          y: 0,
          width: this.svgWidth,
          height: this.svgHeight,
        });
    });

    this.renderPipeline.render(46);
  }

  showInfoBubble(e) {
    const { offsetX } = e.nativeEvent;
    const closestPoints = this.getClosestPointsByX(offsetX);

    this.setState({
      bubble: {
        show: true,
        position: this.getBubblePosition(closestPoints),
        points: closestPoints,
      },
    });
  }

  hideInfoBubble() {
    this.setState({
      bubble: {
        show: false,
      },
    });
  }

  render() {
    const { bubble } = this.state;
    let { bubbleFormatter } = this.props;

    if (!bubbleFormatter) bubbleFormatter = Chart.defaultBubbleFormatter;

    this.colorCounter.resetIterator();

    return (
      <div className="c-chart" ref={this.domComponent}>
        <div onMouseLeave={() => this.hideInfoBubble()} onMouseMove={(e) => this.showInfoBubble(e)}>
          <svg ref={this.domSvg} width={'100%'} />
          {bubble.show ? (
            <div
              className="c-chart__bubble"
              style={{
                top: bubble.position.y,
                left: bubble.position.x,
              }}
            >
              <div className="c-chart__bubble-content">
                <div
                  className="c-chart__time"
                >
                  {format(new Date(bubble.points[0].bestPoint.timestamp), 'd.MM.yyyy | H:mm')}
                </div>
                {bubble.points.map((item) => (
                  <div className="c-chart__info" key={item.name}>
                    <span
                      className="c-chart__info-feature"
                      style={{ background: this.colorCounter.getNextColor() }}
                    >
                    </span>
                    <span className="c-chart__info-name">{item.name}</span>
                    <span className="c-chart__info-value">{bubbleFormatter(item.bestPoint.value)}</span>
                  </div>
                ))}
              </div>
            </div>
          ) : ('')}
        </div>
      </div>
    );
  }
}

const propsChartData = PropTypes.arrayOf(
  PropTypes.shape({
    name: PropTypes.string,
    color: PropTypes.string,
    points: PropTypes.arrayOf(
      PropTypes.shape({
        timestamp: PropTypes.number,
        value: PropTypes.number,
      }),
    ),
  }));

Chart.propTypes = {
  items: propsChartData,
  highlight: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number,
  ]),
  height: PropTypes.number,
  bubbleFormatter: PropTypes.func,
};

export { propsChartData, Chart };
export default Chart;
