/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-array-index-key */
/**
 *
 * Expenses tab content component
 *
 */

// main imports
import React from 'react';
import PropTypes from 'prop-types';

// components imports

// style imports
import './style';

// other imports
import arrowDown from './arrow-down.svg';


// eslint-disable-line react/prefer-stateless-function
export default class Dropdown extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      listOpen: false,
      headerTitle: props.title
    };
  }

  handleClickOutside() {
    this.setState({
      listOpen: false
    });
  }

  toggleList() {
    this.setState((prevState) => ({
      listOpen: !prevState.listOpen
    }));
  }

  render() {
    const { list } = this.props;
    const { listOpen, headerTitle } = this.state;
    console.log(list);
    return (
      <div className="dd-wrapper">
        <div className="dd-header" onClick={() => this.toggleList()}>
          <div className="dd-header-title">
            {headerTitle}
            {listOpen
              ? <div className="icon" style={{ background: `url(${arrowDown})`, backgroundRepeat: 'no-repeat', transform: 'rotate(180deg)', transformOrigin: '6px 3px' }}></div>
              : <div className="icon" style={{ background: `url(${arrowDown})`, backgroundRepeat: 'no-repeat' }}></div>
            }
          </div>
        </div>
        {listOpen && <ul className="dd-list">
          {list.map((item) => (
            <li className={`dd-list-item ${item.selected ? 'selected' : ''}`} key={item.id}>{item.title}</li>
          ))}
        </ul>}
      </div>
    );
  }
}

Dropdown.propTypes = {
  list: PropTypes.array,
  title: PropTypes.string,
};
