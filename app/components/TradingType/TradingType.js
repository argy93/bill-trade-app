/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-array-index-key */
/**
 *
 * TradingType tab content component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
  timer
} from 'rxjs';
import {
  toast
} from 'react-toastify';
import SettingsService from '../../containers/SettingsPage/service';
import ButtonSmall from '../ButtonSmall';
import Input from '../Input';
import Button from '../Button';
import Done from '../Icons/DoneIcon';
import PlusIcon from '../Icons/PlusIcon';
import './style';


// eslint-disable-line react/prefer-stateless-function
export default class TradingType extends React.PureComponent {
  state = {
    trader: 'publicTrader',
    copier: 'privateCopier',
    isSaved: false,
  };

  componentDidMount() {
    SettingsService.getAccount()
      .then(({ data }) => {
        const loadableData = data.data;
        this.setState({
          trader: loadableData.trader_type,
          copier: loadableData.copier_type,
        });
      })
      .catch((error) => {
        toast('The data could not be loaded, please try again');
      });
  }

  handleOptionChange = (name, value) => {
    this.setState({
      [name]: value
    });
  }

  handleSave = () => {
    const { trader, copier } = this.state;
    console.log(trader, copier);
    SettingsService.updateTradingType(trader, copier)
      .then(() => {
        this.setState({
          isSaved: true,
        }, this.handleCreateFinish);
      })
      .catch((error) => {
        toast('Some error occured, please try again.');
      });
  }

  handleCreateFinish = () => {
    const source = timer(2000);
    const subscriber = source.subscribe(() => {
      this.setState({
        isSaved: false,
      });
      subscriber.unsubscribe();
    });
  }

  render() {
    const { trader, copier, isSaved } = this.state;

    const footerButton = (
      <div className="footer-container-default">
        <button onClick={this.handleSave} type="button" className="add-new">Save settings</button>
      </div>
    );

    const footerMessage = (
      <div className="footer-container-saved">
        <div className="content">
          <span className="icon">
            <Done />
          </span>
          <span className="text">Trading type settings are being saved!</span>
        </div>
      </div>
    );
    return (
      <div className="trading-type-wrapper">
        <div className="trading-type">
          <div className="column">
            <div className="title">Trader</div>
            <div className="divider"></div>
            <form>
              <div className="radio">
                <label htmlFor="publicTrader">
                  <input
                    id="publicTrader"
                    name="publicTrader"
                    type="radio"
                    value="PUBLIC"
                    checked={trader === 'PUBLIC'}
                    onChange={(e) => this.handleOptionChange('trader', e.target.value)}
                  />
                  Public
                </label>
                <div className="description">
                  By becoming a public trader you will broadcast every transaction made by you to other members of the Bill Trade system.Thereby obtaining the possibility of additional income.
                </div>
                <div className="notes">
                  Comission: 50%
                </div>
              </div>
              <div className="divider"></div>
              <div className="radio">
                <label htmlFor="privateTrader">
                  <input
                    id="privateTrader"
                    name="privateTrader"
                    type="radio"
                    value="PRIVATE"
                    checked={trader === 'PRIVATE'}
                    onChange={(e) => this.handleOptionChange('trader', e.target.value)}
                  />
                  Private
                </label>
                <div className="description">
                  No bradcasting of any of your transactions.
                </div>
              </div>
            </form>
          </div>
          <div className="column">
            <div className="title">Copier</div>
            <div className="divider"></div>
            <form>
              <div className="radio">
                <label htmlFor="publicCopier">
                  <input
                    id="publicCopier"
                    name="publicCopier"
                    type="radio"
                    value="PUBLIC"
                    checked={copier === 'PUBLIC'}
                    onChange={(e) => this.handleOptionChange('copier', e.target.value)}
                  />
                  Public
                </label>
                <div className="description">
                  By becoming a public copier you will become analyst and braodcast every transaction that you copy to other copying accounts.Thereby obtaining the possibility of additional income.
                </div>
                <div className="notes">
                  Comission: 10%
                </div>
              </div>
              <div className="divider"></div>
              <div className="radio">
                <label htmlFor="privateCopier">
                  <input
                    id="privateCopier"
                    name="privateCopier"
                    type="radio"
                    value="PRIVATE"
                    checked={copier === 'PRIVATE'}
                    onChange={(e) => this.handleOptionChange('copier', e.target.value)}
                  />
                  Private
                </label>
                <div className="description">
                  No bradcasting of any of your copied transactions.
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="divider"></div>
        {isSaved ? footerMessage : footerButton}
      </div>
    );
  }
}

TradingType.propTypes = {

};
