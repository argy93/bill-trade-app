/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-array-index-key */
/**
 *
 * Expenses tab content component
 *
 */

// main imports
import React from 'react';
import ReactPaginate from 'react-paginate';

// components imports
import StackedCards from '../StackedCards';
import SubscriptionStats from '../SubscriptionStats';
import MemberCard from '../MemberCard';

// style imports
import './style';


// eslint-disable-line react/prefer-stateless-function
export default class Expenses extends React.PureComponent {
  state = {
    stackedCardsData: [
      {
        title: 'Expenses total:',
        value: '8,779 USD',
        description: '0.6278 BTC',
        background: '#E75152',
        color: '#FCE9E9'
      },
      {
        title: 'Balance:',
        value: '5,050 BTC',
        description: '50,345,123 USD',
      },
    ],
  }

  render() {
    const {
      stackedCardsData
    } = this.state;

    const paymentHeader = ['Members type', 'Q-ty', 'Amount'];
    const paymentBody = [
      {
        name: 'Traders',
        value: '2',
        amount: '422 USD',
        isOpened: true,
        subitems: [{
          name: 'Trader1234556',
          value: '211 USD'
        }, {
          name: 'Trader1234556',
          value: '211 USD'
        }]
      }, {
        name: 'Analysts',
        value: '1',
        amount: '422 USD',
        isOpened: false,
        subitems: [{
          name: 'Trader1234556',
          value: '211 USD'
        }, {
          name: 'Trader1234556',
          value: '211 USD'
        }]
      },
    ];

    return (
      <div className="l-expenses">
        <StackedCards items={stackedCardsData} />
        <div className="divider"></div>
        <div className="flex-grid">
          <div className="column">
            <SubscriptionStats
              title="Payment required"
              header={paymentHeader}
              body={paymentBody}
            />
          </div>
          <div className="column"></div>
        </div>
        <div className="divider"></div>
        <div className="details-container">
          <div className="title">Details</div>
          <div className="member-card-container">
          </div>
          <div className="member-card-container">
          </div>
          <div className="member-card-container">
          </div>
          <ReactPaginate
            previousLabel={'<'}
            nextLabel={'>'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={20}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'active'}
          />
        </div>
      </div>
    );
  }
}

Expenses.propTypes = {

};
