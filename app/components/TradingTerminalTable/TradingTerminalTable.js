/*
 * TradingTerminalTable
 *
 * This is the first thing users see when they start using our service, at the '/payment' route
 */

// main imports
import React from 'react';
import PropTypes from 'prop-types';
import formatDate from '../../utils/formatDate';

// components import

// styles imports
import './style.scss';


// eslint-disable-line react/prefer-stateless-function
export default class TradingTerminalTable extends React.PureComponent {

  // ////////////////////////////////////////////
  // # table header methods
  // ////////////////////////////////////////////

  renderTableHead = () => {
    const { head } = this.props;
    return head.map((tableHeadItem) => {
      return (<th className="header-cell" key={tableHeadItem.id}>{tableHeadItem.name}</th>);
    });
  }

  // ////////////////////////////////////////////
  // # table body methods
  // ////////////////////////////////////////////

  renderTableRows = () => {
    const { rows } = this.props;
    return rows.map((row) => {
      if (row.visible) {
        const profitWidth = `${row.profit * 3}px`;
        const loseWidth = `${row.lose * 3}px`;
        return (
          <tr key={row.id} className="row">
            <td className="cell">{row.ticker}</td>
            <td className="cell">{formatDate(row.date, true)}</td>
            <td className="cell">{row.dep}</td>
            <td className="cell">{row.amount}</td>
            <td className="cell">{row.vol}</td>
            <td className="cell">{row.price}</td>
            <td className="cell">{row.sl}</td>
            <td className="cell">
              {row.tp}
              <div className="sl" style={{ width: loseWidth }}></div>
              <div className="tp" style={{ width: profitWidth }}></div>
            </td>
            <td className="cell">{row.lastPrice}</td>
            <td className="cell">{row.swap}</td>
            <td className="cell">{row.plc}</td>
            <td className="cell">{row.pld}</td>
            <td className="cell">{row.plp}</td>
            <td className="cell">{row.comsn}</td>
            <td className="cell">{row.cmnt}</td>
            <td className="cell"></td>
          </tr>
        );
      }
    });
  }

  // ////////////////////////////////////////////
  // # utils
  // ////////////////////////////////////////////

  render() {
    return (
      <div className="c-trading-terminal-table">
        <div className="bar">
          <div className="title">Tools</div>
          <div className="icons"></div>
        </div>
        <table className="terminal-table">
          <tr className="header-container">
            {this.renderTableHead()}
          </tr>
          {this.renderTableRows()}
        </table>
      </div>
    );
  }
}

TradingTerminalTable.propTypes = {
  head: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      visible: PropTypes.bool,
    })
  ),
  rows: PropTypes.arrayOf(
    PropTypes.shape({
      ticker: PropTypes.string,
      date: PropTypes.string,
      dep: PropTypes.string,
      amount: PropTypes.string,
      vol: PropTypes.string,
      price: PropTypes.string,
      sl: PropTypes.string,
      tp: PropTypes.string,
      lastPrice: PropTypes.string,
      swap: PropTypes.string,
      plc: PropTypes.string,
      pld: PropTypes.string,
      plp: PropTypes.string,
      comsn: PropTypes.string,
      cmnt: PropTypes.string,
    })
  ),
};
