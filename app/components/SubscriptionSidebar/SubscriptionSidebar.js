/* eslint-disable no-restricted-globals */
/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
/* eslint-disable react/no-array-index-key */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import ReactApexChart from 'react-apexcharts';

import StatsProgress from '../StatsProgress';
import CheckboxRed from '../CheckboxRed';
import CheckboxGreen from '../CheckboxGreen';

import carretDown from './carret-down.svg';
import carretUp from './carret-up.svg';

import './style.scss';

class SubscriptionSidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      balanceOpened: false,
      totalOpened: false,
      currentOpened: true,
      balanceChartData: {},
      totalChartData: {},
      subscriptionAmountChartData: {
        options: {
          plotOptions: {
            radialBar: {
              hollow: {
                size: '50%',
                margin: 0,
              },
              dataLabels: {
                show: true,
                name: {
                  show: false,
                },
                value: {
                  show: true,
                  fontSize: '12px',
                  fontFamily: 'Gilroy',
                  color: '#000',
                  offsetY: 5,
                  formatter: (val) => `${val}%`
                },
                total: {
                  show: false,
                  label: 'Total',
                  color: '#373d3f',
                }
              }
            },
          },
          labels: ['']
        },
        series: [props.investProgress.progress],
      }
    };
  }

  // ///////////////////////////////////////////////
  // # HANDLERS
  // ///////////////////////////////////////////////

  toggleSection = (name) => {
    this.setState({
      currentOpened: false,
      totalOpened: false,
      balanceOpened: false,
      [name]: !this.state[name]
    });
  }

  // ///////////////////////////////////////////////
  // # START TOTAL BALANCE
  // ///////////////////////////////////////////////

  renderTotalBalance = () => {
    const {
      statsData,
      currentChartData,
      createChartData
    } = this.props;

    if (!statsData || !currentChartData.data.length) {
      return;
    }
    const { total } = statsData;
    const { balanceOpened } = this.state;
    const totalData = createChartData(
      'totalBalanceChart',
      currentChartData.data.map((value) => this.formatCurrency(value.value)),
      currentChartData.data.map((value) => this.formatCurrency(value.percent, 2)),
    );
    return (
      <div className="total-balance">
        <StatsProgress progress={100}>Total balance</StatsProgress>
        <img
          src={balanceOpened ? carretUp : carretDown}
          alt=""
          className="toggle-icon"
          onClick={() => this.toggleSection('balanceOpened')}
        />
        <div className="value">{this.formatCurrency(total.BTC)} BTC</div>
        <div className="label">{this.formatCurrency(total.USDT, 2)} USD</div>
        <div className="dropdown">
          {balanceOpened ? (
            <div className="total-dropdown">
              <ReactApexChart options={totalData.options} series={totalData.series} type="donut" height="170" />
              {this.renderTotalBalanceDropdown()}
            </div>
          ) : ''}
        </div>
      </div>
    );
  }

  renderTotalBalanceDropdown = () => {
    const { currentChartData } = this.props;
    if (!currentChartData.data.length) {
      return;
    }

    return currentChartData.data.map((stock, index) => (
      <div className={index === currentChartData.data.length - 1 ? 'currency last' : 'currency'} key={index}>
        <div className="item">{this.formatCurrency(stock.percent, 2)}%</div>
        <div className="item">{this.formatCurrency(stock.value)}</div>
        <div className="item">{stock.stockCurrency}</div>
      </div>
    ));
  }

  // ///////////////////////////////////////////////
  // # END TOTAL BALANCE
  // ///////////////////////////////////////////////

  // ///////////////////////////////////////////////
  // # START TOTAL INVESTED
  // ///////////////////////////////////////////////

  renderTotalInvested = () => {
    const { statsData } = this.props;

    if (!statsData) {
      return;
    }
    const { invested, invested_percent } = statsData;
    const { totalOpened } = this.state;

    return (
      <div className="total-invested">
        <StatsProgress
          progress={Number(invested_percent)}
        >
          Total invested
        </StatsProgress>
        <img
          src={totalOpened ? carretUp : carretDown}
          alt=""
          className="toggle-icon"
          onClick={() => this.toggleSection('totalOpened')}
        />
        <div className="value">{this.formatCurrency(invested.BTC)} BTC</div>
        <div className="label">{this.formatCurrency(invested.USDT, 2)} USD</div>
        <div className="dropdown">
          {totalOpened ? this.renderTotalInvestedDropdown() : ''}
        </div>
      </div>
    );
  }

  renderTotalInvestedDropdown = () => {
    const { statsData } = this.props;
    const { invested } = statsData;
    return Object.keys(invested).map((currency, index) => {
      const amount = this.formatCurrency(invested[currency]);
      return (
        <div className="currency" key={index}>
          <div className="item">0%</div>
          <div className="item">{amount}</div>
          <div className="item">{currency}</div>
        </div>
      );
    });
  }

  // ///////////////////////////////////////////////
  // # END TOTAL INVESTED
  // ///////////////////////////////////////////////

  // ///////////////////////////////////////////////
  // # START CURRENT INVESTMENT
  // ///////////////////////////////////////////////

  renderCurrentInvestment = () => {
    const {
      statsData,
      investProgress,
      createChartData,
    } = this.props;

    if (!statsData) return;

    const { currentOpened, subscriptionAmountChartData } = this.state;
    const currentChartData = createChartData('currentChartData');

    return (
      <div className="current-investment">
        <StatsProgress progress={this.formatCurrency(investProgress.progress, 0)}>Current investment</StatsProgress>
        <img
          src={currentOpened ? carretUp : carretDown}
          alt=""
          className="toggle-icon"
          onClick={() => this.toggleSection('currentOpened')}
        />
        <div className="value">{ this.formatCurrency(investProgress.currencies.BTC) } BTC</div>
        <div className="label">{ this.formatCurrency(investProgress.currencies.USDT, 2) } USD</div>
        {currentOpened
          ? (
            <div className="wallets">
              <div className="chart-container">
                <div className="chart-left">
                  <ReactApexChart options={currentChartData.options} series={currentChartData.series} type="donut" height="170" />
                </div>
                <div className="chart-right">
                  <ReactApexChart options={subscriptionAmountChartData.options} series={[this.formatCurrency(investProgress.progress, 0)]} type="radialBar" height="160" />
                </div>
              </div>
              {this.renderCurrentInvestmentDropdown()}
            </div>
          ) : ''}
      </div>
    );
  }

  renderCurrentInvestmentDropdown = () => {
    const {
      include,
      exclude,
      currentChartData,
      currentStep,
    } = this.props;

    return currentChartData.data.map((investData, index) => (
      <div
        className={
          `wallet
          ${include ? ' assets-include' : `${investData.included ? 'include-active' : ''}`}
          ${exclude ? 'assets-exclude' : `${investData.excluded ? 'exclude-active' : ''}`}
          `
        }
        key={index}
      >
        <div className="col wide">{investData.stockCurrency}</div>
        <div className="col wide">{this.formatCurrency(investData.percent, 3)}%</div>
        <div className="col wide">
          <input
            max="3"
            type="text"
            name={`${investData.stockName}-${investData.stockCurrency}-percent`}
            className="calc-input"
            value={`${this.formatCurrency(investData.allocatedPercent, 3)}%`}
            onChange={({ target }) => this.props.calculateSubscribedAmountByPercent(investData.stockName, investData.stockCurrency, target.value)}
            disabled={currentStep > 0}
          />
        </div>
        <div className="col wide">
          <input
            type="text"
            name={`${investData.stockName}-${investData.stockCurrency}-value`}
            className="calc-input"
            value={this.formatCurrency(investData.subscribedAmount, 4)}
            onChange={({ target }) => this.props.calculateStockAmountByPercent(investData.stockName, investData.stockCurrency, target.value)}
            disabled={currentStep > 0}
          />
        </div>
        <div className="col checkbox-column">
          { exclude
            ? (
              <div className="negative">
                {!investData.included ? <CheckboxRed disabled={currentStep > 0} defaultChecked={investData.excluded} onChange={() => this.props.handleExclude(index)} name={`${investData.stockName}-${investData.stockCurrency}-red`} /> : ''}
              </div>
            ) : ''}
          { include
            ? (
              <div className="positive">
                {!investData.excluded ? <CheckboxGreen disabled={currentStep > 0} defaultChecked={investData.included} onChange={() => this.props.handleInclude(index)} name={`${investData.stockName}-${investData.stockCurrency}-green`} /> : ''}
              </div>
            ) : ''}
        </div>
      </div>
    ));
  }

  // ///////////////////////////////////////////////
  // # END CURRENT INVESTMENT
  // ///////////////////////////////////////////////

  // ///////////////////////////////////////////////
  // # UTILS
  // ///////////////////////////////////////////////

  formatCurrency = (currency, amount = 4) => Number(parseFloat(Math.round(currency * 10000) / 10000).toFixed(amount));

  render() {
    const {
      stats,
    } = this.props;

    return (
      <div
        className="stats-container"
        style={{
          left: stats ? '770px' : 0,
          boxShadow: stats ? '0px 10px 20px rgba(105, 105, 105, 0.1)' : 'none'
        }}
      >
        <div className="title">Stats</div>
        {this.renderTotalBalance()}
        {this.renderTotalInvested()}
        {this.renderCurrentInvestment()}
      </div>
    );
  }
}

SubscriptionSidebar.propTypes = {
  stats: PropTypes.bool,
  include: PropTypes.bool,
  exclude: PropTypes.bool,
  statsData: PropTypes.object,
  currentChartData: PropTypes.object,
  calculateStockAmountByPercent: PropTypes.func,
  calculateSubscribedAmountByPercent: PropTypes.func,
  investProgress: PropTypes.object,
  currentStep: PropTypes.number,
  createChartData: PropTypes.func,
  handleExclude: PropTypes.func,
  handleInclude: PropTypes.func,
};

export default SubscriptionSidebar;
