/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';

// styles imports
import './style.scss';

// images imports
import arrowDown from './arrow-down.svg';

// const properties = {
//   title: '',
//   header: ['Members type', 'Q-ty', 'Amount'],
//   body: [
//     {
//       name: '',
//       value: '',
//       amount: '',
//       isOpened: false,
//       subitems: [
//         {
//           name: '',
//           value: '',
//         }
//       ]
//     }
//   ]
// };

class SubscriptionStats extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      ...this.props,
    };
  }

  handleToggle = (index) => {
    const { body } = this.state;
    const toggledState = body.map((value, key) => {
      if (key === index) {
        return { ...value, isOpened: !value.isOpened };
      }
      return value;
    });
    this.setState({
      body: toggledState
    });
  }

  renderBody = () => {
    const { body } = this.state;
    return body.map((value, key) => (
      <div className="row" key={key}>
        <div
          className="main-content"
          onClick={() => this.handleToggle(key)}
        >
          <div className="cell">{value.name}</div>
          <div className="cell">{value.value}</div>
          <div className="cell">
            <div className="text">{value.amount}</div>
            {value.subitems && value.subitems.length
              ? (
                <div className="icon-container">
                  <div
                    className="icon"
                    style={{
                      background: `url(${arrowDown})`,
                      backgroundRepeat: 'no-repeat',
                      backgroundSize: 'cover',
                      backgroundPosition: 'center center',
                      transform: `rotate(${value.isOpened ? '180deg' : '0'}) scale(2)`
                    }}
                  >
                  </div>
                </div>
              ) : ''}
          </div>
        </div>
        { value.subitems && value.subitems.length && value.isOpened ? (
          <div className="sub-content">
            {
              value.subitems.map((subvalue, subkey) => (
                <div className="sub-cell" key={subkey}>
                  <div className="item">{subvalue.name}</div>
                  <div className="item">{subvalue.value}</div>
                </div>
              ))
            }
          </div>
        ) : '' }
      </div>
    ));
  }

  render() {
    const { title, header, body } = this.state;
    return (
      <div className="c-subscription-stats">
        <div className="title">{title}</div>
        {header ? (
          <div className="header">
            {
              header.length ? header.map((value, key) => <div className="cell" key={key}>{value}</div>) : ''
            }
          </div>
        ) : ''}
        <div className="body">
          {this.renderBody(body)}
        </div>
      </div>
    );
  }
}

SubscriptionStats.propTypes = {
  // eslint-disable-next-line react/no-unused-prop-types
  title: PropTypes.string,
  // eslint-disable-next-line react/no-unused-prop-types
  header: PropTypes.array,
  // eslint-disable-next-line react/no-unused-prop-types
  body: PropTypes.array
};

export default SubscriptionStats;
