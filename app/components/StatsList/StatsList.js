import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class StatsList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      items,
    } = this.props;

    const half = Math.ceil(items.length / 2);

    return (
      <div className="c-stats-list">
        <div className="c-stats-list__column">
          {
            items.slice(0, half)
              .map((item, count) => (
                <div className="c-stats-list__item" key={item.name}>
                  <div className="c-stats-list__item-name">{count + 1}. {item.name}</div>
                  <div className="c-stats-list__item-value">{item.value}</div>
                </div>
              ))
          }
        </div>
        <div className="c-stats-list__column">
          {
            items.slice(half, items.length)
              .map((item, count) => (
                <div className="c-stats-list__item" key={item.name}>
                  <div className="c-stats-list__item-name">{half + count + 1}. {item.name}</div>
                  <div className="c-stats-list__item-value">{item.value}</div>
                </div>
              ))
          }
        </div>
      </div>
    );
  }
}

StatsList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])
  })),
};

export default StatsList;
