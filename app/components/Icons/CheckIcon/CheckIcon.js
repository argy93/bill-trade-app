import React from 'react';
import PropTypes from 'prop-types';

const CheckIcon = ({ width = 24, height = 18, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 24 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M8.75182 13.2482L21.9167 0.083252L23.9202 2.08672L8.75182 17.2551L0.666748 9.17005L2.67022 7.16659L8.75182 13.2482Z" fill="#20AD65" />
  </svg>
);

CheckIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string
};

export default CheckIcon;
