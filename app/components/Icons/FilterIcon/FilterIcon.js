import React from 'react';
import PropTypes from 'prop-types';

const FilterIcon = ({ width = 21, height = 22, className }) => (
  <svg className={className} width={width} height={height} viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M0.0830078 4.64107L7.37468 12.9744V21.8986L13.6247 19.5549V12.9744L20.9163 4.64107V2.16634C20.9163 1.01575 19.9836 0.0830078 18.833 0.0830078H2.16634C1.01575 0.0830078 0.0830078 1.01575 0.0830078 2.16634V4.64107ZM18.833 2.16634V3.20801H2.16634V2.16634H18.833ZM3.42027 5.29134H17.5791L12.1103 11.5413H8.88902L3.42027 5.29134ZM9.45801 13.6247V18.8924L11.5413 18.1111V13.6247H9.45801Z"/>
  </svg>
);

FilterIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default FilterIcon;
