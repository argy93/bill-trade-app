import React from 'react';
import PropTypes from 'prop-types';

const FullScreenIcon = ({ width = 18, height = 18, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M3 3V9H1.5V1.5H9V3H3ZM11.7803 7.28033L15 4.06066V6.75H16.5V1.5H11.25V3H13.9393L10.7197 6.21967L11.7803 7.28033ZM15 9V15H9V16.5H16.5V9H15ZM6.75 16.5V15H4.06066L7.28033 11.7803L6.21967 10.7197L3 13.9393V11.25H1.5V16.5H6.75Z" fill="white" />
  </svg>
);

FullScreenIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default FullScreenIcon;
