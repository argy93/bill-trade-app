import React from 'react';
import PropTypes from 'prop-types';

const TraidingTerminalIcon = ({ width = 24, height = 24, className }) => (
  <svg width={width} height={height} className={className} fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M6.96004 6.64754V19.6075H8.88004V6.64754H6.96004ZM5.04004 21.5275H10.8V4.72754H5.04004V21.5275Z" />
    <path fillRule="evenodd" clipRule="evenodd" d="M15.0841 4.32039V17.2804H17.0041V4.32039H15.0841ZM13.1641 19.2004H18.9241V2.40039H13.1641V19.2004Z" />
  </svg>
);

TraidingTerminalIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default TraidingTerminalIcon;
