import React from 'react';
import PropTypes from 'prop-types';

const HamburgerIcon = ({ width = 92.833, height = 92.833, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 92.833 92.833" xmlSpace="preserve" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g>
      <g>
        <path d="M89.834,1.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V4.75 C92.834,3.096,91.488,1.75,89.834,1.75z" />
        <path d="M89.834,36.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V39.75 C92.834,38.096,91.488,36.75,89.834,36.75z" />
        <path d="M89.834,71.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V74.75 C92.834,73.095,91.488,71.75,89.834,71.75z" />
      </g>
    </g>
  </svg>
);

HamburgerIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default HamburgerIcon;
