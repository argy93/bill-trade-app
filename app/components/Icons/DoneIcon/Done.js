import React from 'react';

const Done = () => (
  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M6.40264 11.2687L14.2669 3.10498L16.3682 5.28626L6.40264 15.6313L0.894531 9.91346L2.9958 7.73218L6.40264 11.2687Z" fill="white" />
  </svg>
);

export default Done;
