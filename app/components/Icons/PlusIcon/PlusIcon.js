import React from 'react';
import PropTypes from 'prop-types';

const PlusIcon = ({ width = 32, height = 32, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="8.5" cy="8.5" r="8" stroke="white" />
    <path d="M8.5 4V13" stroke="white" />
    <path d="M13 8.5L4 8.5" stroke="white" />
  </svg>
);

PlusIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default PlusIcon;
