import React from 'react';
import PropTypes from 'prop-types';

const NormalScreenIcon = ({ width = 18, height = 18, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M14 6.66688V5.33355H11.6094L14.4714 2.47162L13.5286 1.52881L10.6666 4.39074V2.00021H9.3333V6.66688H14ZM4.66664 4.66688V8.00022H3.3333V3.33355H7.99997V4.66688H4.66664ZM11.3333 8.00022V11.3336H7.99997V12.6669H12.6666V8.00022H11.3333ZM2.47137 14.4716L5.3333 11.6097V14.0002H6.66664V9.33355H1.99997V10.6669H4.39049L1.52856 13.5288L2.47137 14.4716Z" fill="white" />
  </svg>

);

NormalScreenIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default NormalScreenIcon;
