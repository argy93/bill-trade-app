import React from 'react';
import PropTypes from 'prop-types';

const DownArrowIcon = ({ width = 11, height = 7, className }) => (
  <svg width={width} height={height} className={className} viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M5.61842 4.50809L9.69276 0.644499C9.92369 0.425507 10.2856 0.425507 10.5166 0.644499C10.7655 0.880571 10.7655 1.27716 10.5166 1.51323L5.61842 6.15801L0.720286 1.51323C0.471338 1.27716 0.471339 0.88057 0.720287 0.644499C0.951224 0.425507 1.31315 0.425507 1.54408 0.644499L5.61842 4.50809Z" />
  </svg>

);

DownArrowIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default DownArrowIcon;
