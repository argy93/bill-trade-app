import React from 'react';
import PropTypes from 'prop-types';

const SearchIcon = ({ width = 21, height = 21, className }) => (
  <svg className={className} width={width} height={height} viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M8.41634 16.7497C3.81397 16.7497 0.0830078 13.0187 0.0830078 8.41634C0.0830078 3.81397 3.81397 0.0830078 8.41634 0.0830078C13.0187 0.0830078 16.7497 3.81397 16.7497 8.41634C16.7497 10.3421 16.0965 12.1153 14.9995 13.5264L20.6112 19.1381L19.1381 20.6112L13.5264 14.9995C12.1153 16.0965 10.3421 16.7497 8.41634 16.7497ZM14.6663 8.41634C14.6663 11.8681 11.8681 14.6663 8.41634 14.6663C4.96456 14.6663 2.16634 11.8681 2.16634 8.41634C2.16634 4.96456 4.96456 2.16634 8.41634 2.16634C11.8681 2.16634 14.6663 4.96456 14.6663 8.41634Z" />
  </svg>

);

SearchIcon.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string
};

export default SearchIcon;
