/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-array-index-key */
/**
 *
 * Profit tab content component
 *
 */

// main imports
import React from 'react';
import SubscriptionStats from '../SubscriptionStats';

// components imports
import StackedCards from '../StackedCards';

// style imports
import './style';


// eslint-disable-line react/prefer-stateless-function
export default class Profit extends React.PureComponent {
  state = {
    stackedCardsData: [
      {
        title: 'Profit:',
        value: '8,779 USD',
        description: '0.6278 BTC',
        background: '#20AD65',
        color: '#E1F9ED'
      },
      {
        title: 'Balance:',
        value: '5,050 BTC',
        description: '50,345,123 USD',
      },
    ],
  }

  render() {
    const {
      stackedCardsData
    } = this.state;

    const signalsBody = [{
      name: 'Copying',
      value: '127',
      amount: '0.2123 BTC',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234556',
          value: '211 USD'
        }
      ]
    }, {
      name: 'Analysts',
      value: '127',
      amount: '0.2123 BTC',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234556',
          value: '211 USD'
        }
      ]
    }, {
      name: 'Copier profit',
      value: '127',
      amount: '0.2123 BTC',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234556',
          value: '211 USD'
        }
      ]
    }, {
      name: 'Analysts profit',
      value: '127',
      amount: '0.2123 BTC',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234556',
          value: '211 USD'
        }
      ]
    }, {
      name: 'Paid to analysts',
      value: '127',
      amount: '0.2123 BTC',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234556',
          value: '211 USD'
        }
      ]
    },
    ];

    const referalHeader = ['Members type', 'Q-ty', 'Amount'];

    const referalBody = [
      {
        name: 'From traders, 1%',
        value: '2',
        amount: '422 USD',
        isOpened: true,
        subitems: [{
          name: 'Trader1234556',
          value: '211 USD'
        }, {
          name: 'Trader1234556',
          value: '211 USD'
        }]
      },
      {
        name: 'From investors, 3%',
        value: '2',
        amount: '422 USD',
        isOpened: false,
        subitems: [{
          name: 'Trader1234556',
          value: '211 USD'
        }, {
          name: 'Trader1234556',
          value: '211 USD'
        }]
      },
      {
        name: 'From traders, 2%',
        value: '2',
        amount: '422 USD',
        isOpened: false,
        subitems: [{
          name: 'Trader1234556',
          value: '211 USD'
        }, {
          name: 'Trader1234556',
          value: '211 USD'
        }]
      },
      {
        name: 'From investors, 1%',
        value: '2',
        amount: '422 USD',
        isOpened: false,
        subitems: [{
          name: 'Trader1234556',
          value: '211 USD'
        }, {
          name: 'Trader1234556',
          value: '211 USD'
        }]
      },
    ];
    return (
      <div className="l-profit">
        <StackedCards items={stackedCardsData} />
        <div className="section">
          <div className="column">
            <SubscriptionStats
              title="Revenue from sending signals"
              body={signalsBody}
            />
          </div>
          <div className="column">
            <div className="total-card">
              <div className="label">Total:</div>
              <div className="value-usd">4.779 USD</div>
              <div className="value-btc">0.6221 BTC</div>
            </div>
          </div>
        </div>
        <div className="section">
          <div className="column">
            <SubscriptionStats
              title="Revenue from referral program"
              header={referalHeader}
              body={referalBody}
            />
          </div>
          <div className="column">
            <div className="total-card">
              <div className="label">Total:</div>
              <div className="value-usd">4.779 USD</div>
              <div className="value-btc">0.6221 BTC</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Profit.propTypes = {

};
