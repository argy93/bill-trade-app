import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import Chart, { propsChartData } from '../Chart';
import ColorCounter from '../../utils/ColorCounter';

class ChartWithControls extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      highlight: false,
      activeFilter: 'c',
    };
  }

  changeFilter(type) {
    this.setState({
      activeFilter: type,
    });
  }

  render() {
    const { highlight, activeFilter } = this.state;
    const {
      transactions,
      percentTransactions,
      height,
      chartTitle
    } = this.props;

    const items = activeFilter === 'p' ? percentTransactions : transactions;
    const colorCounter = new ColorCounter();

    if (!items.length) return ('');

    return (
      <div className="c-chart-with-controls">
        <div className="c-chart-with-controls__chart">
          <Chart
            items={items}
            highlight={highlight}
            height={height}
            bubbleFormatter={activeFilter === 'p' ? (value) => `${value.toFixed()}%` : null}
          />
        </div>
        <div className="c-chart-with-controls__controls">
          {chartTitle ? (
            <div className="c-chart-with-controls__title">
              Activity graph
            </div>
          ) : null}
          <div className="c-chart-with-controls__history">
            {
              items.map((item, index) => (
                <div
                  className="c-chart-with-controls__currency"
                  onMouseEnter={() => this.setState({ highlight: index })}
                  onMouseLeave={() => this.setState({ highlight: false })}
                  key={item.name}
                >
                  <span
                    className="c-chart-with-controls__currency-feature"
                    style={{
                      background: colorCounter.getNextColor(),
                    }}
                  />
                  <span className="c-chart-with-controls__currency-name">{item.name}</span>
                </div>
              ))
            }
          </div>
          <div className="c-chart-with-controls__type">
            <button
              type="button"
              className={activeFilter === 'p' ? 'is-active' : ''}
              onClick={() => this.changeFilter('p')}
            >
              Percent
            </button>
            <button
              type="button"
              className={activeFilter === 'c' ? 'is-active' : ''}
              onClick={() => this.changeFilter('c')}
            >
              Currency
            </button>
          </div>
        </div>
      </div>
    );
  }
}

ChartWithControls.propTypes = {
  transactions: propsChartData,
  percentTransactions: propsChartData,
  height: PropTypes.number,
  chartTitle: PropTypes.bool
};

export default ChartWithControls;
