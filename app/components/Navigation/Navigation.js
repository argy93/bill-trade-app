/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import './style.scss';
import PropTypes from 'prop-types';
import UserIcon from '../Icons/UserIcon';
import TopMembersIcon from '../Icons/TopMembersIcon';
import TraidingTerminalIcon from '../Icons/TraidingTerminalIcon';
import AffiliateProgramIcon from '../Icons/AffiliateProgramIcon';
import PaymentIcon from '../Icons/PaymentIcon';
import SupportIcon from '../Icons/SupportIcon';
import SettingsIcon from '../Icons/SettingsIcon';
import LogOutIcon from '../Icons/LogOutIcon';
import LangSelect from '../LangSelect';
import Auth from '../../containers/App/auth';

// eslint-disable-next-line react/prefer-stateless-function
class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.navigation = [
      {
        icon: (<UserIcon />),
        path: '/',
        title: 'My profile',
      },
      {
        icon: (<TopMembersIcon />),
        path: '/top-members',
        title: 'Top members',
      },
      {
        icon: (<TraidingTerminalIcon />),
        path: '/trading-terminal',
        title: 'Traiding terminal',
      },
      {
        icon: (<AffiliateProgramIcon />),
        path: '/affiliate-program',
        title: 'Affiliate program',
      },
      {
        icon: (<PaymentIcon />),
        path: '/payment',
        title: 'Payment',
      },
      {
        icon: (<SupportIcon />),
        path: '/support',
        title: 'Support',
      },
      {
        icon: (<SettingsIcon />),
        path: '/settings',
        title: 'Settings',
      },
    ];
  }

  static getNavigationItem(item, activeClass) {
    const itemStatus = activeClass ? 'is-active' : '';

    return (
      <li className={itemStatus} key={item.title}>
        <Link to={item.path}>
          {item.icon}
          <span>{item.title}</span>
        </Link>
      </li>
    );
  }

  handleLogout = () => {
    Auth.logout();
    window.location.href = '/';
  }

  render() {
    const {
      location: {
        pathname,
      },
    } = this.props;

    return (
      <nav className="c-sidebar__nav">
        <ul className="c-sidebar__nav-list">
          {this.navigation.map((item) => {
            const activeClass = item.path === pathname;
            return Navigation.getNavigationItem(item, activeClass);
          })}
          <li onClick={this.handleLogout}>
            <div className="logout">
              <LogOutIcon />
              <span>Log out</span>
            </div>
          </li>
        </ul>
        {window.innerWidth < 768 ? (
          <LangSelect
            position={'top'}
          />
        ) : null}
      </nav>
    );
  }
}

const NavigationWithRouter = withRouter((props) => <Navigation {...props} />);

Navigation.propTypes = {
  location: PropTypes.object
};

export default NavigationWithRouter;
