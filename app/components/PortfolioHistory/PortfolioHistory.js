import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

// eslint-disable-next-line react/prefer-stateless-function
class PortfolioHistory extends React.Component {
  render() {
    return (
      <div className="c-portfolio-history">
        <div className="c-portfolio__title c-content-card-title">Live trading history</div>
        <div className="c-portfolio-history__row">
          <div className="c-portfolio-history__table">
            <div className="c-portfolio__col">
              <span className="c-portfolio-history__title">Date & time</span>
              <span className="c-portfolio-history__text">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text is-loss">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text is-loss">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text is-loss">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text is-loss">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text">29.01.2019 16:19</span>
              <span className="c-portfolio-history__text is-loss">29.01.2019 16:19</span>
            </div>
            <div className="c-portfolio__col">
              <span className="c-portfolio-history__title">Currency</span>
              <span className="c-portfolio-history__text">BTC</span>
              <span className="c-portfolio-history__text">BTC</span>
              <span className="c-portfolio-history__text">BTC</span>
              <span className="c-portfolio-history__text is-loss">BTC</span>
              <span className="c-portfolio-history__text is-loss">BTC</span>
              <span className="c-portfolio-history__text is-loss">BTC</span>
              <span className="c-portfolio-history__text is-loss">BTC</span>
              <span className="c-portfolio-history__text">BTC</span>
              <span className="c-portfolio-history__text">BTC</span>
              <span className="c-portfolio-history__text is-loss">BTC</span>
            </div>
            <div className="c-portfolio__col">
              <span className="c-portfolio-history__title">Order</span>
              <span className="c-portfolio-history__text">SELL</span>
              <span className="c-portfolio-history__text">SELL</span>
              <span className="c-portfolio-history__text">SELL</span>
              <span className="c-portfolio-history__text is-loss">SELL</span>
              <span className="c-portfolio-history__text is-loss">SELL</span>
              <span className="c-portfolio-history__text is-loss">SELL</span>
              <span className="c-portfolio-history__text is-loss">SELL</span>
              <span className="c-portfolio-history__text">SELL</span>
              <span className="c-portfolio-history__text">SELL</span>
              <span className="c-portfolio-history__text is-loss">SELL</span>
            </div>
            <div className="c-portfolio__col">
              <span className="c-portfolio-history__title">Open/close</span>
              <span className="c-portfolio-history__text">Open</span>
              <span className="c-portfolio-history__text">Open</span>
              <span className="c-portfolio-history__text">Open</span>
              <span className="c-portfolio-history__text is-loss">Open</span>
              <span className="c-portfolio-history__text is-loss">Open</span>
              <span className="c-portfolio-history__text is-loss">Open</span>
              <span className="c-portfolio-history__text is-loss">Open</span>
              <span className="c-portfolio-history__text">Open</span>
              <span className="c-portfolio-history__text">Open</span>
              <span className="c-portfolio-history__text is-loss">Open</span>
            </div>
            <div className="c-portfolio__col">
              <span className="c-portfolio-history__title">Stock</span>
              <span className="c-portfolio-history__text">Bitmex</span>
              <span className="c-portfolio-history__text">Bitmex</span>
              <span className="c-portfolio-history__text">Bitmex</span>
              <span className="c-portfolio-history__text is-loss">Bitmex</span>
              <span className="c-portfolio-history__text is-loss">Bitmex</span>
              <span className="c-portfolio-history__text is-loss">Bitmex</span>
              <span className="c-portfolio-history__text is-loss">Bitmex</span>
              <span className="c-portfolio-history__text">Bitmex</span>
              <span className="c-portfolio-history__text">Bitmex</span>
              <span className="c-portfolio-history__text is-loss">Bitmex</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PortfolioHistory.propTypes = {};

export default PortfolioHistory;
