/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-restricted-globals */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-array-index-key */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import axios from 'axios';

import Switch from '../Switch';
import SubscriptionSidebar from '../SubscriptionSidebar';
import ButtonSmall from '../ButtonSmall';
import Button from '../Button';
import Currency from '../Icons/CurrencyIcon';
import {
  BALANCES,
  ACCOUNT,
  EXCHANGE_CONVERT
} from './constants';

import iconPlus from './icon-plus.svg';
import iconMinus from './icon-minus.svg';
import pieChart from './pie-chart.svg';
import arrowRight from './icon.svg';
import checkMark from './check-mark.svg';
import circleBlock from './circle-block.svg';
import handShake from './hand-shake.svg';
import avatar from './user-avatar.png';

class Subscription extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.loadBalances();
    this.loadAccountData();
    this.include = this.include.bind(this);
    this.exclude = this.exclude.bind(this);
  }

  state = {
    currentStep: 0,
    allocated_percent: 100,
    multiplier_percent: 100,
    exclude: false,
    include: false,
    stats: false,
    statsData: null,
    primaryCurrency: '',
    excluded: [],
    included: [],
    return_orders: true,
    fix_lot_percent: false,
    fix_lot_percent_value: '',
    fix_lot_value: false,
    fix_lot_data: [
      {
        name: 'BTC',
        enabled: false,
        value: 0,
      },
      {
        name: 'ETH',
        enabled: false,
        value: 0,
      },
      {
        name: 'USDT',
        enabled: false,
        value: 0
      },
    ],
    currentChartData: {
      labels: [],
      series: [],
      data: [],
    },
  };

  calculateSubscribedAmountByPercent = (stockName, stockCurrency, value) => {
    const {
      currentChartData
    } = this.state;
    const reg = new RegExp('%');
    const valueFormated = value.replace(reg, '');

    this.setState({
      currentChartData: {
        ...currentChartData,
        options: {
          ...currentChartData.options,
          labels: currentChartData.data.map((stock) => this.formatCurrency(stock.subscribedAmount))
        },
        series: currentChartData.data.map((stock) => this.formatCurrency(stock.percent, 2)),
        data: currentChartData.data.map((stock) => {
          if (stock.stockName === stockName && stock.stockCurrency === stockCurrency) {
            return {
              ...stock,
              allocatedPercent: valueFormated,
              subscribedAmount: stock.value * (valueFormated / 100)
            };
          }
          return stock;
        })
      }
    });
  }

  calculateStockAmountByPercent = (stockName, stockCurrency, value) => {
    const { currentChartData } = this.state;
    const subscribedAmount = Number(value);

    this.setState({
      currentChartData: {
        ...currentChartData,
        options: {
          ...currentChartData.options,
          labels: currentChartData.data.map((stock) => this.formatCurrency(stock.subscribedAmount))
        },
        series: currentChartData.data.map((stock) => this.formatCurrency(stock.percent)),
        data: currentChartData.data.map((stock) => {
          if (stock.stockName === stockName && stock.stockCurrency === stockCurrency) {
            return {
              ...stock,
              allocatedPercent: subscribedAmount / (stock.value / 100),
              subscribedAmount,
            };
          }
          return stock;
        })
      }
    });
  }

  exclude(index) {
    const { currentChartData } = this.state;
    const updateData = currentChartData.data[index];
    const updated = [
      ...currentChartData.data.slice(0, index),
      {
        ...currentChartData.data[index],
        excluded: !updateData.excluded,
        included: false,
      },
      ...currentChartData.data.slice(index + 1)
    ];


    this.setState({
      currentChartData: {
        ...currentChartData,
        data: updated
      }
    }, () => this.setExcluded(this.state.currentChartData.data.filter((value) => value.excluded)));
  }

  createChartData = (chartName, labels = this.state[chartName].labels, series = this.state[chartName].series) => ({
    options: {
      dataLabels: {
        enabled: false,
      },
      legend: {
        show: false,
      },
      plotOptions: {
        pie: {
          donut: {
            labels: {
              show: true,
            },
            size: '90%'
          }
        }
      },
      labels,
    },
    series,
  });

  include(index) {
    const { currentChartData } = this.state;

    const updateData = currentChartData.data[index];
    const updated = [
      ...currentChartData.data.slice(0, index),
      {
        ...currentChartData.data[index],
        included: !updateData.included,
        excluded: false,
      },
      ...currentChartData.data.slice(index + 1)
    ];

    this.setState({
      currentChartData: {
        ...currentChartData,
        data: updated
      }
    }, () => this.setIncluded(this.state.currentChartData.data.filter((value) => value.included)));
  }

  handleAllocatedPercentUpdate = (allocatedPercent) => {
    const { currentChartData } = this.state;
    this.setState({
      currentChartData: {
        ...currentChartData,
        options: {
          ...currentChartData.options,
          labels: currentChartData.data.map((stock) => this.formatCurrency(stock.subscribedAmount))
        },
        series: currentChartData.data.map((stock) => this.formatCurrency(stock.percent)),
        data: currentChartData.data.map((stock) => ({
          ...stock,
          subscribedAmount: allocatedPercent * (stock.value / 100),
          allocatedPercent,
        }))
      }
    });
  }

  loadBalances = () => {
    axios.get(BALANCES)

      .then(({ data }) => {
        this.setState({
          statsData: data.data,
        }, this.formatStocksData);
      });
  }

  formatStocksData = () => {
    const {
      statsData,
      allocated_percent
    } = this.state;

    if (!statsData) {
      return;
    }

    const stocksList = [];

    const {
      currentChartData
    } = this.state;

    const {
      stocks,
      total
    } = statsData;

    Object.keys(stocks).map((stock) => Object.keys(stocks[stock]).map((currency) => stocksList.push({
      stockName: stock,
      stockCurrency: currency,
      value: stocks[stock][currency]
    })));

    this.getExchangeRates(stocksList)
      .then(({
        data
      }) => {
        const exchange = data.data;
        const currentInvestmentChartData = {
          labels: [],
          series: []
        };
        const stocksData = stocksList.map((stock) => {
          const rates = exchange[stock.stockCurrency][stock.stockName];
          const percent = stock.value * rates.USDT / total.USDT * 100;

          currentInvestmentChartData.labels.push(this.formatCurrency(stock.value));
          currentInvestmentChartData.series.push(this.formatCurrency(percent, 2));

          return {
            ...stock,
            rates,
            percent,
            allocatedPercent: allocated_percent,
            excluded: false,
            included: false,
            subscribedAmount: stock.value * (allocated_percent / 100)
          };
        });

        this.setState({
          currentChartData: {
            ...currentChartData,
            labels: currentInvestmentChartData.labels,
            series: currentInvestmentChartData.series,
            data: stocksData,
          },
        });
      })
      .catch(console.log);
  }

  calculateProgress = () => {
    const {
      statsData,
      currentChartData
    } = this.state;

    const progressCounterObject = {
      value: 0,
      percent: 0,
      progress: 0,
      currencies: {
        BTC: 0,
        ETH: 0,
        USDT: 0,
      }
    };

    if (!currentChartData.data.length) return progressCounterObject;


    currentChartData.data.map((stock) => {
      if (!stock.excluded) {
        progressCounterObject.percent += Number(stock.allocatedPercent);
        progressCounterObject.value += stock.subscribedAmount * stock.rates.USDT;
        progressCounterObject.currencies.BTC += stock.subscribedAmount * stock.rates.BTC;
        progressCounterObject.currencies.ETH += stock.subscribedAmount * stock.rates.ETH;
        progressCounterObject.currencies.USDT += stock.subscribedAmount * stock.rates.USDT;
      }
    });

    progressCounterObject.percent /= currentChartData.data.length;
    progressCounterObject.progress = (progressCounterObject.value / (statsData.total.USDT / 100)) * 100 / progressCounterObject.percent || 0;

    return progressCounterObject;
  }

  getExchangeRates = (data) => {
    let exchangeRates = '?base=';

    data.map(({ stockCurrency }) => {
      exchangeRates += `${stockCurrency},`;
    });

    exchangeRates = exchangeRates.substr(0, exchangeRates.length - 1);
    return axios.get(EXCHANGE_CONVERT + exchangeRates);
  }

  loadAccountData = () => {
    axios.get(ACCOUNT)
      .then(({ data }) => {
        console.log(data.data);
        this.setState({
          primaryCurrency: data.data.primary_currency,
        });
      });
  }

  handleNextStep = () => {
    const { currentStep } = this.state;
    if (currentStep === 0 && this.calculateProgress().progress < 99.99) {
      return;
    }
    this.setState({
      currentStep: currentStep + 1
    });
  }

  handleBackStep = () => {
    const { currentStep } = this.state;
    this.setState({
      currentStep: currentStep - 1
    });
  }

  handleExcludeToggle = () => {
    const { exclude } = this.state;
    this.setState({
      exclude: !exclude,
      include: false,
      stats: true
    });
  }

  handleIncludeToggle = () => {
    const { include } = this.state;
    this.setState({
      include: !include,
      exclude: false,
      stats: true
    });
  }

  handleStatsToggle = () => {
    const { stats, include, exclude } = this.state;
    this.setState({
      stats: !stats,
      exclude: stats ? false : exclude,
      include: stats ? false : include,
    });
  }

  handleDataChange = (name, value) => {
    this.setState({
      [name]: Number(value),
    }, () => name === 'allocated_percent' && this.handleAllocatedPercentUpdate(value));
  }

  isAllocatedPercentValid = (value) => {
    if ((Number(value) <= 100 && Number(value) > 0) || value === '') {
      return true;
    }
    return false;
  }

  setExcluded = (data) => {
    this.setState({
      excluded: data,
    });
  }

  setIncluded = (data) => {
    this.setState({
      included: data,
    });
  }

  renderExcluded = () => {
    const { excluded } = this.state;
    return excluded.map((excludedItem, excludedIndex) => (
      <div className="label" key={excludedIndex}>
        <div className="text">{ this.formatCurrency(excludedItem.percent, 2) }%</div>
        <div className="text">{ this.formatCurrency(excludedItem.value) }</div>
        <div className="text">{ excludedItem.stockCurrency }</div>
      </div>
    ));
  }

  renderIncluded = () => {
    const { included } = this.state;
    return included.map((includedItem, includedIndex) => (
      <div className="label" key={includedIndex}>
        <div className="text">{ this.formatCurrency(includedItem.percent, 2) } %</div>
        <div className="text">{ this.formatCurrency(includedItem.value) }</div>
        <div className="text">{ includedItem.stockCurrency }</div>
      </div>
    ));
  }

  formatCurrency = (currency, amount = 4) => Number(parseFloat(Math.round(currency * 10000) / 10000).toFixed(amount));

  renderFixLotIcons = () => {
    const { fix_lot_data, fix_lot_value } = this.state;
    return fix_lot_data.map((icon, index) => (
      <Currency
        key={index}
        icon={icon.name}
        isActive={icon.enabled}
        disabled={!fix_lot_value}
        onClick={() => this.handleLotIconClick(index)}
      />
    ));
  }

  handleLotIconClick = (index) => {
    const { fix_lot_data } = this.state;
    this.setState({
      fix_lot_data: fix_lot_data.map((data, iterator) => {
        if (iterator === index) {
          return { ...data, enabled: true };
        }
        return { ...data, enabled: false, value: 0 };
      })
    });
  }

  renderFixLotPercentInput = () => {
    const {
      fix_lot_percent,
      fix_lot_percent_value
    } = this.state;

    if (fix_lot_percent) {
      return (
        <input
          type="text"
          className="fix-input"
          placeholder="%"
          defaultValue={fix_lot_percent_value}
          onChange={this.handleFixLotPercentInputChange}
        />
      );
    }
    return '';
  }

  handleFixLotPercentInputChange = ({ target }) => {
    this.setState({
      fix_lot_percent_value: target.value,
    });
  }

  renderFixLotValueInput = () => {
    const { fix_lot_data } = this.state;
    const active = fix_lot_data.filter((fix_item) => fix_item.enabled);
    if (active.length) {
      return (
        <input
          type="text"
          className="fix-input"
          placeholder={active[0].name}
          defaultValue={active[0].value}
          onChange={({ target }) => this.handleFixLotInputChange(active[0].name, target.value)}
        />
      );
    }
    return '';
  }

  handleFixLotInputChange = (currencyName, value) => {
    const { fix_lot_data } = this.state;
    this.setState({
      fix_lot_data: fix_lot_data.map((fixLotItem) => {
        if (fixLotItem.name === currencyName && fixLotItem.enabled) {
          return { ...fixLotItem, value };
        }
        return fixLotItem;
      })
    });
  }

  handleFixLotPercentChange = () => {
    const { fix_lot_percent, fix_lot_data } = this.state;
    this.setState({
      fix_lot_percent: !fix_lot_percent,
      fix_lot_value: false,
      fix_lot_data: fix_lot_data.map((fix) => ({ ...fix, enabled: false, value: '' })),
    });
  }

  handleFixLotValueChange = () => {
    const { fix_lot_value, fix_lot_data } = this.state;
    this.setState({
      fix_lot_value: !fix_lot_value,
      fix_lot_percent: false,
      fix_lot_data: fix_lot_data.map((fix) => ({ ...fix, enabled: false, value: '' })),
    });
  }

  handleReturnOrdersChange = () => {
    const { return_orders } = this.state;
    this.setState({
      return_orders: !return_orders
    });
  }

  handleSubscribe = () => {
    const investProgress = this.calculateProgress();

    const {
      allocated_percent,
      multiplier_percent,
      return_orders,
      fix_lot_percent,
      fix_lot_percent_value,
      fix_lot_value,
      fix_lot_data,
      excluded,
      included,
    } = this.state;

    const { signalistDisplayName } = this.props;

    const active = fix_lot_data.filter((fix_item) => fix_item.enabled);

    if (fix_lot_value && !active.length) return;

    const dataToSend = {
      signalist_display_name: signalistDisplayName || 'semakoua',
      allocated_percent,
      multiplier_percent,
      return_orders,
      fix_lot: {
        percent: {
          enabled: fix_lot_percent,
          value: Number(fix_lot_percent_value)
        },
        currency: {
          enabled: fix_lot_value,
        }
      },
      investment: investProgress.currencies,
      assets_excluded: excluded
        .filter((stockItem) => stockItem.excluded)
        .map((stockItem) => stockItem.stockCurrency),
      assets_prioritized: included
        .filter((stockItem) => stockItem.included)
        .map((stockItem) => stockItem.stockCurrency),
    };

    if (fix_lot_value) {
      dataToSend.fix_lot.currency.value = active[0].value;
      dataToSend.fix_lot.currency.currency = active[0].name;
    }

    console.log(dataToSend);


    this.setState({
      stats: false,
      currentStep: 3,
    });
  }

  handleClose = () => {
    this.setState({
      currentStep: 0,
    });
  }

  render() {
    const {
      currentStep,
      currentChartData,
      allocated_percent,
      multiplier_percent,
      exclude,
      include,
      stats,
      excluded,
      included,
      statsData,
      primaryCurrency,
      return_orders,
      fix_lot_percent,
      fix_lot_value
    } = this.state;


    const investProgress = this.calculateProgress();

    const showStatsTemplate = (
      <div className="show-stats">
        <div className="title-container">
          <div className="column">
            <img src={pieChart} alt="hide stats" className="icon" />
            {stats ? 'Hide Stats' : 'Show stats'}
          </div>
          <div className="column">
            <div className="toggle">
              <Switch
                name="stats"
                isOn={stats}
                handleToggle={this.handleStatsToggle}
                onColor="#1F2123"
              />
            </div>
          </div>
        </div>
      </div>
    );

    const stepOne = (
      <div className="body">
        <div className="allocated-funds">
          <div className="title">
            Allocated funds
            <div className="input-container">
              <input
                type="text"
                className="numbered-input"
                value={allocated_percent}
                onChange={
                  ({ target }) => (this.isAllocatedPercentValid(target.value)
                    ? this.handleDataChange('allocated_percent', Number(target.value)) : '')
                }
              />
              <div className="percent">%</div>
            </div>
          </div>
          <div className="description">The allocated amount of funds, which is automatically converted into an exact copy of the signalist’s portfolio.</div>
          <div className="assets">
            <div className="exclude-assets">
              <div className="title-container">
                <div className="column">
                  <img src={iconMinus} alt="" className="icon" />
                  Exclude asset
                </div>
                <div className="column">
                  <div className="toggle">
                    <Switch
                      name="exclude"
                      isOn={exclude}
                      handleToggle={this.handleExcludeToggle}
                      onColor="#1F2123"
                    />
                  </div>
                </div>
              </div>
              <div className="asset-description">
                {!excluded.length
                  ? 'The excluded asset asset does not participate in portfolio formation after a member.'
                  : this.renderExcluded()}
              </div>
            </div>
            <div className="include-assets">
              <div className="title-container">
                <div className="column">
                  <img src={iconPlus} alt="" className="icon" />
                  Include asset
                </div>
                <div className="column">
                  <div className="toggle">
                    <Switch
                      name="include"
                      isOn={include}
                      handleToggle={this.handleIncludeToggle}
                      onColor="#1F2123"
                    />
                  </div>
                </div>
              </div>
              <div className="asset-description">
                {!included.length
                  ? 'The included asset is a priority for forming a portfolio after a member.'
                  : this.renderIncluded()}
              </div>
            </div>
            {showStatsTemplate}
          </div>
        </div>
      </div>
    );

    const stepTwo = (
      <div className="body step-two">
        <div className="allocated-funds">
          <div className="title">
            Multiplier
            <div className="input-container">
              <input
                type="text"
                className="numbered-input"
                value={multiplier_percent}
                onChange={
                  ({ target }) => (this.isAllocatedPercentValid(target.value)
                    ? this.handleDataChange('multiplier_percent', Number(target.value)) : '')
                }
              />
              <div className="percent">%</div>
            </div>
          </div>
          <div className="description">Default trader rate multiplier is 100% and is calculated from the allocated portfolio. With 100% and 1% of a signalist per trade, your base order size will be &quot;Allocated&quot; * 1% = 1% of the amount allocated.</div>
          <div className="assets">
            <div className="exclude-assets">
              <div className="title-container">
                <div className="column">
                  Return orders
                </div>
                <div className="column">
                  <div className="toggle">
                    <Switch
                      name="return_orders"
                      isOn={return_orders}
                      handleToggle={this.handleReturnOrdersChange}
                      onColor="#1F2123"
                    />
                  </div>
                </div>
              </div>
              <div className="asset-description">
                Automated return of positions in case of intervention, closing or withdrawal of an asset outside the signalist`s trade. In the event of an accidental sale of an asset that is allocated to a signalist, the system will automatically buy it from available funds.
              </div>
            </div>
            <div className="include-assets">
              <div className={fix_lot_percent ? 'title-container' : 'title-container active'}>
                <div className="column">
                  Fix lot, %
                </div>
                <div className="column fix-input-container">
                  {this.renderFixLotPercentInput()}
                </div>
                <div className="column">
                  <div className="toggle">
                    <Switch
                      name="fix_lot_percent"
                      isOn={fix_lot_percent}
                      handleToggle={this.handleFixLotPercentChange}
                      onColor="#1F2123"
                    />
                  </div>
                </div>
              </div>
              <div className="asset-description">
                Fixed lot as a percentage of the allocated amount to the signalist.It works with all types of orders and assets.
              </div>
            </div>
            <div className="include-assets">
              <div className={fix_lot_value ? 'title-container' : 'title-container active'}>
                <div className="column title-lot">
                  Fix lot in
                </div>
                <div className="column icons">
                  {this.renderFixLotIcons()}
                </div>
                <div className="column fix-input-container">
                  {this.renderFixLotValueInput()}
                </div>
                <div className="column">
                  <div className="toggle">
                    <Switch
                      name="fix_lot_value"
                      isOn={fix_lot_value}
                      handleToggle={this.handleFixLotValueChange}
                      onColor="#1F2123"
                    />
                  </div>
                </div>
              </div>
              <div className="asset-description">
                Fixed lot in an allocated asset.It works with all types of orders and assets.
              </div>
            </div>
            {showStatsTemplate}
          </div>
        </div>
      </div>
    );

    const stepThree = (
      <div className="p-summary-step">
        <div className="submited-title">
          <span className="text">Allocated</span>
          <span className="value">{allocated_percent}%</span>
        </div>
        <div className="submited-title">
          <span className="text">Multiplier</span>
          <span className="value">{multiplier_percent}%</span>
        </div>
        <div className="submited-title">
          <span className="text">Return orders</span>
          <span className="value">{
            return_orders
              ? <img src={checkMark} alt="" />
              : <img src={circleBlock} alt="" />}
          </span>
        </div>
        <div className="divider"></div>
        <div className="notes-title">Note:</div>
        <ul className="notes">
          <li className="note">{allocated_percent} % of the total available portfolio will be converted into a similar signalist `s portfolio.</li>
          <li className="note">0.2 BTC will be included in the priority for the formation of a new portfolio.</li>
          <li className="note">0.2 BTC will be received from the sale of altcoins with the lowest capitalization.</li>
        </ul>
        <div className="operation-currency">
          <div className="label">{primaryCurrency}</div>
          <span className="text">Operations are carried out in dollars</span>
        </div>
        <div className="divider"></div>
        {showStatsTemplate}
      </div>
    );

    const backButton = (
      <div className="back-button" onClick={this.handleBackStep}>
        <img src={arrowRight} alt="" />
        Back
      </div>
    );

    const nextButton = (
      <div
        className={investProgress.progress >= 100 ? 'next-button active' : 'next-button'}
        onClick={this.handleNextStep}
      >
        Next
        <img src={arrowRight} alt="" />
      </div>
    );

    const subscribeButton = (
      <ButtonSmall color="#1D62C9" onClick={this.handleSubscribe}>Subscribe</ButtonSmall>
    );

    const subscribedStep = (
      <div className="subscribed-step">
        <img src={avatar} alt="user-avatar" className="avatar" />
        <img src={handShake} alt="hand-shake" className="hand-shake" />
        <div className="congrat-title">Congratulations!</div>
        <div className="congrat-description">You are successfully subscribed to <b>Irvine1290</b></div>
        <div className="button-container">
          <Button onClick={this.handleClose} type="outlined">Close</Button>
        </div>
      </div>
    );

    const header = (
      <div className="header">
        <h6 className="title">
          You are going to subscribe to Irvine1290
          {primaryCurrency ? <div className="label">{primaryCurrency}</div> : ''}
        </h6>
      </div>
    );

    const footer = (
      <div className="footer">
        {currentStep > 0 ? backButton : ''}
        {currentStep === 2 ? subscribeButton : nextButton}
      </div>
    );

    return (
      <>
        <div className="c-subscription">
          { currentStep !== 3 ? header : '' }
          { currentStep === 0 ? stepOne : '' }
          { currentStep === 1 ? stepTwo : '' }
          { currentStep === 2 ? stepThree : '' }
          { currentStep === 3 ? subscribedStep : '' }
          { currentStep !== 3 ? footer : '' }
        </div>
        <SubscriptionSidebar
          stats={stats}
          statsData={statsData}
          include={include}
          exclude={exclude}
          handleInclude={this.include}
          handleExclude={this.exclude}
          currentChartData={currentChartData}
          calculateStockAmountByPercent={this.calculateStockAmountByPercent}
          calculateSubscribedAmountByPercent={this.calculateSubscribedAmountByPercent}
          investProgress={investProgress}
          currentStep={currentStep}
          createChartData={this.createChartData}
        />
      </>
    );
  }
}

Subscription.propTypes = {
  signalistDisplayName: PropTypes.string.isRequired,
};

export default Subscription;
