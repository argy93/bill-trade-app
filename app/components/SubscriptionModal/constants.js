import { BASE_URL } from '../../containers/App/constants';

export const BALANCES = `${BASE_URL}/account/me/balances?stocks=BINANCE,BITMEX`;
export const ACCOUNT = `${BASE_URL}/account/me`;
export const EXCHANGE_CONVERT = `${BASE_URL}/exchange-rates/convert`;
