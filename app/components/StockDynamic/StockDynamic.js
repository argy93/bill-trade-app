import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class StockDynamic extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      rank,
      maxLoss,
      averageTime,
      trades,
      type,
      stats,
    } = this.props;

    console.log(rank, maxLoss, averageTime, trades, type, stats);

    return (
      <div className="c-stock-dynamic">

      </div>
    );
  }
}

StockDynamic.propTypes = {
  rank: PropTypes.number,
  maxLoss: PropTypes.number,
  averageTime: PropTypes.number,
  trades: PropTypes.number,
  type: PropTypes.string,
  stats: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      points: PropTypes.arrayOf(
        PropTypes.shape({
          timestamp: PropTypes.number,
          value: PropTypes.number,
        }),
      ),
    }),
  ),
};

export default StockDynamic;
