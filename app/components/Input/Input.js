/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/**
 *
 * Input
 *
 * This component is the skeleton of standart input
 * example <Input name="Password" type="password"></Input>
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';
import eye from 'images/icons/eye.png';
import eyeNo from 'images/icons/eye-no.png';

const Input = ({
  type = 'text',
  name,
  label = '',
  valid = true,
  touched = false,
  errorText = '',
  index,
  defaultValue,
  labelStyles = {},
  // eslint-disable-next-line no-console
  onChange = console.log,
  // eslint-disable-next-line no-console
  onBlur = console.log,
  showPassword = false,
  disabled = false,
  // eslint-disable-next-line no-console
  togglePassword = console.log,
  noPasswordIcon = false
}) => {
  // generate error text
  const error = valid ? '' : <span>{ errorText }</span>;
  const isValid = valid ? 'touched' : 'error';
  const validationClass = touched ? isValid : '';
  const eyeIcon = showPassword ? eyeNo : eye;

  return (
    <label className="c-input" htmlFor={name}>
      <div
        className={
          `c-input__label 
          ${validationClass}`
        }
        style={labelStyles}
      >
        { label }
      </div>
      <input
        defaultValue={defaultValue || ''}
        disabled={disabled}
        tabIndex={index}
        id={name}
        style={type === 'textarea' ? { height: 113 } : {}}
        type={(type === 'password' && showPassword) ? 'text' : type}
        className={`
          ${validationClass}
        `}
        onChange={(e) => onChange(e.target.value, name)}
        onBlur={() => onBlur(name)}
      />
      { type === 'password' && !noPasswordIcon ? (
        <img
          className="eye"
          src={eyeIcon}
          alt="eye"
          onClick={togglePassword}
        />
      ) : ''}
      <div className="c-input__error">{ touched ? error : '' }</div>
    </label>
  );
};

Input.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  valid: PropTypes.bool,
  touched: PropTypes.bool,
  showPassword: PropTypes.bool,
  errorText: PropTypes.string,
  onChange: PropTypes.func,
  labelStyles: PropTypes.object,
  onBlur: PropTypes.func,
  togglePassword: PropTypes.func,
  label: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  noPasswordIcon: PropTypes.bool,
  index: PropTypes.number,
};

export default Input;
