/**
 *
 * Button
 *
 * This component is the skeleton of standart button
 * types: contained, outlined, text
 * example <Button type="outlined">button text</Button>
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style';

const Button = ({
  type = 'contained',
  children,
  index,
  // eslint-disable-next-line no-console
  onClick = console.log,
}) => (
  <button
    tabIndex={index}
    type="button"
    className={`normal-button ${type}`}
    onClick={onClick}
  >
    {children}
  </button>
);

Button.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func.isRequired,
  index: PropTypes.number,
};

export default Button;
