import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import RoundedPlusIcon from '../Icons/RoundedPlusIcon';
import UserInfo from '../UserInfo';

class Posts extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { items, filterType, changePostsFilter } = this.props;

    return (
      <div className="c-news">
        <div className="c-news__header">
          <h4 className="c-news__header-title">Posts</h4>
          <button type="button" className="c-news__header-btn">
            <RoundedPlusIcon />
            <span>Create new post</span>
          </button>
        </div>
        <div className="c-news__nav">
          <button
            type="button"
            onClick={() => changePostsFilter('all')}
            className={filterType === 'all' ? 'is-active' : ''}
          >All
          </button>
          <button
            type="button"
            onClick={() => changePostsFilter('other')}
            className={filterType === 'other' ? 'is-active' : ''}
          >Others posts
          </button>
          <button
            type="button"
            onClick={() => changePostsFilter('self')}
            className={filterType === 'self' ? 'is-active' : ''}
          >Your posts
          </button>
        </div>
        <div className="c-news__body">
          {
            items.map((item, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <div className="c-post" key={`post${index}`}>
                <div className="c-post__user-info">
                  <UserInfo
                    width={'19px'}
                    height={'19px'}
                  />
                  <span className="user-name">{item.author}</span>
                  <span className="timestamp">{item.timestamp}</span>
                </div>
                <p className="c-post__excerpt">{item.excerpt}</p>
                <a href={item.short} className="c-post__link">Read more</a>
                <div className="c-post__img">
                  <img src={item.preview} alt="" />
                </div>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

Posts.propTypes = {
  changePostsFilter: PropTypes.func,
  filterType: PropTypes.oneOf(['all', 'other', 'self']),
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      photo: PropTypes.string,
      author: PropTypes.string,
      timestamp: PropTypes.number,
      short: PropTypes.string,
      excerpt: PropTypes.string,
      preview: PropTypes.string,
    }),
  ),
};

export default Posts;
