import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class AboutCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      text
    } = this.props;

    return (
      <div className="c-about-card">
        <span className="c-about-card__title">About me</span>
        <p className="c-about-card__text">{text}</p>
      </div>
    );
  }
}

AboutCard.propTypes = {
  text: PropTypes.string
};

export default AboutCard;
