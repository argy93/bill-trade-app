import React, { Fragment } from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import SettingsIcon from '../Icons/SettingsIcon';
import HamburgerIcon from '../Icons/HamburgerIcon';
import WalletInfo from '../WalletInfo';
import UserInfo from '../UserInfo';
import LangSelect from '../LangSelect';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      error,
      errorMessage,
      loading,
      photo,
      name,
      balanceBtc,
      balanceUsd,
      toggleSidebar
    } = this.props;

    return (
      <div className="c-header">
        {window.innerWidth < 768 ? (
          <button type="button" className="c-header__mobile-menu" onClick={toggleSidebar}>
            <HamburgerIcon />
          </button>
        ) : (
          <Fragment>
            <div className="c-header__user is-right-separator">
              <UserInfo
                photo={photo}
              />
              <p className="c-header__user-name">{name}</p>
            </div>
            <WalletInfo
              title={'Balance:'}
              usd={balanceUsd}
              btc={balanceBtc}
              type={'short'}
            />
            <div className="c-header__settings is-right-separator">
              <SettingsIcon />
            </div>
            <LangSelect
              position={'bottom'}
            />
          </Fragment>
        )}
      </div>
    );
  }
}

Header.propTypes = {
  error: PropTypes.bool,
  errorMessage: PropTypes.any,
  loading: PropTypes.bool,
  photo: PropTypes.string,
  name: PropTypes.string,
  balanceBtc: PropTypes.number,
  balanceUsd: PropTypes.number,
  toggleSidebar: PropTypes.func
};

export default Header;
