import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  profileRequest,
  toggleSidebar
} from '../App/actions';
import {
  makeSelectPhoto,
  makeSelectBalanceBtc,
  makeSelectBalanceUsd,
  makeSelectName,
  makeSelectProfileError,
  makeSelectProfileErrorMessage,
  makeSelectProfileLoading,
} from '../App/selectors';
import Header from '../../components/Header';
import injectSaga from '../../utils/injectSaga';
import saga from './saga';

const mapDispatchToProps = (dispatch) => ({
  getProfileData: () => dispatch(profileRequest()),
  toggleSidebar: () => dispatch(toggleSidebar())
});

const mapStateToProps = createStructuredSelector({
  error: makeSelectProfileError(),
  errorMessage: makeSelectProfileErrorMessage(),
  loading: makeSelectProfileLoading(),
  photo: makeSelectPhoto(),
  name: makeSelectName(),
  balanceBtc: makeSelectBalanceBtc(),
  balanceUsd: makeSelectBalanceUsd(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({
  key: 'profile',
  saga,
});

export default compose(withSaga, withConnect)(Header);
export { mapDispatchToProps };
