import {
  takeLatest,
} from 'redux-saga/effects';
import { PROFILE_REQUEST } from '../App/constants';
import { getProfileData } from '../App/saga';

export default function* headerSaga() {
  yield takeLatest(PROFILE_REQUEST, getProfileData);
}
