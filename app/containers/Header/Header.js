import React, { Fragment } from 'react';
import './style.scss';
import PropTypes from 'prop-types';

export default class Header extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    const { getProfileData } = this.props;

    getProfileData();
  }

  render() {
    const {
      error,
      errorMessage,
      loading,
      photo,
      name,
      balanceBtc,
      balanceUsd
    } = this.props;

    return (
      <Fragment>
        <Header
          error={error}
          errorMessage={errorMessage}
          loading={loading}
          photo={photo}
          name={name}
          balanceBtc={balanceBtc}
          balanceUsd={balanceUsd}
        />
      </Fragment>
    );
  }
}

Header.propTypes = {
  error: PropTypes.bool,
  errorMessage: PropTypes.any,
  loading: PropTypes.bool,
  photo: PropTypes.string,
  name: PropTypes.string,
  balanceBtc: PropTypes.number,
  balanceUsd: PropTypes.number,
  getProfileData: PropTypes.func
};
