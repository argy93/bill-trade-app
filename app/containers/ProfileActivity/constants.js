/*
 * ProfileActivity constants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SET_FULL = 'bt/ProfileActivity/SET_FULL';
export const SET_NORMAL = 'bt/ProfileActivity/SET_NORMAL';
export const ANIMATE_TO_FULL = 'bt/ProfileActivity/ANIMATE_TO_FULL';
export const ANIMATE_TO_NORMAL = 'bt/ProfileActivity/ANIMATE_TO_NORMAL';
export const CHANGE_PERIOD = 'bt/ProfileActivity/CHANGE_PERIOD';
