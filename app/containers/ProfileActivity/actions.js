/*
 * Profile Activity actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  SET_FULL,
  SET_NORMAL,
  ANIMATE_TO_FULL,
  ANIMATE_TO_NORMAL,
  CHANGE_PERIOD,
} from './constants';

export function setFull() {
  return {
    type: SET_FULL,
  };
}

export function setNormal() {
  return {
    type: SET_NORMAL,
  };
}

export function animateToFull() {
  return {
    type: ANIMATE_TO_FULL,
  };
}

export function animateToNormal() {
  return {
    type: ANIMATE_TO_NORMAL,
  };
}

export function changePeriod(from, to) {
  return {
    type: CHANGE_PERIOD,
    from,
    to
  };
}
