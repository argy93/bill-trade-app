import {
  delay, put,
  takeLatest,
} from 'redux-saga/effects';
import { ANIMATE_TO_NORMAL, ANIMATE_TO_FULL } from './constants';
import { setNormal, setFull } from './actions';

function* animateToFull() {
  yield delay(1800);
  yield put(setFull());
}

function* animateToNormal() {
  yield delay(1800);
  yield put(setNormal());
}

export default function* profileActivitySaga() {
  yield takeLatest(ANIMATE_TO_FULL, animateToFull);
  yield takeLatest(ANIMATE_TO_NORMAL, animateToNormal);
}
