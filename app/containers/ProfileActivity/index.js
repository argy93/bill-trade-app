import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import ProfileActivity from './ProfileActivity';
import {
  animateToFull,
  animateToNormal,
  changePeriod,
} from './actions';
import {
  makeSelectPeriod,
  makeSelectTransactions,
  makeSelectFull,
  makeSelectPercentTransactions,
  makeSelectAnimate,
} from './selectors';

const mapDispatchToProps = (dispatch) => ({
  animateToFull: () => dispatch(animateToFull()),
  animateToNormal: () => dispatch(animateToNormal()),
  changePeriod: (from, to) => dispatch(changePeriod(from, to)),
});
const mapStateToProps = createStructuredSelector({
  period: makeSelectPeriod(),
  full: makeSelectFull(),
  transactions: makeSelectTransactions(),
  percentTransactions: makeSelectPercentTransactions(),
  animate: makeSelectAnimate(),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({
  key: 'profileActivity',
  reducer,
});
const withSaga = injectSaga({
  key: 'profileActivity',
  saga,
});

export default compose(withReducer, withSaga, withConnect)(ProfileActivity);
export { mapDispatchToProps };
