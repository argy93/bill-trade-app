import { createSelector } from 'reselect';
import { selectGlobal } from '../App/selectors';

const selectProfileActivity = (state) => state.profileActivity;

const makeSelectProfile = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('profile'),
);

const makeSelectTransactions = () => createSelector(
  makeSelectProfile(),
  (profileState) => profileState.get('transactions')
    .toJS(),
);

const makeSelectPercentTransactions = () => createSelector(
  makeSelectProfile(),
  (profileState) => profileState.get('percentTransactions')
    .toJS(),
);

const makeSelectFull = () => createSelector(
  selectProfileActivity,
  (profileActivityState) => profileActivityState.get('full'),
);

const makeSelectAnimate = () => createSelector(
  selectProfileActivity,
  (profileActivityState) => profileActivityState.get('animate'),
);

const makeSelectPeriod = () => createSelector(
  selectProfileActivity,
  (profileActivityState) => profileActivityState.get('period').toJS(),
);

export {
  makeSelectTransactions,
  makeSelectPercentTransactions,
  makeSelectFull,
  makeSelectAnimate,
  makeSelectPeriod
};
