/*
 * Profile Activity
 */
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { TimelineMax, TweenMax } from 'gsap';
import Activity from '../../components/Activity';

export default class ProfileActivity extends PureComponent {
  constructor(props) {
    super(props);

    this.fullMain = React.createRef();
    this.fullWrapper = React.createRef();
    this.normalWrapper = React.createRef();
    this.normalMain = React.createRef();
  }

  componentDidUpdate() {
    const { animate, full } = this.props;
    const blockGap = window.innerWidth < 1440 ? 15 : 30;

    if (animate && !full) {
      this.animateToFull();
    }

    if (animate && full) {
      this.animateToNormal(blockGap);
    }
  }

  animateToFull() {
    const { type = 'normal' } = this.props;

    if (type === 'full' && this.fullMain.current) {
      const fullMarginBottom = window.getComputedStyle(this.fullMain.current).marginBottom;
      const fullHeight = this.fullMain.current.clientHeight + 2;

      TweenMax.set(this.fullWrapper.current, {
        height: 0,
        marginBottom: 0,
        overflow: 'hidden',
      });

      TweenMax.set(this.fullMain.current, {
        opacity: 0,
      });

      // eslint-disable-next-line no-unused-vars
      const timeline = new TimelineMax()
        .delay(0.6)
        .add([
          // Выставляем большую карточку в исходную позицию
          // Увеличенная и опущенная на 200px
          TweenMax.set(this.fullMain.current, {
            scale: 1.3,
            y: 200,
            marginBottom: 0,
          }),
        ])
        .delay(0.6)
        // Паралельно -
        // 2. Перемещаем большую карточку чуть выше исходной позиции
        // 3. Увеличиваем враппер большой карточки до исходной высоты и маржина
        .add([
          TweenMax.to(this.fullWrapper.current, 0.6, {
            height: fullHeight,
            marginBottom: fullMarginBottom,
          }),
          TweenMax.to(this.fullMain.current, 0.6, {
            opacity: 1,
            scale: 1,
            y: -40,
          }, '+=1'),
        ])
        // Двигаем большую карточку в исходную позицию
        .to(this.fullMain.current, 0.6, {
          y: 0,
        })
        .set(this.fullWrapper.current, {
          height: 'auto',
          marginBottom: 0,
        })
        .set(this.fullMain.current, {
          marginBottom: fullMarginBottom
        });
    } else if (type === 'normal' && this.normalMain.current) {
      TweenMax.set(this.normalWrapper.current, {
        height: this.normalMain.current.clientHeight + 2,
        marginBottom: window.getComputedStyle(this.normalMain.current).marginBottom,
      });

      TweenMax.set(this.normalMain.current, {
        marginBottom: 0,
      });

      // eslint-disable-next-line no-unused-vars
      const timeline = new TimelineMax()
      // Маленькая карточка поднимается и угасает
        .to(this.normalMain.current, 0.6, {
          y: -50,
          opacity: 0,
          scale: 1.1,
        })
        .add([
          TweenMax.set(this.normalWrapper.current, {
            overflow: 'hidden',
          }),
        ])
        // Сжимаем враппер нормальной карточки
        .to(this.normalWrapper.current, 0.6, {
          height: 0,
          marginBottom: 0,
        });
    }
  }

  animateToNormal() {
    const { type = 'normal' } = this.props;

    if (type === 'full' && this.fullMain.current) {
      TweenMax.set(this.fullWrapper.current, {
        marginBottom: window.getComputedStyle(this.fullMain.current).marginBottom,
      });

      TweenMax.set(this.fullMain.current, {
        marginBottom: 0,
      });

      // eslint-disable-next-line no-unused-vars
      const timeline = new TimelineMax()
      // Приподнимаем большую карточку
        .to(this.fullMain.current, 0.6, {
          y: -40,
        })
        // Паралельно
        // 1. Большую карточку резко вниз и скрываем
        // 2. Враппер большой карточки уменьшаем до ноля
        .add([
          TweenMax.to(this.fullMain.current, 0.6, {
            opacity: 0,
            scale: 1.3,
            y: 200,
          }),
          TweenMax.to(this.fullWrapper.current, 0.6, {
            height: 0,
            marginBottom: 0,
          }),
        ]);
    } else if (type === 'normal' && this.normalMain.current) {
      TweenMax.set(this.normalWrapper.current, {
        width: '100%',
        height: 'auto',
      });

      TweenMax.set(this.normalMain.current, {
        width: '100%',
      });

      const normalMarginBottom = window.getComputedStyle(this.normalMain.current).marginBottom;
      const normalHeight = this.normalMain.current.clientHeight + 2;

      TweenMax.set(this.normalWrapper.current, {
        // width: 0,
        height: 0,
        marginBottom: 0,
      });

      TweenMax.set(this.normalMain.current, {
        opacity: 0,
        marginBottom: 0,
      });

      // eslint-disable-next-line no-unused-vars
      const timeline = new TimelineMax()
      // Ожидаем подъема большой карточки
        .delay(0.6)
        // Враппер маленькой карточки увеличиваем
        .to(this.normalWrapper.current, 0.6, {
          height: normalHeight,
          marginBottom: normalMarginBottom,
        })
        // Приподнимаем и увеличиваем маленькую карточку
        .set(this.normalMain.current, {
          y: -50,
          scale: 1.1,
        })
        // Ставим маленькую карточку на исходную позицию
        .to(this.normalMain.current, 0.6, {
          y: 0,
          scale: 1,
          opacity: 1,
        })
        .set(this.normalMain.current, {
          marginBottom: normalMarginBottom,
        })
        .set(this.normalWrapper.current, {
          marginBottom: 0,
          height: 'auto',
        });
    }
  }

  render() {
    const { type = 'normal', full, animate } = this.props;
    const isFull = type === 'full';
    const wrapper = isFull ? this.fullWrapper : this.normalWrapper;
    const main = isFull ? this.fullMain : this.normalMain;

    return (
      <Fragment>
        {isFull === full || animate ? (
          <div ref={wrapper} className="l-profile__activity-wrap">
            <div className="l-profile__content-card" ref={main}>
              <Activity
                {...this.props}
                resizeIcon
              />
            </div>
          </div>
        ) : null}
      </Fragment>
    );
  }
}

ProfileActivity.propTypes = {
  type: PropTypes.oneOf(['normal', 'full']),
  full: PropTypes.bool,
  animate: PropTypes.bool,
};
