import { fromJS, Map } from 'immutable';
import {
  ANIMATE_TO_FULL, ANIMATE_TO_NORMAL, SET_FULL, SET_NORMAL, CHANGE_PERIOD,
} from './constants';

// The initial state of the ProfileActivity
const initialState = fromJS({
  full: false,
  animate: false,
  period: {
    from: 0,
    to: new Date().getTime() * 2,
  },
});

function profileActivityReducer(state = initialState, action) {
  switch (action.type) {
    case ANIMATE_TO_FULL:
      return state.set('animate', true);
    case ANIMATE_TO_NORMAL:
      return state.set('animate', true);
    case SET_NORMAL:
      return state.set('full', false)
        .set('animate', false);
    case SET_FULL:
      return state.set('full', true)
        .set('animate', false);
    case CHANGE_PERIOD:
      return state.set('period', Map({
        from: action.from,
        to: action.to
      }));
    default:
      return state;
  }
}

export default profileActivityReducer;
