/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_ACTIVITY_TYPE = 'bt/Profile/CHANGE_ACTIVITY_TYPE';
export const SET_ACTIVITY_FULL = 'bt/Profile/SET_ACTIVITY_FULL';
export const ANIMATE_ACTIVITY_TO_FULL = 'bt/Profile/ANIMATE_ACTIVITY_TO_FULL';
export const SET_ACTIVITY_NORMAL = 'bt/Profile/SET_ACTIVITY_NORMAL';
export const ANIMATE_ACTIVITY_TO_NORMAL = 'bt/Profile/ANIMATE_ACTIVITY_TO_NORMAL';
export const TOGGLE_SELECT_PLATFORM = 'bt/Profile/TOGGLE_SELECT_PLATFORM';
