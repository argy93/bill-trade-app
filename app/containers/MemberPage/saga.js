import {
  takeLatest,
  delay,
  put,
} from 'redux-saga/effects';
import { PROFILE_REQUEST } from '../App/constants';
import { getProfileData } from '../App/saga';
import {
  ANIMATE_ACTIVITY_TO_FULL,
  ANIMATE_ACTIVITY_TO_NORMAL,
} from './constants';
import { setActivityFull, setActivityNormal } from './actions';

function* animateActivityToFull() {
  yield delay(1800);
  yield put(setActivityFull());
}

function* animateActivityToNormal() {
  yield delay(1800);
  yield put(setActivityNormal());
}

export default function* profileSaga() {
  yield takeLatest(PROFILE_REQUEST, getProfileData);
  yield takeLatest(ANIMATE_ACTIVITY_TO_FULL, animateActivityToFull);
  yield takeLatest(ANIMATE_ACTIVITY_TO_NORMAL, animateActivityToNormal);
}
