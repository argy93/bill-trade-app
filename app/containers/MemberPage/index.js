import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import MemberPage from './MemberPage';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import { profileRequest } from '../App/actions';

import {
  makeSelectPhoto,
  makeSelectAbout,
  makeSelectAverageTime,
  makeSelectBalanceBtc,
  makeSelectBalanceUsd,
  makeSelectFollowers,
  makeSelectFollowing,
  makeSelectMaxLoss,
  makeSelectName,
  makeSelectProfitBtc,
  makeSelectProfitUsd,
  makeSelectRank,
  makeSelectTagsMain,
  makeSelectTagsSecond,
  makeSelectTrades,
  makeSelectTradingType,
  makeSelectWinRate,
  makeSelectProfileError,
  makeSelectProfileErrorMessage,
  makeSelectProfileLoading,
  makeSelectStrides,
} from '../App/selectors';
import {
  makeSelectActivityType,
  makeSelectActivityFull,
  makeSelectActivityPeriod,
  makeSelectActivityAnimateToNormal,
  makeSelectActivityAnimateToFull,
  makeSelectActivityTransactions,
  makeSelectPortfolioItems,
} from './selectors';
import {
  animateActivityToFull,
  animateActivityToNormal,
  changeActivityType,
  toggleSelectPlatform
} from './actions';

const mapDispatchToProps = (dispatch) => ({
  getProfileData: () => dispatch(profileRequest()),
  // activityChangeType: (type) => dispatch(changeActivityType(type)),
  // activitySetFull: () => dispatch(animateActivityToFull()),
  // activitySetNormal: () => dispatch(animateActivityToNormal()),
  // toggleSelectPlatform: (platformName) => dispatch(toggleSelectPlatform(platformName))
});

const withReducer = injectReducer({
  key: 'members',
  reducer,
});

const mapStateToProps = createStructuredSelector({
  photo: makeSelectPhoto(),
  name: makeSelectName(),
  tagsMain: makeSelectTagsMain(),
  tagsSecond: makeSelectTagsSecond(),
  following: makeSelectFollowing(),
  followers: makeSelectFollowers(),
  about: makeSelectAbout(),
  winRate: makeSelectWinRate(),
  // profitUsd: makeSelectProfitUsd(),
  // maxLoss: makeSelectMaxLoss(),
  // averageTime: makeSelectAverageTime(),
  // trades: makeSelectTrades(),
  // tradingType: makeSelectTradingType(),
  // portfolio: makeSelectPortfolioItems(),
  // balanceBtc: makeSelectBalanceBtc(),
  // balanceUsd: makeSelectBalanceUsd(),
  // profitBtc: makeSelectProfitBtc(),
  rank: makeSelectRank(),
  // error: makeSelectProfileError(),
  // errorMessage: makeSelectProfileErrorMessage(),
  // loading: makeSelectProfileLoading(),
  // activityType: makeSelectActivityType(),
  // activityFull: makeSelectActivityFull(),
  // activityPeriod: makeSelectActivityPeriod(),
  // activityAnimateToFull: makeSelectActivityAnimateToFull(),
  // activityAnimateToNormal: makeSelectActivityAnimateToNormal(),
  // transactions: makeSelectActivityTransactions(),
  // strides: makeSelectStrides(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({
  key: 'members',
  saga,
});

export default compose(withReducer, withSaga, withConnect)(MemberPage);
export { mapDispatchToProps };
