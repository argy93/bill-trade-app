/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  ANIMATE_ACTIVITY_TO_FULL,
  ANIMATE_ACTIVITY_TO_NORMAL,
  CHANGE_ACTIVITY_TYPE,
  SET_ACTIVITY_FULL,
  SET_ACTIVITY_NORMAL,
  TOGGLE_SELECT_PLATFORM
} from './constants';

export function changeActivityType(type) {
  return {
    type: CHANGE_ACTIVITY_TYPE,
    activityType: type,
  };
}

export function animateActivityToFull() {
  return {
    type: ANIMATE_ACTIVITY_TO_FULL,
  };
}

export function animateActivityToNormal() {
  return {
    type: ANIMATE_ACTIVITY_TO_NORMAL,
  };
}

export function setActivityFull() {
  return {
    type: SET_ACTIVITY_FULL,
  };
}

export function setActivityNormal() {
  return {
    type: SET_ACTIVITY_NORMAL,
  };
}

export function toggleSelectPlatform(platformName) {
  return {
    type: TOGGLE_SELECT_PLATFORM,
    platformName,
  };
}
