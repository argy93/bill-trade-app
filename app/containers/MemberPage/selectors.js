import { createSelector } from 'reselect';
import { makeSelectPercentTransactions, makeSelectPortfolio, makeSelectTransactions } from '../App/selectors';

const selectProfile = (state) => state.profile;

const makeSelectActivityType = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('activityType'),
);

const makeSelectActivityFull = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('activityFull'),
);

const makeSelectActivityPeriod = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('activityPeriod')
    .toJS(),
);

const makeSelectActivityAnimateToFull = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('animateToFull'),
);

const makeSelectActivityAnimateToNormal = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('animateToNormal'),
);

const makeSelectPlatforms = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('platforms')
    .toJS(),
);

const makeSelectActivityTransactions = () => createSelector(
  makeSelectTransactions(),
  makeSelectPercentTransactions(),
  makeSelectActivityType(),
  makeSelectActivityPeriod(),
  (transactions, percentTransactions, type, period) => {
    for (let i = 0; i < transactions.length; i += 1) {
      transactions[i].points = transactions[i].points.filter((point) => point.timestamp > period.from && point.timestamp < period.to);
    }
    for (let i = 0; i < percentTransactions.length; i += 1) {
      percentTransactions[i].points = percentTransactions[i].points.filter((point) => point.timestamp > period.from && point.timestamp < period.to);
    }

    return [
      {
        filterType: 'c',
        chartData: transactions,
      },
      {
        filterType: 'p',
        chartData: percentTransactions,
      },
    ];
  },
);

const makeSelectPortfolioItems = () => createSelector(
  makeSelectPortfolio(),
  makeSelectPlatforms(),
  (portfolio, platforms) => portfolio.map((pItem) => ({
    platformName: pItem.name,
    selected: !!platforms[pItem.name],
    stats: pItem.stats.map((statsItem) => ({
      currencyName: statsItem.name,
      value: statsItem.value,
    })),
  })),
);

export {
  makeSelectActivityType,
  makeSelectActivityFull,
  makeSelectActivityPeriod,
  makeSelectActivityAnimateToFull,
  makeSelectActivityAnimateToNormal,
  makeSelectActivityTransactions,
  makeSelectPortfolioItems
};
