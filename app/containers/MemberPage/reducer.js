import * as immutable from 'immutable';
import {
  ANIMATE_ACTIVITY_TO_FULL,
  ANIMATE_ACTIVITY_TO_NORMAL,
  CHANGE_ACTIVITY_TYPE,
  SET_ACTIVITY_FULL,
  SET_ACTIVITY_NORMAL,
  TOGGLE_SELECT_PLATFORM
} from './constants';

const initialState = immutable.fromJS({
  activityType: 'c',
  activityFull: false,
  animateToFull: false,
  animateToNormal: false,
  activityPeriod: {
    from: 0,
    to: new Date().getTime() * 2,
  },
  platforms: {}
});

function membersReducer(state = initialState, action) {
  switch (action.type) {
    case ANIMATE_ACTIVITY_TO_FULL:
      return state.set('animateToFull', true);
    case ANIMATE_ACTIVITY_TO_NORMAL:
      return state.set('animateToNormal', true);
    case SET_ACTIVITY_NORMAL:
      return state.set('activityFull', false)
        .set('animateToNormal', false);
    case SET_ACTIVITY_FULL:
      return state.set('activityFull', true)
        .set('animateToFull', false);
    case CHANGE_ACTIVITY_TYPE:
      if (action.activityType !== 'c' && action.activityType !== 'p') return state;
      return state.set('activityType', action.activityType);
    case TOGGLE_SELECT_PLATFORM:
      return state.setIn(['platforms', action.platformName], !state.getIn(['platforms', action.platformName]));
    default:
      return state;
  }
}

export default membersReducer;
