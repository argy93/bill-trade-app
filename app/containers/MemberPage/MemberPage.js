import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import './style.scss';

import DownArrowIcon from '../../components/Icons/DownArrowIcon';
import CheckIcon from '../../components/Icons/CheckIcon';
import InfoIcon from '../../components/Icons/InfoIcon';
import ProfileCard from '../../components/ProfileCard';
import AboutCard from '../../components/AboutCard';
import UserActivityData from '../../components/UserActivityData';
import Activity from '../../components/Activity';
import CardShort from '../../components/CardShort';
import StatsList from '../../components/StatsList';
import ChartPolygon from '../../components/ChartPolygon';
import Portfolio from '../../components/Portfolio';
import ProgressCircle from '../../components/ProgressCircle';
import { formattingNumbersByThousands } from '../../utils/helpers';

export default class MemberPage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.strides = [
      { name: 'Profit factor', value: 90 },
      { name: 'Investors deposits', value: 99 },
      { name: 'Profit/Loss ratio', value: 44 },
      { name: 'Maximum declared loss', value: 50 },
      { name: 'Daily maximum loss', value: 50 },
      { name: 'Winrate', value: 90 },
      { name: 'Percent of average slip', value: 100 },
      { name: 'Platform trading time', value: 10 }
    ];

    this.portfolio = [
      {
        platformName: 'Binance',
        selected: false,
        stats: [
          { currencyName: 'Eth', value: 14.641 },
          { currencyName: 'Erp', value: 8.854 }
        ]
      },
      {
        platformName: 'Bitmax',
        selected: false,
        stats: [
          { currencyName: 'Erp', value: 2.171 }
        ]
      },
      {
        platformName: 'Bitfinex',
        selected: false,
        stats: [
          { currencyName: 'Erp', value: 16.199 },
          { currencyName: 'Eth', value: 10.016 },
          { currencyName: 'Btc', value: 6.108 }
        ]
      },
      {
        platformName: 'Bibox',
        selected: false,
        stats: [
          { currencyName: 'Erp', value: 26.643 }
        ]
      },
      {
        platformName: 'Bitmex',
        selected: false,
        stats: [
          { currencyName: 'Erp', value: 23.409 }
        ]
      },
      {
        platformName: 'Huobi',
        selected: false,
        stats: [
          { currencyName: 'Eth', value: 26.916 },
          { currencyName: 'Erp', value: 12.106 },
          { currencyName: 'Btc', value: 18.082 }
        ]
      },
      {
        platformName: 'OKEx',
        selected: false,
        stats: [
          { currencyName: 'Eth', value: 4.672 },
          { currencyName: 'Erp', value: 17.993 },
          { currencyName: 'Btc', value: 6.214 }
        ]
      },
      {
        platformName: 'Gate',
        selected: false,
        stats: [
          { currencyName: 'Erp', value: 22.361 },
          { currencyName: 'Eth', value: 22.258 },
          { currencyName: 'Btc', value: 13.13 }
        ]
      },
      {
        platformName: 'Poloniex',
        selected: false,
        stats: [
          { currencyName: 'Btc', value: 4.387 },
          { currencyName: 'Erp', value: 11.36 }
        ]
      }
    ];

    this.activityData = JSON.parse('{"type":"normal","period":{"from":0,"to":3141057870282},"full":false,"transactions":[{"name":"Eth","points":[{"timestamp":1404939600000,"value":-4.343},{"timestamp":1405112400000,"value":10.979},{"timestamp":1405285200000,"value":25.63},{"timestamp":1405458000000,"value":32.308},{"timestamp":1405630800000,"value":-1.781},{"timestamp":1405803600000,"value":5.437},{"timestamp":1405976400000,"value":-8.86},{"timestamp":1406149200000,"value":42.529},{"timestamp":1406322000000,"value":36.104},{"timestamp":1406494800000,"value":20.146},{"timestamp":1406667600000,"value":34.377},{"timestamp":1406840400000,"value":22.492},{"timestamp":1407013200000,"value":40.403},{"timestamp":1407186000000,"value":9.654},{"timestamp":1407358800000,"value":-6.79},{"timestamp":1407531600000,"value":10.29}]},{"name":"Usd","points":[{"timestamp":1404939600000,"value":14.596},{"timestamp":1405112400000,"value":-2.254},{"timestamp":1405285200000,"value":-5.144},{"timestamp":1405458000000,"value":37.094},{"timestamp":1405630800000,"value":15.144},{"timestamp":1405803600000,"value":43.355},{"timestamp":1405976400000,"value":6.734},{"timestamp":1406149200000,"value":16.237},{"timestamp":1406322000000,"value":41.328},{"timestamp":1406494800000,"value":4.776},{"timestamp":1406667600000,"value":44.452},{"timestamp":1406840400000,"value":36.067},{"timestamp":1407013200000,"value":16.86},{"timestamp":1407186000000,"value":17.676},{"timestamp":1407358800000,"value":33.801},{"timestamp":1407531600000,"value":-8.562}]},{"name":"Btc","points":[{"timestamp":1404939600000,"value":23.566},{"timestamp":1405112400000,"value":44.947},{"timestamp":1405285200000,"value":29.257},{"timestamp":1405458000000,"value":0.248},{"timestamp":1405630800000,"value":41.798},{"timestamp":1405803600000,"value":29.38},{"timestamp":1405976400000,"value":-7.21},{"timestamp":1406149200000,"value":13.903},{"timestamp":1406322000000,"value":8.231},{"timestamp":1406494800000,"value":10.621},{"timestamp":1406667600000,"value":-6.955},{"timestamp":1406840400000,"value":35.156},{"timestamp":1407013200000,"value":14.9},{"timestamp":1407186000000,"value":31.813},{"timestamp":1407358800000,"value":13.719},{"timestamp":1407531600000,"value":10.231}]}],"percentTransactions":[{"name":"Eth","points":[{"timestamp":1404939600000,"value":2.361},{"timestamp":1405112400000,"value":19.379},{"timestamp":1405285200000,"value":-0.492},{"timestamp":1405458000000,"value":16.628},{"timestamp":1405630800000,"value":28.032},{"timestamp":1405803600000,"value":8.304},{"timestamp":1405976400000,"value":26.729},{"timestamp":1406149200000,"value":6.141},{"timestamp":1406322000000,"value":44.527},{"timestamp":1406494800000,"value":8.89},{"timestamp":1406667600000,"value":-7.243},{"timestamp":1406840400000,"value":10.051},{"timestamp":1407013200000,"value":27.409},{"timestamp":1407186000000,"value":-6.791},{"timestamp":1407358800000,"value":24.36},{"timestamp":1407531600000,"value":12.689},{"timestamp":1407704400000,"value":30.784},{"timestamp":1407877200000,"value":6.668},{"timestamp":1408050000000,"value":28.546},{"timestamp":1408222800000,"value":6.209},{"timestamp":1408395600000,"value":35.205},{"timestamp":1408568400000,"value":21.086},{"timestamp":1408741200000,"value":21.562},{"timestamp":1408914000000,"value":-5.206},{"timestamp":1409086800000,"value":-1.517},{"timestamp":1409259600000,"value":-7.7},{"timestamp":1409432400000,"value":28.875},{"timestamp":1409605200000,"value":6.579},{"timestamp":1409778000000,"value":1.67},{"timestamp":1409950800000,"value":28.508},{"timestamp":1410123600000,"value":31.293},{"timestamp":1410296400000,"value":26.014},{"timestamp":1410469200000,"value":15.816},{"timestamp":1410642000000,"value":34.315},{"timestamp":1410814800000,"value":34.872},{"timestamp":1410987600000,"value":14.458},{"timestamp":1411160400000,"value":6.603},{"timestamp":1411333200000,"value":3.155},{"timestamp":1411506000000,"value":24.467},{"timestamp":1411678800000,"value":39.485},{"timestamp":1411851600000,"value":42.294},{"timestamp":1412024400000,"value":9.194},{"timestamp":1412197200000,"value":27.737},{"timestamp":1412370000000,"value":-2.823},{"timestamp":1412542800000,"value":35.084},{"timestamp":1412715600000,"value":41.225},{"timestamp":1412888400000,"value":-2.985},{"timestamp":1413061200000,"value":-2.318},{"timestamp":1413234000000,"value":38.181},{"timestamp":1413406800000,"value":-9.071},{"timestamp":1413579600000,"value":-2.404},{"timestamp":1413752400000,"value":-7.594},{"timestamp":1413925200000,"value":33.013},{"timestamp":1414098000000,"value":-6.322},{"timestamp":1414270800000,"value":6.28},{"timestamp":1414447200000,"value":-2.611},{"timestamp":1414620000000,"value":16.686},{"timestamp":1414792800000,"value":31.415},{"timestamp":1414965600000,"value":27.337},{"timestamp":1415138400000,"value":17.847}]},{"name":"Usd","points":[{"timestamp":1404939600000,"value":18.992},{"timestamp":1405112400000,"value":16.047},{"timestamp":1405285200000,"value":1.989},{"timestamp":1405458000000,"value":18.174},{"timestamp":1405630800000,"value":9.403},{"timestamp":1405803600000,"value":34.683},{"timestamp":1405976400000,"value":-3.816},{"timestamp":1406149200000,"value":21.493},{"timestamp":1406322000000,"value":9.058},{"timestamp":1406494800000,"value":18.216},{"timestamp":1406667600000,"value":25.39},{"timestamp":1406840400000,"value":1.195},{"timestamp":1407013200000,"value":4.301},{"timestamp":1407186000000,"value":-9.912},{"timestamp":1407358800000,"value":18.917},{"timestamp":1407531600000,"value":35.738},{"timestamp":1407704400000,"value":16.319},{"timestamp":1407877200000,"value":6.491},{"timestamp":1408050000000,"value":39.047},{"timestamp":1408222800000,"value":25.449},{"timestamp":1408395600000,"value":32.09},{"timestamp":1408568400000,"value":34.862},{"timestamp":1408741200000,"value":8.239},{"timestamp":1408914000000,"value":32.914},{"timestamp":1409086800000,"value":9.286},{"timestamp":1409259600000,"value":12.2},{"timestamp":1409432400000,"value":11.702},{"timestamp":1409605200000,"value":15.725},{"timestamp":1409778000000,"value":-8.226},{"timestamp":1409950800000,"value":43.482},{"timestamp":1410123600000,"value":29.319},{"timestamp":1410296400000,"value":28.959},{"timestamp":1410469200000,"value":44.322},{"timestamp":1410642000000,"value":38.068},{"timestamp":1410814800000,"value":28.095},{"timestamp":1410987600000,"value":30.595},{"timestamp":1411160400000,"value":26.286},{"timestamp":1411333200000,"value":-3.213},{"timestamp":1411506000000,"value":6.674},{"timestamp":1411678800000,"value":21.168},{"timestamp":1411851600000,"value":13.299},{"timestamp":1412024400000,"value":28.611},{"timestamp":1412197200000,"value":12.074},{"timestamp":1412370000000,"value":-9.392},{"timestamp":1412542800000,"value":41.461},{"timestamp":1412715600000,"value":16.567},{"timestamp":1412888400000,"value":18.914},{"timestamp":1413061200000,"value":26.444},{"timestamp":1413234000000,"value":26.967},{"timestamp":1413406800000,"value":6.837},{"timestamp":1413579600000,"value":26.596},{"timestamp":1413752400000,"value":26.247},{"timestamp":1413925200000,"value":28.274},{"timestamp":1414098000000,"value":38.624},{"timestamp":1414270800000,"value":24.96},{"timestamp":1414447200000,"value":39.282},{"timestamp":1414620000000,"value":39.608},{"timestamp":1414792800000,"value":7.553},{"timestamp":1414965600000,"value":2.109},{"timestamp":1415138400000,"value":-2.468}]},{"name":"Btc","points":[{"timestamp":1404939600000,"value":-4.846},{"timestamp":1405112400000,"value":16.952},{"timestamp":1405285200000,"value":13.86},{"timestamp":1405458000000,"value":-3.711},{"timestamp":1405630800000,"value":2.702},{"timestamp":1405803600000,"value":-8.785},{"timestamp":1405976400000,"value":15.01},{"timestamp":1406149200000,"value":16.311},{"timestamp":1406322000000,"value":6.799},{"timestamp":1406494800000,"value":16.984},{"timestamp":1406667600000,"value":25.813},{"timestamp":1406840400000,"value":44.868},{"timestamp":1407013200000,"value":28.689},{"timestamp":1407186000000,"value":22.879},{"timestamp":1407358800000,"value":1.616},{"timestamp":1407531600000,"value":39.97},{"timestamp":1407704400000,"value":26.409},{"timestamp":1407877200000,"value":26.163},{"timestamp":1408050000000,"value":15.789},{"timestamp":1408222800000,"value":38.455},{"timestamp":1408395600000,"value":0.211},{"timestamp":1408568400000,"value":-1.986},{"timestamp":1408741200000,"value":39.132},{"timestamp":1408914000000,"value":22.857},{"timestamp":1409086800000,"value":-8.52},{"timestamp":1409259600000,"value":11.786},{"timestamp":1409432400000,"value":-1.233},{"timestamp":1409605200000,"value":2.235},{"timestamp":1409778000000,"value":27.915},{"timestamp":1409950800000,"value":44.662},{"timestamp":1410123600000,"value":25.335},{"timestamp":1410296400000,"value":41.29},{"timestamp":1410469200000,"value":37.528},{"timestamp":1410642000000,"value":15.28},{"timestamp":1410814800000,"value":6.771},{"timestamp":1410987600000,"value":1.413},{"timestamp":1411160400000,"value":37.576},{"timestamp":1411333200000,"value":6.504},{"timestamp":1411506000000,"value":40.801},{"timestamp":1411678800000,"value":30.468},{"timestamp":1411851600000,"value":16.15},{"timestamp":1412024400000,"value":12.828},{"timestamp":1412197200000,"value":23.54},{"timestamp":1412370000000,"value":26.503},{"timestamp":1412542800000,"value":20.926},{"timestamp":1412715600000,"value":8.844},{"timestamp":1412888400000,"value":5.623},{"timestamp":1413061200000,"value":2.703},{"timestamp":1413234000000,"value":-2.936},{"timestamp":1413406800000,"value":26.685},{"timestamp":1413579600000,"value":5.718},{"timestamp":1413752400000,"value":16.62},{"timestamp":1413925200000,"value":30.79},{"timestamp":1414098000000,"value":16.561},{"timestamp":1414270800000,"value":35.597},{"timestamp":1414447200000,"value":40.419},{"timestamp":1414620000000,"value":-3.256},{"timestamp":1414792800000,"value":2.194},{"timestamp":1414965600000,"value":28.653},{"timestamp":1415138400000,"value":37.873}]}],"animate":false}');

    this.windowWidth = window.innerWidth;
    this.activityFull = React.createRef();
    this.activityFullWrapper = React.createRef();
    this.activityNormalWrapper = React.createRef();
    this.activityNormal = React.createRef();
  }

  componentDidMount() {
    const { getProfileData } = this.props;

    getProfileData();
  }

  svgSize() {
    const radius = 97;

    return radius;
  }

  render() {
    const {
      photo,
      name,
      tagsMain,
      tagsSecond,
      following,
      followers,
      winRate,
      about,
      rank
    } = this.props;

    return (
      <div className="l-members-page">
        <Helmet>
          <title>Members</title>
          <meta name="description" content="Members page" />
        </Helmet>
        <Fragment>
          <div className="l-members-page__link">
            <Link to={'/top-members'}>
              <DownArrowIcon
                width={'15px'}
                height={'20px'}
              />
              <span>Back to top member list</span>
            </Link>
          </div>
          <div className="l-members-page__content">
            <div className="l-members-page__row">
              <div className="l-members-page__column is-left">
                <div className="l-members-page__content-card">
                  <ProfileCard
                    photo={photo}
                    name={name}
                    tagsMain={tagsMain}
                    tagsSecond={tagsSecond}
                    following={following}
                    followers={followers}
                    winRate={winRate}
                    userType={'member'}
                    currency={'USD'}
                  />
                  <div className="l-members-page__progress">
                    <div className="l-members-page__progress-info">
                      <ProgressCircle
                        containerClass={'c-progress-circle'}
                        size={this.svgSize()}
                        percent={winRate}
                        borderWidth={9}
                        borderColor={'#20AD65'}
                        progressBarContent={() => (
                          <Fragment>
                            <div className="c-progress-circle__percent">{winRate}%</div>
                            <div className="c-progress-circle__sum">
                              {formattingNumbersByThousands(920000)}
                            </div>
                          </Fragment>
                        )}
                      />
                      <ProgressCircle
                        containerClass={'c-stats-rank'}
                        size={this.svgSize()}
                        percent={rank * 10}
                        borderWidth={9}
                        borderColor={'#A7C2F1'}
                        progressBarContent={() => (
                          <Fragment>
                            <div className="c-stats-rank__title">BT rank</div>
                            <div className="c-stats-rank__number">{rank}</div>
                          </Fragment>
                        )}
                      />
                    </div>
                    <UserActivityData
                      following={following}
                      followers={followers}
                      winRate={winRate}
                    />
                  </div>
                </div>
                <div className="l-members-page__row">
                  <div className="l-members-page__column is-left is-half">
                    <div className="l-members-page__content-card">
                      <div className="c-data-card">
                        <span className="c-data-card__title">
                          All time profit
                        </span>
                        <div className="c-data-card__value">
                          <span className="c-data-card__value-main">
                            $9,280,345
                          </span>
                          <span className="c-data-card__value-secondary">
                            48%
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="l-members-page__column is-right is-half">
                    <div className="l-members-page__content-card">
                      <div className="c-data-card">
                        <span className="c-data-card__title">
                          Minimum deposit
                        </span>
                        <div className="c-data-card__value">
                          <span className="c-data-card__value-main">
                            $203
                          </span>
                          <span className="c-data-card__value-secondary">
                            0,2 BTC
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="l-members-page__column is-right">
                <div className="l-members-page__content-card">
                  <AboutCard
                    text={about}
                  />
                </div>
                <div className="l-members-page__content-card">
                  <div className="c-data-card">
                    <span className="c-data-card__title">
                      Failure of RM policy
                      <InfoIcon />
                    </span>
                    <div className="c-data-card__value">
                      <span className="c-data-card__value-main">
                        Not detected
                      </span>
                      <CheckIcon />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="l-members-page__row">
              <div className="l-members-page__content-card full-width">
                <div className="c-data-card">
                  <span className="c-data-card__title">
                    Investors profit
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      $32,280
                    </span>
                  </div>
                </div>
                <div className="c-data-card">
                  <span className="c-data-card__title">
                    Daily profit
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      ~ $115
                    </span>
                  </div>
                </div>
                <div className="c-data-card">
                  <span className="c-data-card__title">
                    Trades
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      203
                    </span>
                  </div>
                </div>
                <div className="c-data-card">
                  <span className="c-data-card__title">
                    Avg. time
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      36h
                    </span>
                  </div>
                </div>
                <div className="c-data-card">
                  <span className="c-data-card__title">
                    Profit/loss
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      3/1
                    </span>
                  </div>
                </div>
                <div className="c-data-card">
                  <span className="c-data-card__title">
                    Max. declared loss
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main is-loss">
                      20%
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="l-members-page__row">
              <div className="l-members-page__content-card full-width">
                <div className="c-data-card is-mid-size">
                  <span className="c-data-card__title">
                    Avg. profit/trade
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      $29 | 0.21%
                    </span>
                  </div>
                </div>
                <div className="c-data-card is-mid-size">
                  <span className="c-data-card__title">
                    Avg. trades/day
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      4.2
                    </span>
                  </div>
                </div>
                <div className="c-data-card is-mid-size">
                  <span className="c-data-card__title">
                    Avg. order size
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      1%
                    </span>
                  </div>
                </div>
                <div className="c-data-card is-mid-size">
                  <span className="c-data-card__title">
                    Trading type
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main">
                      Day trading
                    </span>
                  </div>
                </div>
                <div className="c-data-card is-mid-size">
                  <span className="c-data-card__title">
                    Max. loss
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main is-loss">
                      $10.03
                    </span>
                  </div>
                </div>
                <div className="c-data-card is-mid-size">
                  <span className="c-data-card__title">
                    Max. loss daily
                  </span>
                  <div className="c-data-card__value">
                    <span className="c-data-card__value-main is-loss">
                      3%
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="l-members-page__row">
              <div className="l-members-page__content-card">
                <div className="c-data-card c-trading-card">
                  <span className="c-data-card__title">
                    Type of trading system
                  </span>
                  <div className="c-trading-card__row">
                    <div className="c-trading-card__row-item">Indicators</div>
                    <div className="c-trading-card__row-item">Technical analysis</div>
                    <div className="c-trading-card__row-item">Arbitration</div>
                    <div className="c-trading-card__row-item">Algorithmic trading</div>
                    <div className="c-trading-card__row-item">Artificial intelligence</div>
                    <div className="c-trading-card__row-item">Fundamental analysis</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="l-members-page__row">
              <div className="l-members-page__content-card">
                <Activity
                  {...this.activityData}
                  chartHeight={125}
                  resizeIcon={false}
                />
              </div>
            </div>
            <div className="l-members-page__row">
              <div className="l-members-page__content-card">
                <div className="l-members-page__rank">
                  <div className="l-members-page__rank-data">
                    <span className="l-members-page__rank-label">BT Rank</span>
                    <span className="l-members-page__rank-value">{rank}</span>
                    <CardShort
                      main="User deposit x BT rank = Max. subscription amount, USD"
                      additional="92 300 x 9.9 = 920 000"
                    />
                    <StatsList items={this.strides} />
                  </div>
                  <div className="l-members-page__rank-chart">
                    <div className="c-stats__chart-wrap">
                      <ChartPolygon
                        items={this.strides}
                        max={100}
                        min={0}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="l-members-page__row">
              <div className="l-members-page__content-card">
                <Portfolio
                  items={this.portfolio}
                  toggleSelectPlatform={() => {}}
                  membersPage
                />
              </div>
            </div>
          </div>
        </Fragment>
      </div>
    );
  }
}

MemberPage.propTypes = {
  photo: PropTypes.any,
  name: PropTypes.any,
  tagsMain: PropTypes.any,
  tagsSecond: PropTypes.any,
  following: PropTypes.any,
  followers: PropTypes.any,
  winRate: PropTypes.any,
  getProfileData: PropTypes.any,
  about: PropTypes.any,
  rank: PropTypes.any
};
