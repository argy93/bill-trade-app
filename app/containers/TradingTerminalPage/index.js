import React from 'react';
import { withRouter } from 'react-router-dom';
import TradingTerminalPage from './TradingTerminalPage';

const mapDispatchToProps = () => ({});

const TradingTerminalPageWithRouter = withRouter((props) => <TradingTerminalPage {...props} />);

export default TradingTerminalPageWithRouter;
export {
  mapDispatchToProps
};
