/*
 * TradingTerminalPage
 *
 * This is the first thing users see when they start using our service, at the '/payment' route
 */

// main imports
import React from 'react';
// import PropTypes from 'prop-types';
import TradingViewWidget, {
  Themes
} from 'react-tradingview-widget';


// components import
import TradingTerminalTable from '../../components/TradingTerminalTable';
import expandWithId from '../../utils/expandWithId';

// styles imports
import './style.scss';

const headData = [{
  name: 'Ticker'
},
{
  name: 'Date'
},
{
  name: '%Dep'
},
{
  name: 'Amount'
},
{
  name: 'Vol $'
},
{
  name: 'Price'
},
{
  name: 'S/L'
},
{
  name: 'T/P'
},
{
  name: 'Last Price'
},
{
  name: 'Swap'
},
{
  name: 'P&L C'
},
{
  name: 'P&L $'
},
{
  name: '% P&L P'
},
{
  name: 'COMSN $'
},
{
  name: 'Cmnt'
},
{
  name: '฿ $'
}
];

const rows = [
  {
    ticker: 'bnb/usdt',
    date: '2019-10-09T09:23:58.301550222Z',
    dep: '3.21',
    amount: '720k',
    vol: '420',
    price: '0.00000003',
    sl: '0.00000002',
    tp: '0.00000003',
    lastPrice: '0.00000004',
    swap: '-0.12',
    plc: '672',
    pld: '24.32',
    plp: '24',
    comsn: '1.64',
    cmnt: 'Первая тестовая...',
    profit: 44,
    lose: 0,
  },
  {
    ticker: 'bnb/usdt',
    date: '2019-10-09T09:23:58.301550222Z',
    dep: '3.21',
    amount: '720k',
    vol: 420,
    price: '0.00000003',
    sl: '0.00000002',
    tp: '0.00000003',
    lastPrice: '0.00000004',
    swap: '-0.12',
    plc: 672,
    pld: 24.32,
    plp: 24,
    comsn: '1.64',
    cmnt: 'Первая тестовая...',
    profit: 0,
    lose: 12,
  },
  {
    ticker: 'bnb/usdt',
    date: '2019-10-09T09:23:58.301550222Z',
    dep: '3.21',
    amount: '720k',
    vol: 420,
    price: '0.00000003',
    sl: '0.00000002',
    tp: '0.00000003',
    lastPrice: '0.00000004',
    swap: '-0.12',
    plc: 672,
    pld: 24.32,
    plp: 24,
    comsn: '1.64',
    cmnt: 'Первая тестовая...',
    profit: 87,
    lose: 0,
  },
  {
    ticker: 'bnb/usdt',
    date: '2019-10-09T09:23:58.301550222Z',
    dep: '3.21',
    amount: '720k',
    vol: 420,
    price: '0.00000003',
    sl: '0.00000002',
    tp: '0.00000003',
    lastPrice: '0.00000004',
    swap: '-0.12',
    plc: 672,
    pld: 24.32,
    plp: 24,
    comsn: '1.64',
    cmnt: 'Первая тестовая...',
    profit: 0,
    lose: 44,
  },
];

// eslint-disable-line react/prefer-stateless-function
export default class TradingTerminalPage extends React.PureComponent {
  state = {
    tableHead: expandWithId(headData, {
      visible: true,
    }),
    tableRows: expandWithId(rows, {
      visible: true,
      opened: false,
    }),
  }

  render() {
    const {
      tableHead,
      tableRows,
    } = this.state;
    return (
      <div className="l-trading-terminal-page">
        <div className="widget">
          <TradingViewWidget
            symbol="BITMEX:XBT"
            theme={Themes.LIGHT}
            locale="en"
            autosize
          />
        </div>
        <TradingTerminalTable head={tableHead} rows={tableRows} />
      </div>
    );
  }
}

TradingTerminalPage.propTypes = {

};
