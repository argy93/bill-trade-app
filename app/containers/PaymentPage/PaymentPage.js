/*
 * PaymentPage
 *
 * This is the first thing users see when they start using our service, at the '/payment' route
 */

// main imports
import React from 'react';
// import PropTypes from 'prop-types';
import {
  Tabs,
  Tab,
  TabPanel,
  TabList
} from 'react-web-tabs';

// components import

import ProfitLose from '../../components/ProfitLose';
import Profit from '../../components/Profit';
import Expenses from '../../components/Expenses';

// styles imports
import './style.scss';


// eslint-disable-line react/prefer-stateless-function
export default class PaymentPage extends React.PureComponent {
  state = {
    activeTab: 'one',
  }

  handleTabChange = (activeTab) => {
    this.setState({
      activeTab,
    });
  }

  render() {
    const { activeTab } = this.state;
    return (
      <div className="l-payment">
        <Tabs
          defaultTab={activeTab}
          className="tabs-container"
          onChange={this.handleTabChange}
        >
          <TabList className="tab-list">
            <Tab className={activeTab === 'one' ? 'active blue' : 'blue'} tabFor="one">P&L</Tab>
            <Tab className={activeTab === 'two' ? 'active green' : ''} tabFor="two">Profit</Tab>
            <Tab className={activeTab === 'three' ? 'active red' : ''} tabFor="three">Expenses</Tab>
          </TabList>
          <TabPanel tabId="one">
            <ProfitLose />
          </TabPanel>
          <TabPanel tabId="two">
            <Profit />
          </TabPanel>
          <TabPanel tabId="three">
            <Expenses />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

PaymentPage.propTypes = {
  // history: PropTypes.any
};
