import React from 'react';
import { withRouter } from 'react-router-dom';
import PaymentPage from './PaymentPage';

const mapDispatchToProps = () => ({});

const PaymentPageWithRouter = withRouter((props) => <PaymentPage {...props} />);

export default PaymentPageWithRouter;
export {
  mapDispatchToProps
};
