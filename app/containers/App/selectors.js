import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectGlobal = (state) => state.global || initialState.global;

const selectProfile = (state) => selectGlobal(state)
  .get('profile');

const makeSelectRoute = (state) => state.router;

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  (globalState) => globalState.currentUser,
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.loading,
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.error,
);

const makeSelectPhoto = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('photo'),
);

const makeSelectName = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('name'),
);

const makeSelectTagsMain = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['tags', 'tagsMain'])
    .toJS(),
);

const makeSelectTagsSecond = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['tags', 'tagsSecond'])
    .toJS(),
);

const makeSelectFollowing = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'following']),
);

const makeSelectFollowers = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'followers']),
);

const makeSelectWinRate = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'winRate']),
);

const makeSelectRank = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'rank']),
);

const makeSelectMaxLoss = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'maxLoss']),
);

const makeSelectAverageTime = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'averageTime']),
);

const makeSelectTrades = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'trades']),
);

const makeSelectTradingType = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['stats', 'tradingType']),
);

const makeSelectPortfolio = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('portfolio')
    .toJS(),
);

const makeSelectBalanceUsd = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['finance', 'balance', 'usd']),
);

const makeSelectBalanceBtc = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['finance', 'balance', 'btc']),
);

const makeSelectProfitUsd = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['finance', 'profit', 'usd']),
);

const makeSelectProfitBtc = () => createSelector(
  selectProfile,
  (profileState) => profileState.getIn(['finance', 'profit', 'btc']),
);

const makeSelectAbout = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('about'),
);

const makeSelectTransactions = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('transactions')
    .toJS(),
);

const makeSelectPercentTransactions = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('percentTransactions')
    .toJS(),
);

const makeSelectStrides = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('strides')
    .toJS(),
);

const makeSelectProfileLoading = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('loading'),
);

const makeSelectProfileError = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('error'),
);

const makeSelectProfileErrorMessage = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('errorMessage'),
);

const makeSelectSidebarExpand = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('sidebarExpand'),
);


export {
  selectGlobal,
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectError,
  makeSelectRoute,
  selectProfile,
  makeSelectPhoto,
  makeSelectName,
  makeSelectTagsMain,
  makeSelectTagsSecond,
  makeSelectFollowing,
  makeSelectFollowers,
  makeSelectWinRate,
  makeSelectRank,
  makeSelectMaxLoss,
  makeSelectAverageTime,
  makeSelectTrades,
  makeSelectTradingType,
  makeSelectPortfolio,
  makeSelectBalanceUsd,
  makeSelectBalanceBtc,
  makeSelectProfitUsd,
  makeSelectProfitBtc,
  makeSelectAbout,
  makeSelectTransactions,
  makeSelectProfileLoading,
  makeSelectProfileError,
  makeSelectProfileErrorMessage,
  makeSelectStrides,
  makeSelectPercentTransactions,
  makeSelectSidebarExpand
};
