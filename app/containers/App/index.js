import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';

import {
  makeSelectSidebarExpand
} from './selectors';
import reducer from './reducer';

import App from './App';
import saga from './saga';
import { profileRequest } from './actions';

const mapDispatchToProps = (dispatch) => ({
  profileRequest: () => dispatch(profileRequest())
});

const withReducer = injectReducer({
  key: 'global',
  reducer,
});

const withSaga = injectSaga({
  key: 'profile',
  saga,
});

const mapStateToProps = createStructuredSelector({
  sidebarExpand: makeSelectSidebarExpand()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withReducer, withSaga, withConnect)(App);

export { mapDispatchToProps };
