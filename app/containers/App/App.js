/* eslint-disable arrow-body-style */
/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import Auth from './auth';
import NotFoundPage from '../NotFoundPage/Loadable';
import ProfilePage from '../ProfilePage';
import LoginPage from '../LoginPage';
import RegisterPage from '../RegisterPage';
import SettingsPage from '../SettingsPage';
import PaymentPage from '../PaymentPage';
import MemberPage from '../MemberPage';
import Header from '../Header';
import Sidebar from '../../components/Sidebar';
import TopMembersPage from '../TopMembersPage/Loadable';
import RegisterConfirm from '../RegisterConfirm';
import PasswordRecovery from '../PasswordRecovery';
import AffiliateProgramPage from '../AffiliateProgramPage';
import TradingTerminalPage from '../TradingTerminalPage';
import Subscription from '../../components/SubscriptionModal';

import 'react-toastify/dist/ReactToastify.css';
import './style.scss';

class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    isAuthenticated$: Auth.authQuery$,
    isAuthenticated: Auth.isAuthenticated(),
    authSubscription: null,
  }

  componentDidMount() {
    const {
      isAuthenticated$,
      isAuthenticated,
    } = this.state;

    const {
      profileRequest
    } = this.props;

    if (isAuthenticated) profileRequest();

    const authSubscription = isAuthenticated$.subscribe((authState) => {
      this.setState({
        isAuthenticated: authState,
        authSubscription,
      });

      if (authState) profileRequest();
    });
  }

  componentWillUnmount() {
    const {
      authSubscription,
    } = this.state;
    if (authSubscription) {
      authSubscription.unsubscribe();
    }
  }

  render() {
    const {
      isAuthenticated,
    } = this.state;
    const {
      sidebarExpand
    } = this.props;

    const AuthPages = (
      <div className="l-auth">
        <Helmet
          titleTemplate="BillTrade Login"
          defaultTitle="BillTrade Login"
        >
          <meta name="description" content="BillTrade login page" />
        </Helmet>
        <Switch>
          <Route exact path="/sign-in" component={LoginPage} />
          <Route exact path="/sign-up" component={RegisterPage} />
          <Route exact path="/register/confirm" component={RegisterConfirm} />
          <Route exact path="/recovery" component={PasswordRecovery} />
          <Redirect from="/" to="/sign-in" />
          <Redirect from="" to="/sign-in" />
        </Switch>
      </div>
    );

    const ProfilePages = (
      <div className="l-app">
        <Helmet
          titleTemplate="%s - React.js Boilerplate"
          defaultTitle="React.js Boilerplate"
        >
          <meta name="description" content="A React.js Boilerplate application" />
        </Helmet>
        <Sidebar
          expand={sidebarExpand}
        />
        <div className="l-app__content">
          <Header />
          <Switch>
            <Route exact path="/" component={ProfilePage} />
            <Route exact path="/top-members" component={TopMembersPage} />
            <Route path="/top-members/:id" component={MemberPage} />
            <Route exact path="/trading-terminal" component={TradingTerminalPage} />
            <Route exact path="/affiliate-program" component={AffiliateProgramPage} />
            <Route exact path="/payment" component={PaymentPage} />
            <Route exact path="/support" component={Subscription} />
            <Route exact path="/settings" component={SettingsPage} />
            <Redirect from="/sign-in" to="/" />
            <Redirect from="/sign-up" to="/" />
            <Route path="" component={NotFoundPage} />
          </Switch>
        </div>
      </div>
    );

    return (
      <div>
        { isAuthenticated ? ProfilePages : AuthPages }
      </div>
    );
  }
}

App.propTypes = {
  sidebarExpand: PropTypes.bool,
  profileRequest: PropTypes.func,
};

export default App;
