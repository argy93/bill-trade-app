import {
  call, put, takeLatest, getContext,
} from 'redux-saga/effects';

import { profileRequestSuccess, profileRequestError } from './actions';
import { PROFILE_REQUEST } from './constants';

export function* getProfileData() {
  const { endpoints } = yield getContext('api');
  try {
    const data = yield call(endpoints.getProfile);

    yield put(profileRequestSuccess(data));
  } catch (err) {
    yield put(profileRequestError(err));
  }
}

export default function* profileSaga() {
  yield takeLatest(PROFILE_REQUEST, getProfileData);
}
