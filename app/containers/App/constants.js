/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

// ACTIONS
export const LOAD_REPOS = 'boilerplate/App/LOAD_REPOS';
export const LOAD_REPOS_SUCCESS = 'boilerplate/App/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS_ERROR = 'boilerplate/App/LOAD_REPOS_ERROR';
export const DEFAULT_LOCALE = 'en';
export const PROFILE_REQUEST = 'bt/App/PROFILE_REQUEST';
export const PROFILE_REQUEST_SUCCESS = 'bt/App/PROFILE_REQUEST_SUCCESS';
export const PROFILE_REQUEST_ERROR = 'bt/App/PROFILE_REQUEST_ERROR';
export const TOGGLE_SIDEBAR = 'bt/App/TOGGLE_SIDEBAR';

// URL's
// @todo: SHOULD BE CONFIGURED AND DEPENDED ON ENV. MAYBE PASSED VIA JENKINS!
export const BASE_URL = `${process.env.API_URL || 'https://api.dev.billtrade.io'}/account`;


export const SIGNIN_URL = `${BASE_URL}/account/auth/login`;
export const SIGNUP_CONFIRM_URL = `${BASE_URL}/account/auth/register/confirm`;
export const SIGNUP_URL = `${BASE_URL}/account/auth/register`;
export const CHECK_ME_URL = `${BASE_URL}/account/me`;


// list of response codes for auth requests
export const DEFAULT_ERROR_MESSAGE = 'Server error. Please try again.';
export const AUTH_ERROR_CODES = {
  1006: 'Server error, account not created. Please try again.',
  17: DEFAULT_ERROR_MESSAGE,
  19: DEFAULT_ERROR_MESSAGE,
  18: DEFAULT_ERROR_MESSAGE,
  16: DEFAULT_ERROR_MESSAGE,
  8: DEFAULT_ERROR_MESSAGE,
  1033: DEFAULT_ERROR_MESSAGE,
  10: DEFAULT_ERROR_MESSAGE,
  9: DEFAULT_ERROR_MESSAGE,
  2: 'Invalid credentials',
  1001: 'Invalid E-mail',
  1002: 'E-mail already taken',
  1003: 'E-mail does not exist',
  1004: 'Name already taken',
  1034: 'Account not found',
};
