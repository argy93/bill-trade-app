// The initial state of the App
import * as immutable from 'immutable';
import {PROFILE_REQUEST, PROFILE_REQUEST_ERROR, PROFILE_REQUEST_SUCCESS, TOGGLE_SIDEBAR} from './constants';

export const initialState = immutable.fromJS({
  loading: false,
  error: false,
  sidebarExpand: false,
  profile: {
    loading: false,
    error: false,
    photo: '',
    name: '',
    tags: {
      tagsMain: [],
      tagsSecond: [],
    },
    stats: {
      following: 0,
      followers: 0,
      winRate: 0,
      rank: 0,
      maxLoss: 0,
      averageTime: 0,
      trades: 0,
      tradingType: '',
    },
    portfolio: [],
    finance: {
      balance: {
        usd: 0,
        btc: 0,
      },
      profit: {
        usd: 0,
        btc: 0,
      },
    },
    about: '',
    transactions: [],
    percentTransactions: [],
    strides: []
  },
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case PROFILE_REQUEST:
      return state.setIn(['profile', 'loading'], true);
    case PROFILE_REQUEST_SUCCESS:
      return state
        .set('profile', immutable.fromJS(action.data))
        .setIn(['profile', 'loading'], false)
        .setIn(['profile', 'error'], false);
    case PROFILE_REQUEST_ERROR:
      return state
        .setIn(['profile', 'errorMesssage'], immutable.fromJS(action.message))
        .setIn(['profile', 'error'], true);
    case TOGGLE_SIDEBAR:
      return state.set('sidebarExpand', !state.get('sidebarExpand'));
    default:
      return state;
  }
}

export default appReducer;
