/* eslint-disable camelcase */
/*
 * Auth Service
 * Guard type service that protects profile pages from unathorized access
 */

import axios from 'axios';
import {
  Subject
} from 'rxjs';
import {
  SIGNIN_URL,
  SIGNUP_URL,
  AUTH_ERROR_CODES,
  DEFAULT_ERROR_MESSAGE,
  SIGNUP_CONFIRM_URL
} from './constants';

// axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.withCredentials = true;

const Auth = {
  authQuery$: new Subject({
    authorized: !!sessionStorage.getItem('bill-trade-authorized')
  }),

  // login request starts here
  login(email, password) {
    const loginPromise = new Promise((resolve, reject) => {
      axios.post(SIGNIN_URL, {
        email,
        password,
      })
        .then((response) => {
          this.setSession(response.data, response.headers);
          this.isAuthenticated();
          this.authQuery$.next({
            authorized: true,
          });
          // resolve(response.data);
        })
        .catch(({ response }) => {
          console.log(response);
          console.log(response.data);
          console.log(response.data.error);
          console.log(response.data.error.code);
          try {
            const errorMessage = this.handleAuthError(response.data.error.code);
            reject(errorMessage);
          } catch (e) {
            const errorMessage = this.handleAuthError();
            reject(errorMessage);
          }
        });
    });
    return loginPromise;
  },

  // register request starts here
  // eslint-disable-next-line camelcase
  register(email, password, display_name) {
    const registerPromise = new Promise((resolve, reject) => {
      axios.post(SIGNUP_URL, {
        email,
        password,
        primary_language: 'en',
        primary_currency: 'USDT',
        display_name,
      })
        .then(({ data }) => {
          resolve(data);
        })
        .catch(({ response }) => {
          try {
            const errorMessage = this.handleAuthError(response.data.error.code);
            reject(errorMessage);
          } catch (e) {
            const errorMessage = this.handleAuthError();
            reject(errorMessage);
          }
        });
    });
    return registerPromise;
  },

  confirm(signup_confirmation_code) {
    return axios.post(SIGNUP_CONFIRM_URL, {
      signup_confirmation_code,
    });
  },

  recover() {
    return axios.post(SIGNUP_CONFIRM_URL, {

    });
  },

  // Sets user details in localStorage
  setSession() {
    sessionStorage.setItem('bill-trade-authorized', true);
  },

  handleAuthError(code) {
    if (code) {
      // eslint-disable-next-line no-restricted-syntax
      for (const key in AUTH_ERROR_CODES) {
        if (Number(key) === code) {
          return AUTH_ERROR_CODES[key];
        }
      }
    }
    return DEFAULT_ERROR_MESSAGE;
  },

  // checks if the user is authenticated
  isAuthenticated() {
    const isAuthorized = sessionStorage.getItem('bill-trade-authorized');
    return !!isAuthorized;
  },

  // removes user details from localStorage
  logout() {
    sessionStorage.removeItem('bill-trade-authorized');
    this.authQuery$.next({
      authorized: false,
    });
  }
};

export default Auth;
