import React from 'react';
import { withRouter } from 'react-router-dom';
import PasswordRecovery from './PasswordRecovery';

// eslint-disable-next-line no-unused-vars
const mapDispatchToProps = (dispatch) => ({});

const PasswordRecoveryWithRouter = withRouter((props) => <PasswordRecovery {...props} />);

export default PasswordRecoveryWithRouter;
export {
  mapDispatchToProps
};
