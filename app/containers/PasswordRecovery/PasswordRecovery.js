/* eslint-disable react/no-unused-prop-types */
/*
 * PasswordRecovery
 *
 * This is the first thing users see when they start auth, at the '/sign-in' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import Auth from '../App/auth';

import {
  DEFAULT_ERROR_MESSAGE,
} from '../App/constants';

import Title from '../../components/Title';
import Input from '../../components/Input';
import Button from '../../components/Button';

import background from '../../images/auth-background.svg';
import logo from '../../images/logo-white.svg';

// import { toast } from 'react-toastify';
import './style.scss';

// eslint-disable-line react/prefer-stateless-function
export default class PasswordRecovery extends React.PureComponent {
  state = {
    oldpassword: '',
    newpassword: '',
    confirmpassword: '',
  }

  handleConfirm = () => {
    const { history } = this.props;
    Auth.confirm()
      .then(() => {
        toast('✅ Your account has been successfully recovered.');
        history.push('/sign-in');
      })
      .catch((error) => {
        toast(DEFAULT_ERROR_MESSAGE);
        console.log(error);
      });
  }

  handleDataChange = (name, value) => {
    this.setState({
      [name]: value
    });
  }

  render() {
    const { oldpassword, newpassword, confirmpassword } = this.state;
    return (
      <div className="l-register-confirm">
        <div
          className="l-register__background"
        >
          <img src={logo} alt="Bill Trade" className="l-register__logo" />
          <div
            className="img"
            style={{ backgroundImage: `url(${background})` }}
          >
          </div>
        </div>
        <div className="l-register__form">
          <div className="wrapper">
            <form className="flex-container">
              <div className="column">
                <Title size="B4" weight="bold">Recover you account</Title>
                <br />
                <Input
                  index={1}
                  label="Old password"
                  defaultValue={oldpassword}
                  noPasswordIcon
                  type="password"
                  name="oldpassword"
                  onChange={this.handleDataChange}
                >
                </Input>
                <Input
                  index={100}
                  label="New password"
                  defaultValue={newpassword}
                  noPasswordIcon
                  type="password"
                  name="newpassword"
                  onChange={this.handleDataChange}
                >
                </Input>
                <Input
                  index={200}
                  label="Confirm password"
                  defaultValue={confirmpassword}
                  noPasswordIcon
                  type="password"
                  name="confirm"
                  onChange={this.handleDataChange}
                >
                </Input>
                <Button
                  index={700}
                  type="contained"
                  onClick={this.handleConfirm}
                >
                  Recover
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

PasswordRecovery.propTypes = {
  location: PropTypes.any,
  history: PropTypes.any,
};
