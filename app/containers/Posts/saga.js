import {
  takeLatest,
  delay,
  put,
} from 'redux-saga/effects';
import previewImg from './images/preview-img.png';
import { REQUEST_POSTS } from './constants';
import { requestPostsSuccess } from './actions';

function* requestPosts() {
  const data = [
    {
      id: 1,
      photo: '',
      author: 'Shaya2121',
      timestamp: 1567692851987,
      short: '#',
      excerpt: 'Investing.com - Bitcoin brach am Dienstag in Asien um mehr als 10% ein und fiel unter die kritische Marke von 10.000 USD...',
      preview: previewImg
    },
    {
      id: 3,
      photo: '',
      author: 'Lil',
      timestamp: 1567692851987,
      short: '#',
      excerpt: 'Investing.com - Bitcoin brach am Dienstag in Asien um mehr als 10% ein und fiel unter die kritische Marke von 10.000 USD...',
      preview: previewImg
    },
    {
      id: 4,
      photo: '',
      author: 'Irvine1290',
      timestamp: 1567692851987,
      short: '#',
      excerpt: 'Investing.com - Bitcoin brach am Dienstag in Asien um mehr als 10% ein und fiel unter die kritische Marke von 10.000 USD...',
      preview: previewImg
    }
  ];

  yield delay(400);
  yield put(requestPostsSuccess(data));
}

export default function* postsSaga() {
  yield takeLatest(REQUEST_POSTS, requestPosts);
}
