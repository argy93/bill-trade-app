import {
  REQUEST_POSTS,
  REQUEST_POSTS_SUCCESS,
  REQUEST_POSTS_ERROR,
  CHANGE_POSTS_FILTER,
} from './constants';

export function requestPosts() {
  return {
    type: REQUEST_POSTS,
  };
}

export function requestPostsSuccess(posts) {
  return {
    type: REQUEST_POSTS_SUCCESS,
    posts,
  };
}

export function requestPostsError(message) {
  return {
    type: REQUEST_POSTS_ERROR,
    message,
  };
}

export function changePostsFilter(filter) {
  return {
    type: CHANGE_POSTS_FILTER,
    filter,
  };
}
