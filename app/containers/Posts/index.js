import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectSaga from '../../utils/injectSaga';
import saga from './saga';
import Posts from './Posts';
import injectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import {
  changePostsFilter,
  requestPosts,
} from './actions';
import {
  makeSelectFilteredItems,
  makeSelectFilterType,
} from './selectors';

const mapDispatchToProps = (dispatch) => ({
  changePostsFilter: (filter) => dispatch(changePostsFilter(filter)),
  requestPosts: () => dispatch(requestPosts()),
});

const withReducer = injectReducer({
  key: 'posts',
  reducer,
});

const mapStateToProps = createStructuredSelector({
  filterType: makeSelectFilterType(),
  posts: makeSelectFilteredItems(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({
  key: 'posts',
  saga,
});

export default compose(withReducer, withSaga, withConnect)(Posts);
export { mapDispatchToProps };
