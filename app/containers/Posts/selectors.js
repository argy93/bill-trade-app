import { createSelector } from 'reselect';
import { makeSelectName } from '../App/selectors';

const selectPosts = (state) => state.posts;

const makeSelectLoading = () => createSelector(
  selectPosts,
  (posts) => posts.get('loading'),
);

const makeSelectError = () => createSelector(
  selectPosts,
  (posts) => posts.get('error'),
);

const makeSelectFilterType = () => createSelector(
  selectPosts,
  (posts) => posts.get('filterType'),
);

const makeSelectItems = () => createSelector(
  selectPosts,
  (posts) => posts.get('items').toJS(),
);

const makeSelectFilteredItems = () => createSelector(
  makeSelectItems(),
  makeSelectFilterType(),
  makeSelectName(),
  (items, filterType, selfLogin) => {
    const limit = 3;
    let result = [];

    if (filterType === 'all') {
      result = items;
    } else if (filterType === 'self') {
      result = items.filter((item) => item.author === selfLogin);
    } else if (filterType === 'other') {
      result = items.filter((item) => item.author !== selfLogin);
    } else {
      // eslint-disable-next-line no-console
      console.warn('undefined filter type');
    }

    if (result.length > limit) {
      result = result.slice(0, limit);
    }

    return result;
  },
);

export {
  makeSelectLoading,
  makeSelectError,
  makeSelectFilterType,
  makeSelectFilteredItems,
};
