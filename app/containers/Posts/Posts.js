import React, { Fragment } from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import PostsComponent from '../../components/Posts';

export default class Posts extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    const { requestPosts } = this.props;

    requestPosts();
  }

  render() {
    const {
      loading,
      error,
      changePostsFilter,
      filterType,
      posts,
    } = this.props;

    return (
      <Fragment>
        <PostsComponent
          changePostsFilter={changePostsFilter}
          filterType={filterType}
          items={posts}
        />
      </Fragment>
    );
  }
}

Posts.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.bool,
  requestPosts: PropTypes.func,
  changePostsFilter: PropTypes.func,
  filterType: PropTypes.oneOf(['all', 'other', 'self']),
  posts: PropTypes.array,
};
