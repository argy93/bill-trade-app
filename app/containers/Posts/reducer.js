import * as immutable from 'immutable';
import {
  REQUEST_POSTS_SUCCESS,
  CHANGE_POSTS_FILTER,
  REQUEST_POSTS,
  REQUEST_POSTS_ERROR,
} from './constants';

// The initial state of the App
const initialState = immutable.fromJS({
  loading: false,
  error: false,
  filterType: 'all', // other | self
  items: [],
});

function topTradesListReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_POSTS:
      return state.set('loading', true);
    case REQUEST_POSTS_SUCCESS:
      return state.set('items', immutable.fromJS(action.posts))
        .set('loading', 'false');
    case REQUEST_POSTS_ERROR:
      return state.set('loading', false)
        .set('error', true);
    case CHANGE_POSTS_FILTER:
      return state.set('filterType', action.filter);
    default:
      return state;
  }
}

export default topTradesListReducer;
