import React from 'react';
import { withRouter } from 'react-router-dom';
import AffiliateProgramPage from './AffiliateProgramPage';

const mapDispatchToProps = () => ({});

const AffiliateProgramPageWithRouter = withRouter((props) => <AffiliateProgramPage {...props} />);

export default AffiliateProgramPageWithRouter;
export {
  mapDispatchToProps
};
