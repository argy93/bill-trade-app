/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/no-array-index-key */
/*
 * AffiliateProgramPage
 *
 * This is the first thing users see when they start using our service, at the '/payment' route
 */

// main imports
import React from 'react';
// import PropTypes from 'prop-types';
import formatDate from 'utils/formatDate';

// components import
import SubscriptionStats from '../../components/SubscriptionStats';
import Done from '../../components/Icons/DoneIcon';

// styles imports
import './style.scss';


// icons import
import copy from './copy.svg';

// eslint-disable-line react/prefer-stateless-function
export default class AffiliateProgramPage extends React.PureComponent {
  state = {
    isCreatingNewRef: false,
    showSuccess: false,
    newRef: '',
    tableHeaders: ['ID', 'Register date', 'Status', 'Code', 'Activity'],
    tableData: [
      {
        id: '07854',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
      {
        id: '07855',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
      {
        id: '07853',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
      {
        id: '07254',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
      {
        id: '07884',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
      {
        id: '02654',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
      {
        id: '23423',
        created_at: '2019-09-30T11:52:28.811Z',
        status: 'Trader',
        code: 'default',
        activity: 'Yes'
      },
    ],
    rewardData: [{
      name: 'Today',
      value: '0',
      amount: '13.37 USD',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
      ]
    },
    {
      name: 'Week',
      value: '1',
      amount: '220.12 USD',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
      ]
    },
    {
      name: 'Month',
      value: '3',
      amount: '420.21 USD',
      isOpened: false,
      subitems: [
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
      ]
    },
    {
      name: 'All period',
      value: '12',
      amount: '830.02 USD',
      isOpened: true,
      subitems: [
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
        {
          name: 'Trader1234567',
          index: '3',
          value: '2,841.09 USD',
        },
      ]
    },
    ],
  };

  renderTableHead = (data) => (
    <div className="table-head">
      {
        data.map((value, key) => (
          <div
            className="head-cell"
            key={key}
          >
            { value }
          </div>
        ))
      }
    </div>
  )

  renderTableRows = (data) => (
    <div className="table-rows">
      {
        data.map((value) => {
          const date = formatDate(value.created_at);
          return (
            <div className="row" key={value.id}>
              <div className="item">{ value.id }</div>
              <div className="item">
                {date}
              </div>
              <div className="item">{ value.status }</div>
              <div className="item">{ value.code }</div>
              <div className="item">{ value.activity || 'none' }</div>
            </div>
          );
        })
      }
    </div>
  )

  handleCreateToggle = () => {
    const { isCreatingNewRef } = this.state;
    this.setState({
      isCreatingNewRef: !isCreatingNewRef,
      newRef: '',
    });
  }

  handleChange = ({ target }) => {
    this.setState({
      newRef: target.value,
    });
  }

  handleNewRef = () => {
    const { newRef } = this.state;
    console.log(newRef);
    this.setState({
      showSuccess: true,
    });
  }

  render() {
    const {
      tableHeaders,
      tableData,
      rewardData,
      isCreatingNewRef,
      showSuccess,
    } = this.state;
    const copyIcon = <img className="copy-icon" src={copy} alt="copy" />;
    const tableHead = this.renderTableHead(tableHeaders);
    const tableBody = this.renderTableRows(tableData);

    const createRefForm = (
      <div className="create-ref-form">
        <div className="title">Create Referral Link</div>
        <input type="text" onChange={this.handleChange} placeholder="Referral link 01" />
        <div className="buttons-container">
          <div className="column"></div>
          <div className="column"></div>
          <div className="column">
            <div className="cancel-button" onClick={this.handleCreateToggle}>Cancel</div>
            <div className="submit-button" onClick={this.handleNewRef}>Create refferal link</div>
          </div>
        </div>
      </div>
    );

    const successMessage = (
      <div className="success-message">
        <span className="icon">
          <Done />
        </span>
        <span className="text">
          New referral link is successfully created!
        </span>
      </div>
    );
    return (
      <div className="l-affiliate-program-wrapper">
        <div className="l-affiliate-program">
          <div className="header">
            <div className="column">
              <div className="title">Affiliate program</div>
            </div>
            <div className="column">
              <div className="sub-column">ID: 80145</div>
              <div className="sub-column">Your code: E3412lQ</div>
            </div>
          </div>
          <div className="divider"></div>
          <div className="conditions">
            <div className="title">Conditions and terms of use of Referral System:</div>
            <div className="description">Invite a friend to join Bill Trade. Get 3% of the profit he makes on Bill Trade as an investor. If he invites a friend, you’ll get 2% of his friend’s profit. You’ll also benefit in case the last one invites his friend - 1% of the profit. If a trader joins Bill Trade via your affiliate link, you’ll get 1% of his profit.</div>
            { isCreatingNewRef && !showSuccess ? createRefForm : '' }
            { isCreatingNewRef && showSuccess ? successMessage : '' }
            <div className="refferal-codes">
              <div className="column">
                <div className="title">My refferal codes</div>
                <div className="code">
                  <div className="column">
                    Default
                  </div>
                  <div className="column">
                    E3412lQ {copyIcon}
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="button-container">
                  { !isCreatingNewRef ? <div className="button" onClick={this.handleCreateToggle}>Create refferal link</div> : ''}
                </div>
              </div>
            </div>
          </div>
          <div className="divider"></div>
          <div className="page-links">
            <div className="title">Page links</div>
            <div className="links-container">
              <div className="row">
                <div className="name">Trading platform page</div>
                <div className="link">{copyIcon} https://billtrade.io/main/E3412lQ</div>
              </div>
              <div className="row">
                <div className="name">Affiliate program</div>
                <div className="link">{copyIcon} https://billtrade.io/main/E3412lQ</div>
              </div>
              <div className="row">
                <div className="name">Registration form</div>
                <div className="link">{copyIcon} https://billtrade.io/main/E3412lQ</div>
              </div>
              <div className="row">
                <div className="name">Top traders</div>
                <div className="link">{copyIcon} https://billtrade.io/main/E3412lQ</div>
              </div>
            </div>
          </div>
          <div className="divider"></div>
          <div className="stats">
            <div className="column">
              <div className="title">Last registrations</div>
              <div className="table">
                {tableHead}
                {tableBody}
              </div>
            </div>
            <div className="column">
              <div className="title">Reward</div>
              <SubscriptionStats title="" body={rewardData} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AffiliateProgramPage.propTypes = {
  // history: PropTypes.any
};
