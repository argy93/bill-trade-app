import * as immutable from 'immutable';

// The initial state of the App
const initialState = immutable.fromJS({});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default loginReducer;
