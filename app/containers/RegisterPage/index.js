
import React from 'react';
import { withRouter } from 'react-router-dom';
import RegisterPage from './RegisterPage';

const mapDispatchToProps = () => ({});

const RegisterPageWithRouter = withRouter((props) => <RegisterPage {...props} />);

export default RegisterPageWithRouter;
export {
  mapDispatchToProps
};
