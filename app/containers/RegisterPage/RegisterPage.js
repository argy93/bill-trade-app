/*
 * RegisterPage
 *
 * This is the first thing users see when they start using our service, at the '/sign-up' route
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import Auth from '../App/auth';
import {
  LOGIN_FIELD_EMPTY_MESSAGE,
  EMAIL_FIELD_EMPTY_MESSAGE,
  EMAIL_FIELD_INVALID_MESSAGE,
  PASSWORD_FIELD_EMPTY_MESSAGE,
  CONFIRM_FIELD_EMPTY_MESSAGE,
  PASSWORD_NOT_MATCH_MESSAGE,
  AGREEMENT_NOT_CHECKED,
  FORM_ERRORS_MESSAGE
} from './constants';
import './style.scss';
import background from '../../images/auth-background.svg';
import logo from '../../images/logo-white.svg';
import Title from '../../components/Title';
import Input from '../../components/Input';
import Checkbox from '../../components/Checkbox';
import Button from '../../components/Button';


// eslint-disable-line react/prefer-stateless-function
export default class RegisterPage extends React.PureComponent {
  state = {
    login: {
      touched: false,
      valid: false,
      errorText: LOGIN_FIELD_EMPTY_MESSAGE
    },
    email: {
      touched: false,
      valid: false,
      errorText: EMAIL_FIELD_EMPTY_MESSAGE
    },
    password: {
      touched: false,
      valid: false,
      visible: false,
      errorText: PASSWORD_FIELD_EMPTY_MESSAGE
    },
    confirm: {
      touched: false,
      valid: false,
      visible: false,
      errorText: CONFIRM_FIELD_EMPTY_MESSAGE
    },
    agreeData: false,
    loginData: '',
    emailData: '',
    passwordData: '',
    confirmData: '',
    loading: false,
  }


  handleInputBlur = (inputName) => {
    const input = inputName.toString().toLowerCase();
    // eslint-disable-next-line react/destructuring-assignment
    const { touched, errorText } = this.state[input];

    if (!touched) {
      this.setState({
        [input]: {
          touched: true,
          valid: false,
          errorText,
        }
      });
    }
  }

  handleLoginChange = (value) => {
    if (value.length < 6) {
      this.setState({
        login: {
          touched: true,
          valid: false,
          errorText: LOGIN_FIELD_EMPTY_MESSAGE,
        }
      });
      return;
    }
    this.setState({
      login: {
        touched: true,
        valid: true,
        errorText: ''
      },
      loginData: value,
    });
  }

  handleEmailChange = (value) => {
    if (value.length < 1) {
      this.setState({
        email: {
          touched: true,
          valid: false,
          errorText: EMAIL_FIELD_EMPTY_MESSAGE,
        }
      });
      return;
    }
    if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
      this.setState({
        email: {
          touched: true,
          valid: false,
          errorText: EMAIL_FIELD_INVALID_MESSAGE
        }
      });
      return;
    }
    this.setState({
      email: {
        touched: true,
        valid: true,
        errorText: ''
      },
      emailData: value,
    });
  }

  handlePasswordChange = (passwordData) => {
    const {
      password
    } = this.state;
    if (passwordData.length < 6) {
      this.setState({
        password: {
          touched: true,
          valid: false,
          visible: password.visible,
          errorText: PASSWORD_FIELD_EMPTY_MESSAGE
        },
        passwordData
      });
      return;
    }
    this.setState({
      password: {
        touched: true,
        valid: true,
        visible: password.visible,
        errorText: ''
      },
      passwordData,
    });
  }

  handleConfirmChange = (value) => {
    const { confirm, passwordData } = this.state;
    if (value.length < 1) {
      this.setState({
        confirm: {
          touched: true,
          valid: false,
          visible: confirm.visible,
          errorText: CONFIRM_FIELD_EMPTY_MESSAGE
        }
      });
      return;
    }
    if (passwordData !== value) {
      this.setState({
        confirm: {
          touched: true,
          valid: false,
          visible: confirm.visible,
          errorText: PASSWORD_NOT_MATCH_MESSAGE
        }
      });
      return;
    }
    this.setState({
      confirm: {
        touched: true,
        valid: true,
        visible: confirm.visible,
        errorText: ''
      }
    });
  }

  handleCheckboxChange = (value) => {
    // eslint-disable-next-line no-console
    console.log(value);
    this.setState({ agreeData: !value });
  }

  handleSignIn = () => {
    const { history } = this.props;
    history.push('/sign-in');
  }

  handleSignUp = () => {
    const {
      login,
      email,
      password,
      confirm,
      emailData,
      passwordData,
      loginData,
      agreeData,
      loading,
    } = this.state;

    // prevent double click on submit
    if (loading) { return; }

    this.setState({
      loading: true,
    }, () => {
      const isTouched = login.touched && email.touched && password.touched && confirm.touched;
      const isValid = login.valid && email.valid && password.valid && confirm.valid;

      if (isTouched && isValid && agreeData) {
        this.handleSignUpStart(emailData, passwordData, loginData);
        return;
      }
      if (!isTouched) {
        this.setState({
          login: {
            ...login,
            touched: true,
          },
          email: {
            ...email,
            touched: true,
          },
          password: {
            ...password,
            touched: true,
          },
          confirm: {
            ...confirm,
            touched: true,
          },
          loading: false,
        });
        return;
      }
      if (isValid) {
        toast(AGREEMENT_NOT_CHECKED);
        this.setState({
          loading: false,
        });
        return;
      }
      toast(FORM_ERRORS_MESSAGE);
      this.setState({
        loading: false,
      });
    });
  }

  // eslint-disable-next-line camelcase
  handleSignUpStart = (email, password, display_name) => {
    Auth.register(email, password, display_name)
      .then(() => {
        toast('📩 Follow the link in the email to complete the registration');
      })
      .catch(this.handleError);
  }

  // on failure signin request
  handleError = (message) => {
    toast(message);
    this.setState({
      loading: false,
    });
  }

  handlePasswordToggle = () => {
    const {
      password
    } = this.state;
    this.setState({
      password: {
        valid: password.valid,
        touched: password.touched,
        visible: !password.visible
      }
    });
  }

  handleConfirmToggle = () => {
    const {
      confirm
    } = this.state;
    this.setState({
      confirm: {
        valid: confirm.valid,
        touched: confirm.touched,
        visible: !confirm.visible
      }
    });
  }

  render() {
    const {
      email,
      login,
      password,
      confirm,
      agreeData
    } = this.state;
    return (
      <div className="l-register">
        <Helmet>
          <title>Sign Up Page</title>
          <meta name="description" content="BillTrade Sign Up page" />
          <meta name="viewport" content="initial-scale=1, maximum-scale=1"></meta>
        </Helmet>
        <div
          className="l-register__background"
        >
          <img src={logo} alt="Bill Trade" className="l-register__logo" />
          <div
            className="img"
            style={{ backgroundImage: `url(${background})` }}
          >
          </div>
        </div>
        <div className="l-register__form">
          <div className="wrapper">
            <div className="title-wrapper">
              <Title size="H4" weight="bold">Join Bill Trade</Title>
            </div>
            <div className="title-wrapper">
              <Title size="B4">Welcome to our platform!</Title>
            </div>
            <form className="flex-container">
              <div className="column">
                <Input
                  index={1}
                  name="login"
                  label="Login"
                  valid={login.valid}
                  touched={login.touched}
                  onChange={this.handleLoginChange}
                  onBlur={this.handleInputBlur}
                  errorText={login.errorText}
                >
                </Input>
                <div className="bottom-input-padding">
                  <Input
                    index={300}
                    name="password"
                    label="Password"
                    type="password"
                    valid={password.valid}
                    touched={password.touched}
                    onChange={this.handlePasswordChange}
                    onBlur={this.handleInputBlur}
                    togglePassword={this.handlePasswordToggle}
                    showPassword={password.visible}
                    errorText={password.errorText}
                  >
                  </Input>
                </div>
              </div>
              <div className="column">
                <Input
                  index={200}
                  name="email"
                  label="E-mail"
                  valid={email.valid}
                  touched={email.touched}
                  onChange={this.handleEmailChange}
                  onBlur={this.handleInputBlur}
                  errorText={email.errorText}
                >
                </Input>
                <div className="bottom-input-padding">
                  <Input
                    index={500}
                    name="confirm"
                    type="password"
                    label="Confirm password"
                    valid={confirm.valid}
                    touched={confirm.touched}
                    onChange={this.handleConfirmChange}
                    onBlur={this.handleInputBlur}
                    togglePassword={this.handleConfirmToggle}
                    showPassword={confirm.visible}
                    errorText={confirm.errorText}
                  >
                  </Input>
                </div>
              </div>
            </form>
            <div className="checkbox-wrapper">
              <Checkbox
                index={600}
                name="remember"
                defaultChecked={agreeData}
                label="I agree with Terms of Use, Privacy Policy and AML/KYC"
                onChange={this.handleCheckboxChange}
              >
              </Checkbox>
            </div>
            <div className="flex-container">
              <div className="column">
                <Button
                  index={700}
                  type="contained"
                  onClick={this.handleSignUp}
                >
                  Sign up
                </Button>
              </div>
              <div className="column">
                <Button
                  index={800}
                  type="outlined"
                  onClick={this.handleSignIn}
                >
                  Log in
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RegisterPage.propTypes = {
  history: PropTypes.any
};
