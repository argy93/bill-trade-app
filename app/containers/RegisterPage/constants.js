/*
 * RegisterConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */
import { BASE_URL } from '../App/constants';

export const LOGIN_FIELD_EMPTY_MESSAGE = 'Please create login to sign up';
export const EMAIL_FIELD_EMPTY_MESSAGE = 'Please enter your E-mail';
export const EMAIL_FIELD_INVALID_MESSAGE = 'Please enter valid E-mail';
export const PASSWORD_FIELD_EMPTY_MESSAGE = 'Please create password';
export const CONFIRM_FIELD_EMPTY_MESSAGE = 'Please confirm your password';
export const FORM_ERRORS_MESSAGE = 'Please fill in all fields and try again';
export const AGREEMENT_NOT_CHECKED = 'You should confirm the agreement of Use, Privacy Policy and AML/KYC';
export const PASSWORD_NOT_MATCH_MESSAGE = 'Password does not match';
export const SIGNUP_SUCCESS_MESSAGE = 'Sign up successful ✅';
export const SIGNUP_ERROR_MESSAGE = 'An error occurred while trying to sign up.';

export const SIGNUP_URL = `${BASE_URL}/account/auth/register`;
export const SIGNIN_SUCCESS_MESSAGE = 'Login successful ✅';
export const SIGNIN_ERROR_MESSAGE = 'An error occurred while trying to log in.';
