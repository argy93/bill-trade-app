import React from 'react';
import { withRouter } from 'react-router-dom';
import LoginPage from './LoginPage';

// eslint-disable-next-line no-unused-vars
const mapDispatchToProps = (dispatch) => ({});

const LoginWithRouter = withRouter((props) => <LoginPage {...props} />);

export default LoginWithRouter;
export {
  mapDispatchToProps
};
