/*
 * LoginPage
 *
 * This is the first thing users see when they start auth, at the '/sign-in' route
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Auth from '../App/auth';
import {
  LOGIN_FIELD_INVALID_MESSAGE,
  LOGIN_FIELD_EMPTY_MESSAGE,
} from './constants';
import background from '../../images/auth-background.svg';
import logo from '../../images/logo-white.svg';
import Title from '../../components/Title';
import Input from '../../components/Input';
import Checkbox from '../../components/Checkbox';
import Button from '../../components/Button';

import './style.scss';

// eslint-disable-line react/prefer-stateless-function
export default class LoginPage extends React.PureComponent {
  state = {
    login: {
      touched: false,
      valid: false,
      errorText: LOGIN_FIELD_EMPTY_MESSAGE,
    },
    password: {
      touched: false,
      valid: false,
      visible: false,
    },
    loginData: '',
    passwordData: '',
    rememberData: false,
    loading: false,
  }

  // //////////////////////////////////////////////////////////
  // # Input change handlers
  // //////////////////////////////////////////////////////////

  handleInputBlur = (inputName) => {
    // eslint-disable-next-line react/destructuring-assignment
    const { touched, errorText = '' } = this.state[inputName];
    if (!touched) {
      this.setState({
        [inputName]: {
          touched: true,
          valid: false,
          errorText,
        }
      });
    }
  }

  handleLoginChange = (value) => {
    if (value.length < 1) {
      this.setState({
        login: {
          touched: true,
          valid: false,
          errorText: LOGIN_FIELD_EMPTY_MESSAGE,
        }
      });
      return;
    }
    if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
      this.setState({
        login: {
          touched: true,
          valid: false,
          errorText: LOGIN_FIELD_INVALID_MESSAGE
        }
      });
      return;
    }
    this.setState({
      login: {
        touched: true,
        valid: true,
        errorText: ''
      },
      loginData: value,
    });
  }

  handlePasswordChange = (value) => {
    const { password } = this.state;
    if (value.length > 5) {
      this.setState({
        password: {
          touched: true,
          valid: true,
          visible: password.visible,
        },
        passwordData: value,
      });
    } else {
      this.setState({
        password: {
          touched: true,
          valid: false,
          visible: password.visible,
        }
      });
    }
  }

  handleCheckboxChange = (value) => {
    this.setState({
      rememberData: !value,
    });
  }

  handlePasswordToggle = () => {
    const {
      password
    } = this.state;
    this.setState({
      password: {
        valid: password.valid,
        touched: password.touched,
        visible: !password.visible
      }
    });
  }

  // //////////////////////////////////////////////////////////
  // # Login handlers
  // //////////////////////////////////////////////////////////

  handleSignUp = () => {
    const {
      history
    } = this.props;
    history.push('/sign-up');
  }

  // Method runs when sign in button clicked
  handleSignIn = () => {
    const {
      login,
      password,
      loginData,
      passwordData,
      loading,
    } = this.state;

    // prevent double click on submit
    if (loading) { return; }

    this.setState({
      loading: true,
    }, () => {
      if ((login.touched && password.touched) && (login.valid && password.valid)) {
        this.handleSignInStart(loginData, passwordData);
      } else {
        this.handleSignInStartError();
      }
    });
  }

  // Method that start doing API request and runs when validation passed
  handleSignInStart = (email, password) => {
    Auth.login(email, password)
      .catch(this.handleError);
  }

  // Method runs when validation didn't passed
  handleSignInStartError = () => {
    const {
      login,
      password,
    } = this.state;

    this.setState({
      login: {
        touched: true,
        valid: login.valid,
      },
      password: {
        touched: true,
        valid: password.valid,
        visible: password.visible,
      },
      loading: false,
    });
  }

  // on failure signin request
  handleError = (message) => {
    toast(message);
    this.setState({
      loading: false,
    });
  }

  render() {
    const {
      login,
      password,
      rememberData,
    } = this.state;

    return (
      <div className="l-login">
        <Helmet>
          <title>Sign In Page</title>
          <meta name="description" content="BillTrade Sign In page" />
          <meta name="viewport" content="initial-scale=1, maximum-scale=1"></meta>
        </Helmet>
        <div
          className="l-login__background"
        >
          <img src={logo} alt="Bill Trade" className="l-login__logo" />
          <div
            className="img"
            style={{ backgroundImage: `url(${background})` }}
          >
          </div>
        </div>
        <div className="l-login__form">
          <div className="wrapper">
            <div className="title-wrapper">
              <Title size="H4" weight="bold">Welcome to Bill Trade</Title>
            </div>
            <div className="title-wrapper">
              <Title size="B4">Please enter your personal data to use all features</Title>
            </div>
            <form className="flex-container">
              <div className="column">
                <Input
                  name="login"
                  label="E-mail"
                  valid={login.valid}
                  touched={login.touched}
                  onChange={this.handleLoginChange}
                  onBlur={this.handleInputBlur}
                  errorText={login.errorText}
                >
                </Input>
              </div>
              <div className="column">
                <Input
                  name="password"
                  label="Password"
                  type="password"
                  valid={password.valid}
                  touched={password.touched}
                  onChange={this.handlePasswordChange}
                  togglePassword={this.handlePasswordToggle}
                  showPassword={password.visible}
                  onBlur={this.handleInputBlur}
                  errorText="Please enter your password"
                >
                </Input>
              </div>
            </form>
            <div className="checkbox-wrapper">
              <Checkbox
                name="remember"
                label="Remember me"
                defaultChecked={rememberData}
                onChange={this.handleCheckboxChange}
              >
              </Checkbox>
            </div>
            <div className="flex-container">
              <div className="column">
                <Button
                  type="contained"
                  onClick={this.handleSignIn}
                >
                  Sign in
                </Button>
              </div>
              <div className="column">
                <Button
                  type="outlined"
                  onClick={this.handleSignUp}
                >
                  Sign up
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  history: PropTypes.any,
};
