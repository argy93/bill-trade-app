/*
 * LoginConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */
import { BASE_URL } from '../App/constants';

export const LOGIN_FIELD_EMPTY_MESSAGE = 'Please enter your E-mail';
export const LOGIN_FIELD_INVALID_MESSAGE = 'Please enter valid E-mail';
export const SIGNIN_SUCCESS_MESSAGE = 'Sign in successful ✅';
export const SIGNIN_ERROR_MESSAGE = 'An error occurred while trying to sign in.';

export const SIGNIN_URL = `${BASE_URL}/account/auth/login`;
