import * as immutable from 'immutable';

const initialState = immutable.fromJS({});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default loginReducer;
