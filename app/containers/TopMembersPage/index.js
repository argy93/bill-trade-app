import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import TopMembersPage from './TopMembersPage';
import reducer from './reducer';
import membersReducer from './membersReducer';
import saga from './saga';
import {
  advancedToggle,
  currencyChange,
  membersTypeChange,
  searchUpdate,
  sortMembersChange,
  timePeriodChange,
} from './actions';
import {
  makeSelectAdvanced,
  makeSelectCurrency, makeSelectFilteredMembers,
  makeSelectMembersType,
  makeSelectPeriod,
  makeSelectSearch,
  makeSelectSort,
} from './selectors';

const mapDispatchToProps = (dispatch) => ({
  sortMembersChange: (sortType) => dispatch(sortMembersChange(sortType)),
  searchUpdate: (s) => dispatch(searchUpdate(s)),
  timePeriodChange: (from, to) => dispatch(timePeriodChange(from, to)),
  currencyChange: (currency) => dispatch(currencyChange(currency)),
  membersTypeChange: (type) => dispatch(membersTypeChange(type)),
  advancedToggle: (name) => dispatch(advancedToggle(name)),
});

const withReducer = injectReducer({
  key: 'topMembers',
  reducer,
});

const withMembersReducer = injectReducer({
  key: 'members',
  reducer: membersReducer,
});

const mapStateToProps = createStructuredSelector({
  sort: makeSelectSort(),
  search: makeSelectSearch(),
  period: makeSelectPeriod(),
  currency: makeSelectCurrency(),
  membersType: makeSelectMembersType(),
  advanced: makeSelectAdvanced(),
  filteredMembers: makeSelectFilteredMembers(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({
  key: 'topMembers',
  saga,
});

export default compose(withReducer, withMembersReducer, withSaga, withConnect)(TopMembersPage);
export { mapDispatchToProps };
