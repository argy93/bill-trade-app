import * as immutable from 'immutable';
import {
  ADVANCED_TOGGLE,
  CURRENCY_CHANGE,
  MEMBERS_TYPE_CHANGE,
  SEARCH_UPDATE,
  SORT_MEMBERS_CHANGE,
  TIME_PERIOD_CHANGE,
  REQUEST_MEMBERS_LIST_SUCCESS,
} from './constants';

const initialState = immutable.fromJS({
  filter: {
    sort: 'all', // subscribed
    search: '',
    period: {
      from: 0,
      to: Date.now(),
    },
    currency: 'usd', // btc | eth
    membersType: 'all', // trader | analyst | investor
    advanced: {
      investorsProfit: false,
      dailyProfit: false,
      trades: false,
      maxLoss: false,
      winrate: false,
      averageTime: false,
      maximumDeclaredLoss: false,
      averageProfit: false,
      averageTrades: false,
      averageOrderSize: false,
      profitLossRatio: false,
      minimumFollowingDeposit: false,
      indicators: false,
      technicalAnalysis: false,
      artificialIntelligence: false,
      algorithmicTrading: false,
      fundamentalAnalysis: false,
      arbitrage: false,
      failureOfRmPolity: false,
    },
  },
  members: [],
});

function topMembersReducer(state = initialState, action) {
  switch (action.type) {
    case SORT_MEMBERS_CHANGE:
      return state.setIn(['filter', 'sort'], action.sortType);
    case SEARCH_UPDATE:
      return state.setIn(['filter', 'search'], action.s);
    case TIME_PERIOD_CHANGE:
      return state.setIn(['filter', 'period', 'from'], action.from)
        .setIn(['filter', 'period', 'to'], action.to);
    case CURRENCY_CHANGE:
      return state.setIn(['filter', 'currency'], action.currency);
    case MEMBERS_TYPE_CHANGE:
      return state.setIn(['filter', 'membersType'], action.membersType);
    case ADVANCED_TOGGLE:
      return state.setIn(['filter', 'advanced', action.name], !state.getIn(['advanced', action.name]));
    case REQUEST_MEMBERS_LIST_SUCCESS:
      return state.set('members', action.membersList.map((item) => item.id));
    default:
      return state;
  }
}

export default topMembersReducer;
