import {
  ADVANCED_TOGGLE,
  CURRENCY_CHANGE,
  MEMBERS_TYPE_CHANGE,
  SEARCH_UPDATE,
  SORT_MEMBERS_CHANGE,
  TIME_PERIOD_CHANGE,
  REQUEST_MEMBERS_LIST,
  REQUEST_MEMBERS_LIST_SUCCESS,
  REQUEST_MEMBERS_LIST_ERROR,
} from './constants';

export function sortMembersChange(sortType) {
  return {
    type: SORT_MEMBERS_CHANGE,
    sortType,
  };
}

export function searchUpdate(s) {
  return {
    type: SEARCH_UPDATE,
    s,
  };
}

export function timePeriodChange(from, to) {
  return {
    type: TIME_PERIOD_CHANGE,
    from,
    to
  };
}

export function currencyChange(currency) {
  return {
    type: CURRENCY_CHANGE,
    currency
  };
}

export function membersTypeChange(membersType) {
  return {
    type: MEMBERS_TYPE_CHANGE,
    membersType
  };
}

export function advancedToggle(name) {
  return {
    type: ADVANCED_TOGGLE,
    name
  };
}

export function requestMembersList() {
  return {
    type: REQUEST_MEMBERS_LIST,
  };
}

export function requestMembersListSuccess(membersList) {
  return {
    type: REQUEST_MEMBERS_LIST_SUCCESS,
    membersList
  };
}

export function requestMembersError(message) {
  return {
    type: REQUEST_MEMBERS_LIST_ERROR,
    message
  };
}
