import { createSelector } from 'reselect';

const selectTopMembers = (state) => state.topMembers;

const selectMembers = (state) => state.members;

const selectFilter = () => createSelector(
  selectTopMembers,
  (topMembers) => topMembers.get('filter'),
);

const makeSelectMembersList = () => createSelector(
  selectMembers,
  (members) => members.get('list').toJS(),
);

const makeSelectSort = () => createSelector(
  selectFilter(),
  (filter) => filter.get('sort'),
);

const makeSelectSearch = () => createSelector(
  selectFilter(),
  (filter) => filter.get('search'),
);

const makeSelectPeriod = () => createSelector(
  selectFilter(),
  (filter) => filter.get('period')
    .toJS(),
);

const makeSelectCurrency = () => createSelector(
  selectFilter(),
  (filter) => filter.get('currency'),
);

const makeSelectMembersType = () => createSelector(
  selectFilter(),
  (filter) => filter.get('membersType'),
);

const makeSelectAdvanced = () => createSelector(
  selectFilter(),
  (filter) => filter.get('advanced')
    .toJS(),
);

const makeSelectFilter = () => createSelector(
  makeSelectSort(),
  makeSelectSearch(),
  makeSelectPeriod(),
  makeSelectCurrency(),
  makeSelectMembersType(),
  makeSelectAdvanced(),
  (sort, search, period, currency, membersType, advanced) => ({
    sort,
    search,
    period,
    currency,
    membersType,
    advanced,
  }),
);

const makeSelectMembersIds = () => createSelector(
  selectTopMembers,
  (topMembers) => topMembers.get('members'),
);

const makeSelectFilteredMembers = () => createSelector(
  makeSelectMembersIds(),
  makeSelectMembersList(),
  (membersIds, members) => members.filter((item) => membersIds.includes(item.id))
);

export {
  makeSelectSort,
  makeSelectSearch,
  makeSelectPeriod,
  makeSelectCurrency,
  makeSelectMembersType,
  makeSelectAdvanced,
  makeSelectFilter,
  makeSelectFilteredMembers,
};
