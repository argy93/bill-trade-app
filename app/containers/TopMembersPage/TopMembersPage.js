import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import './style.scss';
import FilterPanel from '../../components/FilterPanel/FilterPanel';
import MemberCard from '../../components/MemberCard';

export default class TopMembersPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      loading,
      error,
      sort,
      search,
      period,
      currency,
      membersType,
      advanced,
      sortMembersChange,
      searchUpdate,
      timePeriodChange,
      currencyChange,
      membersTypeChange,
      advancedToggle,
      filteredMembers,
    } = this.props;

    return (
      <div className="l-top-members-page">
        <Helmet>
          <title>Top members</title>
          <meta name="description" content="Top members page" />
        </Helmet>
        {!loading ? (
          <Fragment>
            <div className="l-top-members-page__head">
              <FilterPanel
                membersType={membersType}
                currency={currency}
                search={search}
                sort={sort}
                from={period.from}
                to={period.to}
                searchUpdate={searchUpdate}
                currencyUpdate={currencyChange}
                membersTypeUpdate={membersTypeChange}
                sortUpdate={sortMembersChange}
                timePeriodUpdate={timePeriodChange}
                advanced={advanced}
                advancedToggle={advancedToggle}
              />
            </div>
            <div className="l-top-members-page__content">
              {!filteredMembers.length ? ('No members for you request') : null}
              {filteredMembers.map((member) => (
                <MemberCard
                  key={member.id}
                  {...member}
                />
              ))}
            </div>
          </Fragment>
        ) : ('loading...')}
      </div>
    );
  }
}

TopMembersPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.bool,
  sort: PropTypes.string,
  search: PropTypes.string,
  period: PropTypes.object,
  currency: PropTypes.string,
  membersType: PropTypes.string,
  advanced: PropTypes.object,
  sortMembersChange: PropTypes.func,
  searchUpdate: PropTypes.func,
  timePeriodChange: PropTypes.func,
  currencyChange: PropTypes.func,
  membersTypeChange: PropTypes.func,
  advancedToggle: PropTypes.func,
  filteredMembers: PropTypes.array,
};
