import {
  takeLatest,
  put,
  select,
  delay,
} from 'redux-saga/effects';
import {
  ADVANCED_TOGGLE,
  CURRENCY_CHANGE,
  MEMBERS_TYPE_CHANGE, REQUEST_MEMBERS_LIST,
  SEARCH_UPDATE,
  SORT_MEMBERS_CHANGE,
  TIME_PERIOD_CHANGE,
} from './constants';
import {
  requestMembersError,
  requestMembersList as actionRequestMembersList,
  requestMembersListSuccess,
} from './actions';
import { makeSelectFilter } from './selectors';
import { generate } from '../../utils/dummy/dummyProfile';

function* filterUpdate() {
  yield put(actionRequestMembersList());
}

function* requestMembersList() {
  const filterData = yield select(makeSelectFilter());

  try {
    // TODO: send request to server remove delay
    yield delay(300);

    yield put(requestMembersListSuccess(generate()));
  } catch (e) {
    yield put(requestMembersError(e.toString()));
  }
}

export default function* topMembersSaga() {
  yield takeLatest([
    SORT_MEMBERS_CHANGE,
    SEARCH_UPDATE,
    TIME_PERIOD_CHANGE,
    CURRENCY_CHANGE,
    MEMBERS_TYPE_CHANGE,
    ADVANCED_TOGGLE,
  ], filterUpdate);

  yield takeLatest(REQUEST_MEMBERS_LIST, requestMembersList);
}
