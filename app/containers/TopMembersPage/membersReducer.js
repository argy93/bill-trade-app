import * as immutable from 'immutable';
import {
  REQUEST_MEMBERS_LIST_SUCCESS,
} from './constants';

const initialState = immutable.fromJS({
  list: [],
});

function toImmutableKeyedMap(data) {
  return immutable.fromJS(data)
    .toMap()
    // eslint-disable-next-line no-unused-vars
    .mapEntries(([key, value]) => {
      return [value.get('id'), value];
    });
}

function membersReducer(state = initialState, action) {
  let currentList;
  let newList;

  switch (action.type) {
    case REQUEST_MEMBERS_LIST_SUCCESS:
      currentList = toImmutableKeyedMap(state.get('list'));
      newList = toImmutableKeyedMap(action.membersList);

      return state.set('list', currentList.merge(newList).toList());
    default:
      return state;
  }
}

export default membersReducer;
