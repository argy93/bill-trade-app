import * as immutable from 'immutable';
import {
  TOGGLE_SELECT_PLATFORM,
} from './constants';

// The initial state of the App
const initialState = immutable.fromJS({
  platforms: {}
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_SELECT_PLATFORM:
      return state.setIn(['platforms', action.platformName], !state.getIn(['platforms', action.platformName]));
    default:
      return state;
  }
}

export default homeReducer;
