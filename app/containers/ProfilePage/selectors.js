import { createSelector } from 'reselect';
import { makeSelectPortfolio } from '../App/selectors';

const selectProfile = (state) => state.profile;

const makeSelectActivityType = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('activityType'),
);

const makeSelectActivityFull = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('activityFull'),
);

const makeSelectActivityPeriod = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('activityPeriod')
    .toJS(),
);

const makeSelectActivityAnimateToFull = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('animateToFull'),
);

const makeSelectActivityAnimateToNormal = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('animateToNormal'),
);

const makeSelectPlatforms = () => createSelector(
  selectProfile,
  (profileState) => profileState.get('platforms')
    .toJS(),
);

const makeSelectPortfolioItems = () => createSelector(
  makeSelectPortfolio(),
  makeSelectPlatforms(),
  (portfolio, platforms) => portfolio.map((pItem) => ({
    platformName: pItem.name,
    selected: !!platforms[pItem.name],
    stats: pItem.stats.map((statsItem) => ({
      currencyName: statsItem.name,
      value: statsItem.value,
    })),
  })),
);

export {
  makeSelectActivityType,
  makeSelectActivityFull,
  makeSelectActivityPeriod,
  makeSelectActivityAnimateToFull,
  makeSelectActivityAnimateToNormal,
  makeSelectPortfolioItems,
};
