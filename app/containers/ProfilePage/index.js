import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import { profileRequest } from '../App/actions';
import {
  makeSelectPhoto,
  makeSelectAbout,
  makeSelectAverageTime,
  makeSelectBalanceBtc,
  makeSelectBalanceUsd,
  makeSelectFollowers,
  makeSelectFollowing,
  makeSelectMaxLoss,
  makeSelectName,
  makeSelectProfitBtc,
  makeSelectProfitUsd,
  makeSelectRank,
  makeSelectTagsMain,
  makeSelectTagsSecond,
  makeSelectTrades,
  makeSelectTradingType,
  makeSelectWinRate,
  makeSelectProfileError,
  makeSelectProfileErrorMessage,
  makeSelectProfileLoading,
  makeSelectStrides,
} from '../App/selectors';
import {
  makeSelectPortfolioItems,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import ProfilePage from './ProfilePage';
import {
  toggleSelectPlatform,
} from './actions';

const mapDispatchToProps = (dispatch) => ({
  getProfileData: () => dispatch(profileRequest()),
  toggleSelectPlatform: (platformName) => dispatch(toggleSelectPlatform(platformName)),
});

const withReducer = injectReducer({
  key: 'profile',
  reducer,
});

const mapStateToProps = createStructuredSelector({
  photo: makeSelectPhoto(),
  name: makeSelectName(),
  tagsMain: makeSelectTagsMain(),
  tagsSecond: makeSelectTagsSecond(),
  following: makeSelectFollowing(),
  followers: makeSelectFollowers(),
  winRate: makeSelectWinRate(),
  rank: makeSelectRank(),
  maxLoss: makeSelectMaxLoss(),
  averageTime: makeSelectAverageTime(),
  trades: makeSelectTrades(),
  tradingType: makeSelectTradingType(),
  portfolio: makeSelectPortfolioItems(),
  balanceBtc: makeSelectBalanceBtc(),
  balanceUsd: makeSelectBalanceUsd(),
  profitBtc: makeSelectProfitBtc(),
  profitUsd: makeSelectProfitUsd(),
  about: makeSelectAbout(),
  error: makeSelectProfileError(),
  errorMessage: makeSelectProfileErrorMessage(),
  loading: makeSelectProfileLoading(),
  strides: makeSelectStrides(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({
  key: 'profile',
  saga,
});

export default compose(withReducer, withSaga, withConnect)(ProfilePage);
export { mapDispatchToProps };
