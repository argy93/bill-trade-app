import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import './style.scss';
import PropTypes from 'prop-types';
import ProfileCard from '../../components/ProfileCard';
import WalletInfo from '../../components/WalletInfo';
import AboutCard from '../../components/AboutCard';
import Stats from '../../components/Stats';
import Portfolio from '../../components/Portfolio';
import TopTradesList from '../TopTradesList';
import Posts from '../Posts';
import ProfileActivity from '../ProfileActivity';

export default class ProfilePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { getProfileData } = this.props;

    getProfileData();
  }

  render() {
    const {
      loading,
      photo,
      name,
      tagsMain,
      tagsSecond,
      following,
      followers,
      winRate,
      rank,
      maxLoss,
      averageTime,
      trades,
      tradingType,
      portfolio,
      balanceBtc,
      balanceUsd,
      profitBtc,
      profitUsd,
      about,
      strides,
      toggleSelectPlatform,
    } = this.props;

    return (
      <div className="l-profile">
        <Helmet>
          <title>Profile Page</title>
          <meta name="description" content="A React.js Boilerplate application homepage" />
        </Helmet>
        {!loading ? (
          <Fragment>
            <ProfileActivity type={'full'} />
            <div className="l-profile__content">
              <div className="l-profile__column is-left">
                <div className="l-profile__content-card">
                  <ProfileCard
                    photo={photo}
                    name={name}
                    tagsMain={tagsMain}
                    tagsSecond={tagsSecond}
                    following={following}
                    followers={followers}
                    winRate={winRate}
                    userType={'own'}
                  />
                  <div className="l-profile__wallets">
                    <WalletInfo
                      title={'Balance'}
                      usd={balanceUsd}
                      btc={balanceBtc}
                      type={'blue'}
                    />
                    <WalletInfo
                      title={'All time profit'}
                      usd={profitUsd}
                      btc={profitBtc}
                      type={'green'}
                    />
                  </div>
                </div>
                <div className="l-profile__content-card">
                  <AboutCard
                    text={about}
                  />
                </div>
              </div>
              <div className="l-profile__column is-center">
                <ProfileActivity type={'normal'} />
                <div className="l-profile__content-card">
                  <Stats
                    rank={rank}
                    maxLoss={maxLoss}
                    averageTime={averageTime}
                    trades={trades}
                    tradingType={tradingType}
                    strides={strides}
                  />
                </div>
                <div className="l-profile__content-card">
                  <Portfolio
                    items={portfolio}
                    toggleSelectPlatform={toggleSelectPlatform}
                  />
                </div>
              </div>
              <div className="l-profile__column is-right">
                <div className="l-profile__content-card">
                  <TopTradesList />
                </div>
                <div className="l-profile__content-card">
                  <Posts />
                </div>
              </div>
            </div>
          </Fragment>
        ) : ('loading...')}
      </div>
    );
  }
}

ProfilePage.propTypes = {
  loading: PropTypes.bool,
  photo: PropTypes.any,
  name: PropTypes.any,
  tagsMain: PropTypes.any,
  tagsSecond: PropTypes.any,
  following: PropTypes.any,
  followers: PropTypes.any,
  winRate: PropTypes.any,
  rank: PropTypes.any,
  maxLoss: PropTypes.any,
  averageTime: PropTypes.any,
  trades: PropTypes.any,
  tradingType: PropTypes.any,
  portfolio: PropTypes.any,
  balanceBtc: PropTypes.any,
  balanceUsd: PropTypes.any,
  profitBtc: PropTypes.any,
  profitUsd: PropTypes.any,
  about: PropTypes.any,
  getProfileData: PropTypes.func,
  activityPeriod: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  strides: PropTypes.array,
  toggleSelectPlatform: PropTypes.func,
};
