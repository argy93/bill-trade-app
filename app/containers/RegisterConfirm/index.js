import React from 'react';
import { withRouter } from 'react-router-dom';
import RegisterConfirm from './RegisterConfirm';

// eslint-disable-next-line no-unused-vars
const mapDispatchToProps = (dispatch) => ({});

const RegisterConfirmWithRouter = withRouter((props) => <RegisterConfirm {...props} />);

export default RegisterConfirmWithRouter;
export {
  mapDispatchToProps
};
