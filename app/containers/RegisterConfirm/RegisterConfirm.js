/* eslint-disable react/no-unused-prop-types */
/*
 * RegisterConfirm
 *
 * This is the first thing users see when they start auth, at the '/sign-in' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import Auth from '../App/auth';

import {
  DEFAULT_ERROR_MESSAGE,
} from '../App/constants';

import Title from '../../components/Title';
import Input from '../../components/Input';
import Button from '../../components/Button';

import background from '../../images/auth-background.svg';
import logo from '../../images/logo-white.svg';

// import { toast } from 'react-toastify';
import './style.scss';

// eslint-disable-line react/prefer-stateless-function
export default class RegisterConfirm extends React.PureComponent {
  state = {
    confirmValue: '',
  }

  constructor(props) {
    super(props);

    const code = props.location.search.substr(6);

    this.state = {
      confirmValue: code,
    };
  }

  handleConfirm = () => {
    const { confirmValue } = this.state;
    const { history } = this.props;
    Auth.confirm(confirmValue)
      .then(() => {
        toast('✅ Your account has been successfully verified.');
        history.push('/sign-in');
      })
      .catch((error) => {
        toast(DEFAULT_ERROR_MESSAGE);
        console.log(error);
      });
  }

  handleConfirmChange = (value) => {
    this.setState({
      confirmValue: value,
    });
  }

  render() {
    const { confirmValue } = this.state;
    return (
      <div className="l-register-confirm">
        <div
          className="l-register__background"
        >
          <img src={logo} alt="Bill Trade" className="l-register__logo" />
          <div
            className="img"
            style={{ backgroundImage: `url(${background})` }}
          >
          </div>
        </div>
        <div className="l-register__form">
          <div className="wrapper">
            <form className="flex-container">
              <div className="column">
                <Title size="B4" weight="bold">Confirm your registration</Title>
                <br />
                <Input
                  index={1}
                  defaultValue={confirmValue}
                  name="confirm"
                  onChange={this.handleConfirmChange}
                >
                </Input>
                <Button
                  index={700}
                  type="contained"
                  onClick={this.handleConfirm}
                >
                  Confirm
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

RegisterConfirm.propTypes = {
  location: PropTypes.any,
  history: PropTypes.any,
};
