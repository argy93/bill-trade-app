import React from 'react';
import { withRouter } from 'react-router-dom';
import SettingsPage from './SettingsPage';

// eslint-disable-next-line no-unused-vars
const mapDispatchToProps = (dispatch) => ({});

const SettingsPageWithRouter = withRouter((props) => <SettingsPage {...props} />);

export default SettingsPageWithRouter;
export {
  mapDispatchToProps
};
