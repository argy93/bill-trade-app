/*
 * SettingsConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

import { BASE_URL } from '../App/constants';

export const API_KEYS_LIST_URL = `${BASE_URL}/account/me/stock-api-keys`;
export const API_KEY_SET_URL = `${BASE_URL}/account/me/stock-api-keys`;
export const API_KEY_DELETE_URL = `${BASE_URL}/account/me/stock-api-keys/`;
export const TRADING_TYPE_UPDATE_URL = `${BASE_URL}/account/me`;
export const PROFILE_URL = `${BASE_URL}/account/me`;
