/* eslint-disable react/no-unused-prop-types */
/*
 * SettingsPage
 *
 * This is the first thing users see when they start auth, at the '/sign-in' route
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import {
  Tabs,
  Tab,
  TabPanel,
  TabList
} from 'react-web-tabs';
// import { toast } from 'react-toastify';
import APIkeys from '../../components/APIkeys';
import Input from '../../components/Input';
import TradingType from '../../components/TradingType';
import AccountSettings from '../../components/AccountSettings';
import './style.scss';

// eslint-disable-line react/prefer-stateless-function
export default class SettingsPage extends React.PureComponent {
  state = {
    activeTab: 'one',
    tabsWidth: 'calc(70% - 34px)',
  };

  handleTabChange = (activeTab) => {
    const tabsWidth = activeTab === 'one' ? 'calc(70% - 34px)' : '100%';
    this.setState({
      activeTab,
      tabsWidth,
    });
  }

  render() {
    const { activeTab, tabsWidth } = this.state;

    const passwordChange = (
      <div className="password-change">
        <div className="title">Change password</div>
        <Input
          index={1}
          labelStyles={{ color: '#1F2123', fontWeight: 600 }}
          name="oldpass"
          label="Old password"
        >
        </Input>
        <Input
          index={1}
          labelStyles={{ color: '#1F2123', fontWeight: 600 }}
          name="newpass"
          label="New password"
        >
        </Input>
        <Input
          index={1}
          labelStyles={{ color: '#1F2123', fontWeight: 600 }}
          name="confirm"
          label="Confirm new password"
        >
        </Input>
        <div className="footer-container-default">
          <button onClick={this.handlePasswordChange} type="button" className="add-new">Save new password</button>
        </div>
      </div>
    );

    return (
      <div className="l-settings">
        <Helmet>
          <title>Settings Page</title>
          <meta name="description" content="BillTrade settings page" />
        </Helmet>
        <Tabs
          defaultTab={activeTab}
          className="tabs-container"
          onChange={this.handleTabChange}
          style={{ width: tabsWidth }}
        >
          <TabList className="tab-list">
            <Tab className={activeTab === 'one' ? 'active' : ''} tabFor="one">Account</Tab>
            <Tab className={activeTab === 'two' ? 'active' : ''} tabFor="two">API keys</Tab>
            <Tab className={activeTab === 'three' ? 'active' : ''} tabFor="three">Trading type</Tab>
          </TabList>
          <TabPanel tabId="one">
            <AccountSettings />
          </TabPanel>
          <TabPanel tabId="two">
            <APIkeys />
          </TabPanel>
          <TabPanel tabId="three">
            <TradingType />
          </TabPanel>
        </Tabs>
        { activeTab === 'one' ? passwordChange : '' }
      </div>
    );
  }
}

SettingsPage.propTypes = {
  history: PropTypes.any
};
