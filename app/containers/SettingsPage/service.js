/* eslint-disable camelcase */
/*
 * Settings Service
 */

import axios from 'axios';
import {
  Subject
} from 'rxjs';
import {
  API_KEYS_LIST_URL,
  API_KEY_SET_URL,
  API_KEY_DELETE_URL,
  TRADING_TYPE_UPDATE_URL,
  PROFILE_URL,
} from './constants';

axios.defaults.withCredentials = true;

const SettingsService = {
  APIkeys$: new Subject({
    data: []
  }),
  listAPIkeys() {
    axios.get(API_KEYS_LIST_URL)
      .then(({ data }) => {
        if (data.data.length) {
          this.APIkeys$.next({
            data: data.data,
          });
        } else {
          this.APIkeys$.next({
            data: [],
          });
        }
      })
      .catch(console.log);
  },

  createAPIkey(api_key, api_secret, stock) {
    return axios.post(API_KEY_SET_URL, {
      stock,
      api_key,
      api_secret,
    })
  },

  deleteAPIkey(id) {
    axios.delete(`${API_KEY_DELETE_URL}${id}`)
      .then((response) => {
        console.log(response);
        this.listAPIkeys();
      })
      .catch();
  },

  updateTradingType(trader_type, copier_type) {
    return axios.patch(TRADING_TYPE_UPDATE_URL, {
      copier_type,
      trader_type,
    });
  },

  getAccount() {
    return axios.get(PROFILE_URL);
  }
};

export default SettingsService;
