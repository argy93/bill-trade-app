import React, { Fragment } from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import TopTradesListComponent from '../../components/TopTradesList';

export default class TopTradesList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    const { requestList } = this.props;

    requestList();
  }

  render() {
    const {
      loading,
      error,
      filterType,
      tradesPeriod,
      lossPeriod,
      changeFilterType,
      changeTradesPeriod,
      changeLossPeriod,
      filteredList
    } = this.props;

    return (
      <Fragment>
        <TopTradesListComponent
          filterType={filterType}
          tradesPeriod={tradesPeriod}
          lossPeriod={lossPeriod}
          changeFilterType={changeFilterType}
          changeTradesPeriod={changeTradesPeriod}
          changeLossPeriod={changeLossPeriod}
          trades={filteredList}
        />
      </Fragment>
    );
  }
}

TopTradesList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.bool,
  filterType: PropTypes.oneOf(['trades', 'loss']),
  tradesPeriod: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  lossPeriod: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  filteredList: PropTypes.array,
  requestList: PropTypes.func,
  changeFilterType: PropTypes.func,
  changeTradesPeriod: PropTypes.func,
  changeLossPeriod: PropTypes.func,
};
