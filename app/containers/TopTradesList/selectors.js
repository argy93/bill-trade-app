import {createSelector} from 'reselect';

const selectTopTradesList = (state) => state.topTradesList;

const makeSelectLoading = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('loading'),
);

const makeSelectError = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('error'),
);

const makeSelectFilterType = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('filterType'),
);

const makeSelectTopTradesList = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('topTradesList')
    .toJS(),
);

const makeSelectTopLossList = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('topLossList')
    .toJS(),
);

const makeSelectTradesPeriod = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('tradesPeriod')
    .toJS(),
);

const makeSelectLossPeriod = () => createSelector(
  selectTopTradesList,
  (topTradesListState) => topTradesListState.get('lossPeriod')
    .toJS(),
);

const makeSelectFilteredTradesList = () => createSelector(
  makeSelectTopTradesList(),
  makeSelectTopLossList(),
  makeSelectFilterType(),
  makeSelectTradesPeriod(),
  makeSelectLossPeriod(),
  (topTradesList, topLossList, filterType, tradesPeriod, lossPeriod) => {
    const limit = 5;
    let resultList = [];

    // TODO: Доделать ограничение по лимиту

    if (filterType === 'trades') {
      resultList = topTradesList.filter((item, counter) => item.timestamp < tradesPeriod.from && item.timestamp > tradesPeriod.to);
    } else if (filterType === 'loss') {
      resultList = topLossList.filter((item, counter) => item.timestamp < lossPeriod.from && item.timestamp > lossPeriod.to);
    } else {
      return [];
    }

    return resultList;
  },
);

export {
  makeSelectLoading,
  makeSelectError,
  makeSelectFilterType,
  makeSelectTopLossList,
  makeSelectTopTradesList,
  makeSelectFilteredTradesList,
  makeSelectTradesPeriod,
  makeSelectLossPeriod,
};
