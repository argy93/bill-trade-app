import {
  takeLatest,
  delay,
  put,
} from 'redux-saga/effects';
import { REQUEST_LIST } from './constants';
import { requestListSuccess } from './actions';

function* requestList() {
  yield delay(400);
  yield put(requestListSuccess([
    {
      id: 1,
      timestamp: Date.now() - 10000,
      name: 'Irvine1290',
      followers: 45,
      price: 587102,
      crypto: 'BTC/ETH'
    },
    {
      id: 2,
      timestamp: Date.now() - (1000 * 3600 * 24) * 4,
      name: 'Irvine1290',
      followers: 45,
      price: 230046,
      crypto: 'LTC/OMG'
    },
    {
      id: 3,
      timestamp: Date.now() - (1000 * 3600 * 24) * 6,
      name: 'Irvine1290',
      followers: 45,
      price: 120102,
      crypto: 'BTC/ETH'
    },
    {
      id: 4,
      timestamp: Date.now() - (1000 * 3600 * 24) * 25,
      name: 'Irvine1290',
      followers: 45,
      price: 85201,
      crypto: 'BTC/ETH'
    },
    {
      id: 5,
      timestamp: Date.now() - (1000 * 3600 * 24) * 25,
      name: 'Irvine1290',
      followers: 45,
      price: 23420,
      crypto: 'BTC/ETH'
    },
    {
      id: 6,
      timestamp: Date.now() - (1000 * 3600 * 24) * 29,
      name: 'Irvine1290',
      followers: 45,
      price: 587102,
      crypto: 'BTC/ETH'
    },
    {
      id: 7,
      timestamp: Date.now() - 10000,
      name: 'Irvine1290',
      followers: 45,
      price: 380406,
      crypto: 'BTC/ETH'
    },
  ], [
    {
      id: 1,
      timestamp: Date.now() - 10000,
      name: 'Irvine1290',
      followers: 45,
      price: -587102,
      crypto: 'BTC/ETH'
    },
    {
      id: 2,
      timestamp: Date.now() - (1000 * 3600 * 24) * 3,
      name: 'Irvine1290',
      followers: 45,
      price: -230046,
      crypto: 'LTC/OMG'
    },
    {
      id: 3,
      timestamp: Date.now() - (1000 * 3600 * 24) * 6,
      name: 'Irvine1290',
      followers: 45,
      price: -120102,
      crypto: 'BTC/ETH'
    },
    {
      id: 4,
      timestamp: Date.now() - (1000 * 3600 * 24) * 15,
      name: 'Irvine1290',
      followers: 45,
      price: -85201,
      crypto: 'BTC/ETH'
    },
    {
      id: 5,
      timestamp: Date.now() - (1000 * 3600 * 24) * 18,
      name: 'Irvine1290',
      followers: 45,
      price: -23420,
      crypto: 'BTC/ETH'
    },
    {
      id: 6,
      timestamp: Date.now() - (1000 * 3600 * 24) * 20,
      name: 'Irvine1290',
      followers: 45,
      price: -587102,
      crypto: 'BTC/ETH'
    },
    {
      id: 7,
      timestamp: Date.now() - (1000 * 3600 * 24) * 29,
      name: 'Irvine1290',
      followers: 45,
      price: -380406,
      crypto: 'BTC/ETH'
    }
  ]));
}

export default function* topTradesListSaga() {
  yield takeLatest(REQUEST_LIST, requestList);
}
