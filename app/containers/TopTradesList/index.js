import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectError,
  makeSelectFilteredTradesList,
  makeSelectFilterType,
  makeSelectLoading,
  makeSelectLossPeriod,
  makeSelectTradesPeriod,
} from './selectors';
import injectSaga from '../../utils/injectSaga';
import saga from './saga';
import TopTradesList from './TopTradesList';
import injectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import {
  changeFilterType,
  changeLossPeriod,
  changeTradesPeriod,
  requestList,
} from './actions';

const mapDispatchToProps = (dispatch) => ({
  requestList: () => dispatch(requestList()),
  changeFilterType: (type) => dispatch(changeFilterType(type)),
  changeTradesPeriod: (from, to) => dispatch(changeTradesPeriod(from, to)),
  changeLossPeriod: (from, to) => dispatch(changeLossPeriod(from, to)),
});

const withReducer = injectReducer({
  key: 'topTradesList',
  reducer,
});

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  filterType: makeSelectFilterType(),
  tradesPeriod: makeSelectTradesPeriod(),
  lossPeriod: makeSelectLossPeriod(),
  filteredList: makeSelectFilteredTradesList(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({
  key: 'topTradesList',
  saga,
});

export default compose(withReducer, withSaga, withConnect)(TopTradesList);
export { mapDispatchToProps };
