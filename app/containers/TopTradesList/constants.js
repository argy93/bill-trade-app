export const CHANGE_FILTER_TYPE = 'bt/TopTradesList/CHANGE_FILTER_TYPE';
export const CHANGE_TRADES_PERIOD = 'bt/TopTradesList/CHANGE_TRADES_PERIOD';
export const CHANGE_LOSS_PERIOD = 'bt/TopTradesList/CHANGE_LOSS_PERIOD';
export const REQUEST_LIST = 'bt/TopTradesList/REQUEST_LIST';
export const REQUEST_LIST_SUCCESS = 'bt/TopTradesList/REQUEST_LIST_SUCCESS';
export const REQUEST_LIST_ERROR = 'bt/TopTradesList/REQUEST_LIST_ERROR';
