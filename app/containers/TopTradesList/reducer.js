import * as immutable from 'immutable';
import {
  CHANGE_FILTER_TYPE,
  CHANGE_LOSS_PERIOD,
  CHANGE_TRADES_PERIOD,
  REQUEST_LIST_ERROR,
  REQUEST_LIST_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = immutable.fromJS({
  filterType: 'trades', // loss
  tradesPeriod: {
    from: Date.now(),
    to: Date.now() - (1000 * 3600 * 24),
  },
  lossPeriod: {
    from: Date.now(),
    to: Date.now() - (1000 * 3600 * 24),
  },
  topTradesList: [],
  topLossList: [],
});

function topTradesListReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LIST_SUCCESS:
      return state.set('topTradesList', immutable.fromJS(action.topTradesList))
        .set('topLossList', immutable.fromJS(action.topLossList))
        .set('loading', false);
    case REQUEST_LIST_ERROR:
      return state.set('loading', false)
        .set('error', true);
    case CHANGE_FILTER_TYPE:
      return state.set('filterType', action.filterType);
    case CHANGE_LOSS_PERIOD:
      return state.setIn(['lossPeriod', 'from'], action.from)
        .setIn(['lossPeriod', 'to'], action.to);
    case CHANGE_TRADES_PERIOD:
      return state.setIn(['tradesPeriod', 'from'], action.from)
        .setIn(['tradesPeriod', 'to'], action.to);
    default:
      return state;
  }
}

export default topTradesListReducer;
