import {
  CHANGE_FILTER_TYPE,
  CHANGE_LOSS_PERIOD,
  CHANGE_TRADES_PERIOD,
  REQUEST_LIST, REQUEST_LIST_ERROR,
  REQUEST_LIST_SUCCESS,
} from './constants';

export function changeFilterType(filterType) {
  return {
    type: CHANGE_FILTER_TYPE,
    filterType,
  };
}

export function changeTradesPeriod(from, to) {
  return {
    type: CHANGE_TRADES_PERIOD,
    from,
    to,
  };
}

export function changeLossPeriod(from, to) {
  return {
    type: CHANGE_LOSS_PERIOD,
    from,
    to,
  };
}

export function requestList() {
  return {
    type: REQUEST_LIST,
  };
}

export function requestListSuccess(topTradesList, topLossList) {
  return {
    type: REQUEST_LIST_SUCCESS,
    topTradesList,
    topLossList,
  };
}

export function requestListError(message) {
  return {
    type: REQUEST_LIST_ERROR,
    message
  };
}
