FROM node:10.16.3 as node-dev

WORKDIR /opt/app
COPY ./package*.json /opt/app/
RUN npm i
COPY ./ /opt/app
ARG ENV
ARG NODE_ENV
ARG ENV
ENV ENV $ENV
RUN if [ "$ENV" = "prod" ]; \
  then NODE_ENV=$NODE_ENV npm run build:prod;  \
  else NODE_ENV=$NODE_ENV npm run build:dev; \
  fi

FROM node:10.16.3-slim as node-prod
ENV NODE_ENV $NODE_ENV
COPY --from=node-dev /opt/app/build /opt/app
WORKDIR /opt/app
EXPOSE 3000

RUN apt update -y && \
    apt install -y nginx && \
    update-rc.d nginx disable
COPY ./docker.d/nginx.conf /etc/nginx/nginx.conf
COPY ./docker.d/service.conf /etc/nginx/conf.d/service.conf

ENTRYPOINT ["nginx", "-c", "/etc/nginx/nginx.conf"]
